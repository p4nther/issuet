package cz.vse.javaee.issuet;

/**
 * Extended class for GitHub client used with eGit GitHub library, 
 * which allows to read OAuth token.
 * @author ondrejsatera
 */
public class GitHubClient extends org.eclipse.egit.github.core.client.GitHubClient {
    protected String token;
    
    public String getOAuthToken() {
        return this.token;
    }

    @Override
    public GitHubClient setOAuth2Token(String token) {
        this.token = token;
        super.setOAuth2Token(token);
        return this;
    }
}
