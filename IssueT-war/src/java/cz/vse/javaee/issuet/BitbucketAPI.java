package cz.vse.javaee.issuet;

import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.Token;

/**
 * API class for Scribe library to access BitBucket's API
 * @author ondrejsatera
 */
public class BitbucketAPI extends DefaultApi10a {
    public static final String serviceUrl = "https://bitbucket.org/api";
    public static final String oauthUrl = "https://bitbucket.org/api/1.0/oauth";
        
    @Override
    public String getRequestTokenEndpoint() {
        return oauthUrl + "/request_token";
    }

    @Override
    public String getAccessTokenEndpoint() {
        return oauthUrl + "/access_token";
    }

    @Override
    public String getAuthorizationUrl(Token token) {
        return String.format(oauthUrl + "/authenticate?oauth_token=%s", token.getToken());
    }
    
    /**
     * Creates BitBucket API endpoint URL with given query params divided by slashes (/)
     * @param queryParams
     * @return URL string
     */
    public static String formatUrl(String queryParams[]) {
        StringBuilder builder = new StringBuilder();
        builder.append(serviceUrl);
        for (String param : queryParams) {
            builder.append("/");
            builder.append(param);
        }
        return builder.toString();
    }
    
}
