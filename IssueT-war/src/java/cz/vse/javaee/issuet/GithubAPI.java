package cz.vse.javaee.issuet;

import java.util.Random;
import org.scribe.builder.api.DefaultApi20;
import org.scribe.model.OAuthConfig;

/**
 * Class which can be used with Scribe library to access GitHub API
 * @author ondrejsatera
 */
public class GithubAPI extends DefaultApi20 {
    public static final String oauthUrl = "https://github.com/login/oauth";
    
    /**
     * Returns URI with endpoint to access token
     * @return URI string
     */
    @Override
    public String getAccessTokenEndpoint() {
        return oauthUrl + "/access_token";
    }

    /**
     * Returns URI with endpoint to authorize token
     * @param oac
     * @return URI string
     */
    @Override
    public String getAuthorizationUrl(OAuthConfig oac) {
        Random rand = new Random();
        int state = rand.nextInt(10000000);
        return String.format("%s/authorize?client_id=%s&redirect_uri=%s&scope=%s&state=%s", oauthUrl, oac.getApiKey(), oac.getCallback(), oac.getScope(), state);
    }
    
}
