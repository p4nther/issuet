package cz.vse.javaee.issuet.exceptions;

/**
 * Used when user is not authorized in VCS repository
 * @author ondrejsatera
 */
public class NotAuthorizedException extends Exception {
}
