package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.exceptions.NotAuthorizedException;
import cz.vse.javaee.issuet.frontend.BitBucketPageBean;
import cz.vse.javaee.issuet.frontend.BitBucketServlet;
import cz.vse.javaee.issuet.frontend.GithubPageBean;
import cz.vse.javaee.issuet.frontend.GithubServlet;
import cz.vse.javaee.issuet.frontend.IssuePageBean;
import cz.vse.javaee.issuet.frontend.ProjectPageBean;
import cz.vse.javaee.issuet.model.Person;
import java.io.IOException;
import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

/**
 * Servlet used for main application screen and as a parent for all other servlets
 * @author ondrejsatera
 */
public class FrontendServlet extends HttpServlet {

    @EJB
    protected UserDAOBeanLocal userDAO;
    
    @EJB
    protected ProjectDAOBeanLocal projectDAO;
    
    @EJB
    protected IssueDAOLocal issueDAO;
    
    @EJB
    protected GithubDAOBeanLocal githubDAO;
    
    @EJB
    protected BitBucketDAOBeanLocal bitbucketDAO;
    
    @EJB
    protected RepositoryDAOBeanLocal repositoryDAO;
    
    @EJB
    protected CommitDAOBeanLocal commitDAO;
    
    @Resource(name = "mail/myMailSession")
    private Session mailSession;
    
    protected Person loggedUser = null;
    
    protected OAuthService bitbucketOAuthService;
    
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        
        initializeBitBucketService();
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @param action name of the action to be processed
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String action)
            throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        verifyLoggedUser(request, response);
        processRequest(request, response, action);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        verifyLoggedUser(request, response);
        processRequest(request, response, action);
    }
    
    /**
     * If some user is logged in, fetches application Person into the session and internal attribute
     * @param request
     * @param response
     */
    protected void verifyLoggedUser(HttpServletRequest request, HttpServletResponse response) {
        if (request.getUserPrincipal() != null)
        {
            try {
                loggedUser = userDAO.getUserByEmail(request.getUserPrincipal().getName());
            } catch (NoUserFoundException ex) {
                request.getSession().invalidate();
            }
        }
        request.getSession().setAttribute("loggedUser", loggedUser);
    }
    
    /**
     * Creates Project Page Bean and saves it in the given session
     * @param session
     * @return ProjectPageBean
     */
    protected ProjectPageBean initializeProjectPageBean(HttpSession session) {
        ProjectPageBean bean = new ProjectPageBean();
        
        session.setAttribute("projectBean", bean);
        
        return bean;
    }
    
    /**
     * Returns Project Page Bean, initializes it if not available
     * @param request
     * @return ProjectPageBean
     */
    protected ProjectPageBean getProjectPageBean(HttpServletRequest request) {
        ProjectPageBean projectBean = (ProjectPageBean) request.getSession().getAttribute("projectBean");
        if (projectBean == null) {
            projectBean = initializeProjectPageBean(request.getSession());
        }
        return projectBean;
    }
    
    /**
     * Creates new Issue Page Bean and saves it in the given session
     * @param session
     * @return IssuePageBean
     */
    protected IssuePageBean initializeIssuePageBean(HttpSession session) {
        IssuePageBean bean = new IssuePageBean();
        
        session.setAttribute("issueBean", bean);
        
        return bean;
    }
    
    /**
     * Returns Issue Page Bean, initializes it if not available
     * @param request
     * @return IssuePageBean
     */
    protected IssuePageBean getIssuePageBean(HttpServletRequest request) {
        IssuePageBean issueBean = (IssuePageBean) request.getSession().getAttribute("issueBean");
        if (issueBean == null) {
            issueBean = initializeIssuePageBean(request.getSession());
        }
        return issueBean;
    }
    
    /**
     * Returns Github Page Bean, initializes it if not available
     * @param request
     * @return GithubPageBean
     * @throws ServletException if initialization fails
     */
    protected GithubPageBean getGithubPageBean(HttpServletRequest request) throws ServletException {
        GithubPageBean githubPageBean = 
                (GithubPageBean) request.getSession().getAttribute(GithubPageBean.GITHUB_PAGE_BEAN);
        if (githubPageBean == null) {
            githubPageBean = initializeGithubPageBean(request.getSession());
        }
        return githubPageBean;
    }

    /**
     * Creates new Github Page Bean and saves it in the given session
     * @param session
     * @return GithubPageBean
     * @throws ServletException
     */
    protected GithubPageBean initializeGithubPageBean(HttpSession session) throws ServletException {
        String token = (String) session.getAttribute(GithubServlet.GITHUB_TOKEN);
        
        if ("".equals(token)) {
            throw new ServletException("Github not authorized");
        }
        
        GitHubClient client = new GitHubClient().setOAuth2Token(token);
        GithubPageBean githubPageBean = new GithubPageBean(client, githubDAO);
        
        session.setAttribute(GithubPageBean.GITHUB_PAGE_BEAN, githubPageBean);
        
        return githubPageBean;
    }
    
    /**
     * Returns BitBucket Page Bean, initializes it if not available
     * @param request
     * @return BitBucketPageBean
     * @throws ServletException
     * @throws NotAuthorizedException
     */
    protected BitBucketPageBean getBitBucketPageBean(HttpServletRequest request) 
            throws ServletException, NotAuthorizedException {
        BitBucketPageBean bitBucketPageBean = (BitBucketPageBean) request.getSession().getAttribute(BitBucketServlet.BITBUCKET_PAGE_BEAN);
        if (bitBucketPageBean == null) {
            bitBucketPageBean = initializeBitBucketPageBean(request);
        } else {
            bitBucketPageBean.setUserRepositories(bitbucketDAO.getAll());
        }
        return bitBucketPageBean;
    }
    
    /**
     * Returns BitBucket's token either from session or logged user entity
     * @param request used for fetching session
     * @return BitBucket token
     * @throws NotAuthorizedException if token not available in neither location
     */
    protected Token getBitBucketToken(HttpServletRequest request)
            throws NotAuthorizedException {
        Token token = (Token) request.getSession().getAttribute(BitBucketServlet.BITBUCKET_ACCESS_TOKEN);
        
        if (token == null || token.isEmpty()) {
            token = loggedUser.getBitbucketToken();
            if (token == null || token.isEmpty())
                throw new NotAuthorizedException();
        }
        return token;
    }

    /**
     * Creates new BitBucket Page Bean and saves it in the given session
     * @param request
     * @return BitBucketPageBean
     * @throws ServletException
     * @throws NotAuthorizedException
     */
    protected BitBucketPageBean initializeBitBucketPageBean(HttpServletRequest request) 
            throws ServletException, NotAuthorizedException {
        getBitBucketToken(request); // authorization detection
        initializeBitBucketService();
        BitBucketPageBean bitBucketPageBean = new BitBucketPageBean(bitbucketOAuthService, bitbucketDAO);
        
        bitBucketPageBean.setUserRepositories(bitbucketDAO.getAll());
        
        request.getSession().setAttribute(BitBucketServlet.BITBUCKET_PAGE_BEAN, bitBucketPageBean);
        
        return bitBucketPageBean;
    }
    
    /**
     * Sends email via Java Mail API to given receiver with given subject and email body
     * @param receiver
     * @param subject
     * @param body
     * @return true if email sent, otherwise throws exception
     * @throws ServletException if sending fails
     */
    final protected boolean sendEmail(String receiver, String subject, String body) 
            throws ServletException {
        try {
            Message msg = new MimeMessage(mailSession);
            
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiver, false));
            msg.setSubject(subject);
            msg.setText(body);
            msg.setHeader("X-Mailer", "IssueT mailer");
            msg.setSentDate(new Date());
            
            Transport.send(msg);
            
            return true;
        } catch (MessagingException ex) {
            throw new ServletException(ex);
        }
    }

    /**
     * Initializes BitBucket service with context params from BitBucket website
     */
    private void initializeBitBucketService() {
        String KEY = getServletContext().getInitParameter("BITBUCKET_KEY");
        String SECRET = getServletContext().getInitParameter("BITBUCKET_SECRET");
        String REDIRECT_URI = getServletContext().getInitParameter("BITBUCKET_REDIRECT_URI");
        
        bitbucketOAuthService = new ServiceBuilder()
                .provider(BitbucketAPI.class)
                .apiKey(KEY)
                .apiSecret(SECRET)
                .callback(REDIRECT_URI)
                .build();
    }
}
