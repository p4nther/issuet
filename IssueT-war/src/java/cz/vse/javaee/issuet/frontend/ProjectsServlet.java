package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.FrontendServlet;
import cz.vse.javaee.issuet.exceptions.NotAuthorizedException;
import static cz.vse.javaee.issuet.frontend.ProjectServlet.ERR_PROJECT_NOT_DELETED;
import static cz.vse.javaee.issuet.frontend.ProjectServlet.INF_PROJECT_DELETED;
import cz.vse.javaee.issuet.model.BitBucketRepository;
import cz.vse.javaee.issuet.model.GithubRepository;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.Repository;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for managing internal projects
 * @author ondrejsatera
 */
public class ProjectsServlet extends FrontendServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String action)
            throws ServletException, IOException {
        if ("create".equals(action)) {
            doCreateProject(request, response);
        } else {
            String filter = request.getParameter("filter");
            if ("my".equals(filter)) {
                request.setAttribute("pageTitle", "My Projects");
                request.setAttribute("projects", loggedUser.getProjects());
            } else if ("team".equals(filter)) {
                request.setAttribute("pageTitle", "My Team Projects");
                request.setAttribute("projects", loggedUser.getTeamProjects());
            } else {
                request.setAttribute("pageTitle", "All Projects");
                List<Project> projects = loggedUser.getProjects();
                projects.addAll(loggedUser.getTeamProjects());
                request.setAttribute("projects", projects);
            }
            if (INF_PROJECT_DELETED.equals(request.getParameter("info"))) {
                request.setAttribute("message", "Project succesfully deleted");
            }
            if (ERR_PROJECT_NOT_DELETED.equals(request.getParameter("error"))) {
                request.setAttribute("errorMessage", "Project cannot be deleted");
            }
            request.getRequestDispatcher("projects.jsp").forward(request, response);
        }
    }

    /**
     * Handles creating new project. Showing the form and handling the user input
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doCreateProject(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getMethod().equals("POST")) {
            Project newProject = new Project();
            String projectName = request.getParameter("name");
            
            if (projectName == null)
                throw new ServletException("Invalid data in the POST");
            
            String newProjectGithubName = request.getParameter("github");
            String newProjectBitbucketName = request.getParameter("bitbucket");
            if (newProjectBitbucketName == null || newProjectGithubName == null)
                throw new ServletException("Invalid data in the POST");
            
            newProject.setOwner(loggedUser);
            newProject.setName(projectName);
            
            Repository newRepo = new Repository();
            
            newRepo.setOwner(loggedUser);
            loggedUser.getRepositories().add(newRepo);
            
            newRepo = repositoryDAO.save(newRepo);
            
            if (!newProjectGithubName.isEmpty()) {
                GithubRepository githubRepo = this.githubDAO.getRepository(newProjectGithubName);
                if (githubRepo == null) {
                    throw new ServletException(String.format("Github repository '%s' does not exist", newProjectGithubName));
                }
                
                newRepo.setGithubRepository(githubRepo);
                
                githubDAO.save(githubRepo);
                loggedUser = userDAO.save(loggedUser);
            }
            else if (!newProjectBitbucketName.isEmpty())
            {
                BitBucketRepository bitbucketRepo = this.bitbucketDAO.get(newProjectBitbucketName);
                if (bitbucketRepo == null) {
                    throw new ServletException(String.format("BitBucket repository '%s' does not exist", newProjectBitbucketName));
                }
                
                newRepo.setBitBucketRepository(bitbucketRepo);
                
                bitbucketDAO.save(bitbucketRepo);
                loggedUser = userDAO.save(loggedUser);
            }
            newProject.setRepository(newRepo);
            
            newProject = projectDAO.createProject(newProject);
            loggedUser.getProjects().add(newProject);
            userDAO.save(loggedUser);
            
            newRepo.setProject(newProject);
            repositoryDAO.edit(newRepo);
            
            response.sendRedirect("project?id=" + newProject.getId());
        } else {
            initializeProjectPageBean(request.getSession());
            getGithubPageBean(request);
            try {
                getBitBucketPageBean(request);
            } catch (NotAuthorizedException ex) {
                System.err.println("Error while getting BitBucket page bean - " + ex.getMessage());
            }
            request.getRequestDispatcher("project-form.jsp").forward(request, response);
        }
    }
}
