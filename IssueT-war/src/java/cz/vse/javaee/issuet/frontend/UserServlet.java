package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.FrontendServlet;
import cz.vse.javaee.issuet.NoUserFoundException;
import cz.vse.javaee.issuet.model.Person;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for managing users
 * @author ondrejsatera
 */
public class UserServlet extends FrontendServlet {

    public static final String ERR_DUPLICIT = "dupl";
    public static final String ERR_MALFORMED = "malf";
    public static final String INF_CREATED = "created";
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String  action)
            throws ServletException, IOException {
        if ("create".equals(action) || "register".equals(action)) {
            doCreateUser(request, response);
        } else {
            doListUsers(request, response);
        }
    }

    /**
     * Handles creating new user account. Showing the form and handling the user input
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doCreateUser(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        if ("POST".equals(request.getMethod())) {
            String userEmail = request.getParameter("email");
            String password = request.getParameter("password");
            String bitbucketUsername = request.getParameter("bitbucketUsername");
            
            if (userEmail == null || password == null || bitbucketUsername == null)
                throw new ServletException("Invalid data in the POST");
            
            try {
                Person possibleDuplicit = userDAO.getUserByEmail(userEmail);
                if (possibleDuplicit == null) {
                    throw new NoUserFoundException("");
                }
                response.sendRedirect("user?action=create&err=" + ERR_DUPLICIT);
            } catch (NoUserFoundException ex) {
                Person createPerson = userDAO.createPerson(userEmail, password, "developer");
                createPerson.setBitbucketUsername(bitbucketUsername);

                userDAO.save(createPerson);

                response.sendRedirect(".?info=" + INF_CREATED);
            }
        } else {
            String err = request.getParameter("error");
            if (ERR_DUPLICIT.equals(err)) {
                request.setAttribute("errorMessage", "User with this email cannot be created. Other user's already using it.");
            }
            request.getRequestDispatcher("user.jsp").forward(request, response);
        }
    }

    /**
     * Shows list of all users in the application
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doListUsers(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        if (loggedUser == null) {
            response.sendRedirect(".");
            return;
        }
        
        String info = request.getParameter("info");
        if (INF_CREATED.equals(info)) {
            request.setAttribute("message", "User created");
        }
        request.setAttribute("users", userDAO.getAll());
        request.getRequestDispatcher("users.jsp").forward(request, response);
    }
    
}
