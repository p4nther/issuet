package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Project;

/**
 * Bean which provides access to active issue and project in the JSP's
 * @author ondrejsatera
 */
public class IssuePageBean {
    private Issue activeIssue;

    public Issue getActiveIssue() {
        return activeIssue;
    }

    public void setActiveIssue(Issue activeIssue) {
        this.activeIssue = activeIssue;
    }
    
    public Project getActiveProject() {
        return this.activeIssue.getProject();
    }
}
