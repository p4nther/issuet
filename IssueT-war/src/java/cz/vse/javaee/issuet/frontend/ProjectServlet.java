package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.FrontendServlet;
import cz.vse.javaee.issuet.NoUserFoundException;
import cz.vse.javaee.issuet.exceptions.NotAuthorizedException;
import cz.vse.javaee.issuet.model.BitBucketRepository;
import cz.vse.javaee.issuet.model.GithubRepository;
import cz.vse.javaee.issuet.model.Person;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.Repository;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for managing single internal project
 * @author ondrejsatera
 */
public class ProjectServlet extends FrontendServlet {

    public static final String INF_PROJECT_DELETED = "projdel";
    public static final String ERR_PROJECT_NOT_DELETED = "projnotdel";
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String action)
            throws ServletException, IOException {
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "delete":
                doDeleteProject(request, response);
                break;
            case "edit":
                doEditProject(request, response);
                break;
            case "members":
                doListProjectMembers(request, response);
                break;
            case "new-member":
                doAssignNewMember(request, response);
                break;
            case "remove-member":
                doRemoveMember(request, response);
                break;
            default:
                doPreviewProject(request, response);
        }
    }

    /**
     * Removes active project via the DAO object
     * @param request
     * @param response
     * @throws ServletException if fetching active project fails
     * @throws IOException if fetching page template fails
     */
    protected void doDeleteProject(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Project projectToBeDeleted = getActiveProject(request);
        if (projectToBeDeleted == null) {
            response.sendRedirect("projects?error=" + ERR_PROJECT_NOT_DELETED);
        } else {
            projectDAO.removeProject(projectToBeDeleted);
            response.sendRedirect("projects?info=" + INF_PROJECT_DELETED);
        }
    }

    /**
     * Returns project identified by the query parameter 'id'
     * @param request
     * @return Project|null
     */
    private Project getActiveProject(HttpServletRequest request) {
        Project result = null;
        if (null != request.getParameter("id")) {
            Long id = Long.parseLong(request.getParameter("id"));
            result = projectDAO.getProjectById(id);
        }
        return result;
    }

    /**
     * Handles editing active project. Showing the form and handling the user input
     * @param request
     * @param response
     * @throws ServletException if fetching active project fails
     * @throws IOException if fetching page template fails
     */
    protected void doEditProject(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Project project = getActiveProject(request);
        if (project == null) {
            response.sendRedirect("projects");
            return;
        }
        if (request.getMethod().equals("POST") && request.getParameter("name") != null) {
            String newProjectName = request.getParameter("name");
            String newProjectGithubName = request.getParameter("github");
            String newProjectBitbucketName = request.getParameter("bitbucket");
            
            if (newProjectName == null || newProjectBitbucketName == null || newProjectGithubName == null)
                throw new ServletException("Invalid data in the POST");
            
            Repository projectRepo = project.getRepository();
            if (projectRepo == null)
                throw new ServletException("Project repository cannot be null");
            
            if (!newProjectGithubName.isEmpty()) {
                GithubRepository githubRepo = this.githubDAO.getRepository(newProjectGithubName);
                if (githubRepo == null) {
                    throw new ServletException(
                            String.format("Github repository '%s' does not exist", newProjectGithubName)
                    );
                }
                if (projectRepo.getGithubRepository() == null 
                        || !projectRepo.getGithubRepository().equals(githubRepo))
                {
                    projectRepo.setGithubRepository(githubRepo);

                    repositoryDAO.edit(projectRepo);
                }
            }
            else if (!newProjectBitbucketName.isEmpty())
            {
                BitBucketRepository bitbucketRepo = this.bitbucketDAO.get(newProjectBitbucketName);
                if (bitbucketRepo == null) {
                    throw new ServletException(
                            String.format("BitBucket repository '%s' does not exist", newProjectGithubName)
                    );
                }
                if (projectRepo.getBitBucketRepository() == null 
                        || !projectRepo.getBitBucketRepository().equals(bitbucketRepo))
                {
                    projectRepo.setBitBucketRepository(bitbucketRepo);

                    repositoryDAO.edit(projectRepo);
                }
            }
            project.setName(newProjectName);
            projectDAO.editProject(project);
            response.sendRedirect("project?id=" + project.getId());
        } else {
            getProjectPageBean(request).setActiveProject(getActiveProject(request));
            getGithubPageBean(request);
            try {
                getBitBucketPageBean(request);
            } catch (NotAuthorizedException ex) {
                System.err.println("Error while getting BitBucket page bean - " + ex.getMessage());
            }
            request.getRequestDispatcher("project-form.jsp").forward(request, response);
        }
    }

    /**
     * Displays page with project preview
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching active project fails
     */
    protected void doPreviewProject(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Project project = getActiveProject(request);
        if (project == null) {
            System.out.println("outside");
            response.sendRedirect("projects");
        } else {
            project = projectDAO.refreshProject(project);
            getProjectPageBean(request).setActiveProject(project);
            request.getRequestDispatcher("project.jsp").forward(request, response);
        }
    }

    /**
     * Displays page with project members
     * @param request
     * @param response
     * @throws ServletException if fetching active project fails
     * @throws IOException if fetching page template fails
     */
    protected void doListProjectMembers(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        Project project = getActiveProject(request);
        if (project == null) {
            response.sendRedirect("projects");
        } else {
            project = projectDAO.refreshProject(project);
            getProjectPageBean(request).setActiveProject(project);
            request.getRequestDispatcher("project-members.jsp").forward(request, response);
        }
    }

    /**
     * Handles assigning new user to the project team members. 
     * Showing the form and handling the user input. Sends email notification to the user.
     * @param request
     * @param response
     * @throws ServletException if fetching active project fails
     * @throws IOException if fetching page template fails
     */
    protected void doAssignNewMember(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        Project project = getActiveProject(request);
        if (project == null) {
            response.sendRedirect("projects");
        } else {
            if ("POST".equals(request.getMethod())) {
                String newMemberEmail = request.getParameter("user");
                if (newMemberEmail == null)
                    throw new ServletException("Invalid data in the POST");
                
                Person newMember;
                try {
                    newMember = userDAO.getUserByEmail(newMemberEmail);
                    newMember.getTeamProjects().add(project);
                    project.getMembers().add(newMember);

                    projectDAO.editProject(project);
                    userDAO.save(newMember);
                } catch (NoUserFoundException ex) {
                    throw new ServletException(ex);
                }
                if (project.hasMember(newMember)) {
                    response.sendRedirect("project?id=" + project.getId() + "&action=new-member");
                }
                
                String subject = "Assigned to project - IssueT - Integrated Issue Tracker";
                String emailBodyTmpl = "Hi %s,\n\nyou have been assigned to the project %s.\n\nBest,\nIssueT";
                String emailBody = String.format(emailBodyTmpl, newMember.getEmail(), project.getName());
                try {
                    this.sendEmail(newMember.getEmail(), subject, emailBody);
                } catch (ServletException ex) {
                    System.err.println("E-mail failed to sent: " + ex.getMessage());
                }
                System.out.println("Notification email sent to " + newMember.getEmail());
                
                response.sendRedirect("project?id=" + project.getId() + "&action=members");
            } else {
                project = projectDAO.refreshProject(project);
                getProjectPageBean(request).setActiveProject(project);
                request.setAttribute("users", userDAO.getAll());
                request.getRequestDispatcher("project-new-member.jsp").forward(request, response);
            }
        }
    }

    /**
     * Handles removing user from the project team members
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching active project fails
     */
    protected void doRemoveMember(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, ServletException {
        Project project = getActiveProject(request);
        if (project == null || !"POST".equals(request.getMethod())) {
            response.sendRedirect("projects");
        } else {
            String personEmail = request.getParameter("person");
            Person member;
            try {
                member = userDAO.getUserByEmail(personEmail);
                
                member.removeTeamProject(project);
                project.removeMember(member);

                projectDAO.editProject(project);
                userDAO.save(member);
            } catch (NoUserFoundException ex) {
                throw new ServletException(ex);
            }
            
            String subject = "Removed from project - IssueT - Integrated Issue Tracker";
            String emailBodyTmpl = "Hi %s,\n\nyou have been removed from the  project %s.\n\nBest,\nIssueT";
            String emailBody = String.format(emailBodyTmpl, member.getEmail(), project.getName());
            try {
                this.sendEmail(member.getEmail(), subject, emailBody);
            } catch (ServletException ex) {
                System.err.println("E-mail failed to sent: " + ex.getMessage());
            }
            System.out.println("Notification email sent to " + member.getEmail());

            response.sendRedirect("project?id=" + project.getId() + "&action=members");
        }
    }
}
