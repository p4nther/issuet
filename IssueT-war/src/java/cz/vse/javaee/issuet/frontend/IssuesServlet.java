package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.FrontendServlet;
import cz.vse.javaee.issuet.exceptions.NotAuthorizedException;
import cz.vse.javaee.issuet.model.BitBucketIssue;
import cz.vse.javaee.issuet.model.BitBucketRepository;
import cz.vse.javaee.issuet.model.GithubIssue;
import cz.vse.javaee.issuet.model.GithubRepository;
import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Person;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.Repository;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.egit.github.core.client.RequestException;
import org.scribe.model.Token;

/**
 * Servlet for managing issues
 * @author ondrejsatera
 */
public class IssuesServlet extends FrontendServlet {
    
    private final static String ERR_BITBUCKET_AUTH = "bbnoauth";
    private final static String ERR_GITHUB_AUTH = "ggnoauth";
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String action)
            throws ServletException, IOException {
        if ("create".equals(action)) {
            doCreateIssue(request, response);
        } else if ("import".equals(action)) {
            doImportIssues(request, response);
        } else {
            String filter;
            if (action == null)
                action = "";
            switch (action) {
                case "all":
                    filter = "all";
                    break;
                case "only-opened":
                    filter = "open";
                    break;
                case "only-closed":
                    filter = "closed";
                    break;
                case "search":
                    filter = request.getParameter("entry").length() == 0 ? "all" : request.getParameter("entry");
                    break;
                default:
                    filter = "open";
            }
            doListIssues(request, response, filter);
        }
    }

    /**
     * Handles creating new issue. Showing the form and handling the user input
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    protected void doCreateIssue(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Project project = getActiveProject(request);
        if (project == null) {
            response.sendRedirect("projects");
            return;
        }
        
        if (request.getMethod().equals("POST")) {
            String issueName = request.getParameter("name");
            String issueBody = request.getParameter("body");
            
            Issue newIssue = new Issue();
            
            newIssue.setName(issueName);
            newIssue.setProject(project);
            newIssue.setLocalOwner(loggedUser);
            newIssue.setLocalBody(issueBody);
            newIssue.setLocalState("open");
            newIssue.setRepository(project.getRepository());
            
            newIssue = issueDAO.createIssue(newIssue);
            
            loggedUser.getIssues().add(newIssue);
            userDAO.save(loggedUser);
            
            if (project.getRepository().getGithubRepository() != null) {
                newIssue.setGithubIssue(getGithubPageBean(request).createIssue(newIssue, project.getRepository().getGithubRepository()));
                githubDAO.saveIssue(newIssue.getGithubIssue());
                
                newIssue.getGithubIssue().setIssue(newIssue);
                issueDAO.editIssue(newIssue);
            }
            
            if (project.getRepository().getBitBucketRepository() != null) {
                try {
                    newIssue.setBitbucketIssue(getBitBucketPageBean(request).createIssue(newIssue, getBitBucketToken(request), project.getRepository().getBitBucketRepository(), loggedUser.getBitbucketUsername()));
                } catch (NotAuthorizedException ex) {
                    throw new ServletException(ex);
                }
                
                bitbucketDAO.saveIssue(newIssue.getBitbucketIssue());
                
                newIssue.getBitbucketIssue().setIssue(newIssue);
                issueDAO.editIssue(newIssue);
            }
            
//            project.getIssues().add(newIssue);
//            projectDAO.editProject(project);
         
            String subject = "New issue - IssueT - Integrated Issue Tracker";
            String emailBodyTmpl = "Hi %s,\n\nin the project %s has been added a new issue: %s.\n\nBest,\nIssueT";
            for (Person member : project.getMembers()) {
                String emailBody = String.format(emailBodyTmpl, member.getEmail(), project.getName(), newIssue.getName());
                try {
                    this.sendEmail(member.getEmail(), subject, emailBody);
                } catch (ServletException ex) {
                    System.err.println("E-mail failed to sent: " + ex.getMessage());
                    break;
                }
                System.out.println("Notification email sent to " + member.getEmail());
            }

            response.sendRedirect("issue?id=" + newIssue.getId() + "&projectId=" + getActiveProject(request).getId());
        } else {
            initializeProjectPageBean(request.getSession());
            initializeIssuePageBean(request.getSession());
            getProjectPageBean(request).setActiveProject(getActiveProject(request));
            request.getRequestDispatcher("issue-form.jsp").forward(request, response);
        }
    }
    
    /**
     * Returns project identified by the query parameter 'id'
     * @param request
     * @return Project|null
     */
    private Project getActiveProject(HttpServletRequest request) {
        Project result = null;
        if (null != request.getParameter("projectId")) {
            Long id = Long.parseLong(request.getParameter("projectId"));
            result = projectDAO.getProjectById(id);
        }
        return result;
    }
    
    /**
     * Returns project identifier from the request params
     * @param request
     * @return project identifier or zero (0) if not found
     */
    private Long getActiveProjectId(HttpServletRequest request) {
        Long result = Long.parseLong("0");
        if (null != request.getParameter("projectId")) {
            result = Long.parseLong(request.getParameter("projectId"));
        }
        return result;
    }

    /**
     * Displays page with active project's issue list
     * @param request
     * @param response
     * @param filter
     * @throws ServletException
     * @throws IOException
     */
    protected void doListIssues(HttpServletRequest request, HttpServletResponse response, String filter)
            throws ServletException, IOException {
        Project project = getActiveProject(request);
        if (null == project)
            response.sendRedirect("projects");
        
        project = projectDAO.refreshProject(project);
        getProjectPageBean(request).setActiveProject(project);
        request.setAttribute("filter", filter);
        if ("all".equals(filter)) {
            request.setAttribute("issues", project.getIssues());
        } else {
            request.setAttribute("issues", issueDAO.getIssues(project, filter));
        }
        if (ERR_BITBUCKET_AUTH.equals(request.getParameter("err")))
            request.setAttribute("errorMessage", "BitBucket is not authorized or the repository is not accessible");
        if (ERR_GITHUB_AUTH.equals(request.getParameter("err")))
            request.setAttribute("errorMessage", "GitHub is not authorized or the repository is not accessible");
        
        request.getRequestDispatcher("issues.jsp").forward(request, response);
    }

    /**
     * Handles importing new specific repository issues in the active project
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    protected void doImportIssues(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, ServletException {
        Project activeProject = getActiveProject(request);
        Repository activeProjectRepository = activeProject.getRepository();
        GithubRepository githubRepository = activeProjectRepository.getGithubRepository();
        BitBucketRepository bitbucketRepository = activeProjectRepository.getBitBucketRepository();
        
        if (githubRepository != null) {
            String TOKEN = (String) request.getSession().getAttribute(GithubServlet.GITHUB_TOKEN);
        
            if (TOKEN == null || TOKEN.isEmpty()) {
                TOKEN = loggedUser.getGithubToken();
                if (TOKEN == null || TOKEN.isEmpty()) {
                    response.sendRedirect("issues?projectId=" + getActiveProjectId(request));
                } else {
                    request.getSession().setAttribute(GithubServlet.GITHUB_TOKEN, TOKEN);
                }
            }
            
            try {
                List<org.eclipse.egit.github.core.Issue> issues = getGithubPageBean(request).getIssues(githubRepository);
                
                importGithubIssues(issues, activeProject, activeProjectRepository, githubRepository);
            } catch (ServletException | RequestException ex) {
                response.sendRedirect("issues?projectId=" + getActiveProjectId(request) + "&err=" + ERR_GITHUB_AUTH);
                return;
            }
        } else if (bitbucketRepository != null) {
            Token accessToken = (Token) request.getSession().getAttribute(BitBucketServlet.BITBUCKET_ACCESS_TOKEN);
            if (accessToken == null)
                accessToken = loggedUser.getBitbucketToken();
            if (accessToken == null) {
                response.sendRedirect("issues?projectId=" + getActiveProjectId(request) + "&err=" + ERR_BITBUCKET_AUTH);
                return;
            }
            try {
                importBitBucketIssues(getBitBucketPageBean(request), accessToken, activeProject, activeProjectRepository, bitbucketRepository);
            } catch (NotAuthorizedException ex) {
                throw new ServletException(ex);
            }
        }
        
        response.sendRedirect("issues?projectId=" + getActiveProjectId(request));
    }

    /**
     * Handles importing new Github issues from the given repository in given active project
     * @param issues
     * @param activeProject
     * @param activeProjectRepository
     * @param githubRepository
     * @throws IOException if fetching page template fails
     */
    private void importGithubIssues(List<org.eclipse.egit.github.core.Issue> issues, Project activeProject, Repository activeProjectRepository, GithubRepository githubRepository) 
            throws IOException {
        System.out.println("Received " + issues.size() + " github issues");
        for (org.eclipse.egit.github.core.Issue repositoryIssue : issues) {
            if (githubDAO.getIssue(repositoryIssue.getNumber()) != null)
                continue;
            
            Issue issue = new Issue();
            GithubIssue githubIssue = new GithubIssue();
            
            githubIssue.setNumber(repositoryIssue.getNumber());
            githubIssue.setBody(repositoryIssue.getBody());
            githubIssue.setState(repositoryIssue.getState());
            githubIssue.setTitle(repositoryIssue.getTitle());
            githubIssue.setRepository(githubRepository);
            
            githubIssue = githubDAO.saveIssue(githubIssue);
            
            issue.setGithubIssue(githubIssue);
            issue.setName(githubIssue.getTitle());
            issue.setProject(activeProject);
            issue.setRepository(activeProjectRepository);
            
            issue = issueDAO.createIssue(issue);
            
            githubIssue.setIssue(issue);
            githubDAO.saveIssue(githubIssue);
            
            githubRepository.getIssues().add(githubIssue);
            githubDAO.save(githubRepository);
            
            System.out.println("Saving issue: " + issue.getName());
        }
    }

    /**
     * Handles importing new BitBucket issues from the given repository in given active project
     * @param bitbucketPageBean
     * @param accessToken
     * @param activeProject
     * @param activeProjectRepository
     * @param bitbucketRepository
     * @throws ServletException if fetching issues fails
     */
    private void importBitBucketIssues(BitBucketPageBean bitbucketPageBean, Token accessToken, Project activeProject, Repository activeProjectRepository, BitBucketRepository bitbucketRepository) 
            throws ServletException {
        List<BitBucketIssue> issues = bitbucketPageBean.getRepositoryIssues(bitbucketRepository, accessToken, loggedUser.getBitbucketUsername());
        for (BitBucketIssue bitBucketIssue : issues) {
            if (bitbucketDAO.getIssue(bitBucketIssue.getLocalId()) != null)
                continue;
            
            bitbucketDAO.saveIssue(bitBucketIssue);
            
            Issue issue = new Issue();
            
            issue.setName(bitBucketIssue.getTitle());
            issue.setProject(activeProject);
            issue.setRepository(activeProjectRepository);
            issue.setBitbucketIssue(bitBucketIssue);
            
            issue = issueDAO.createIssue(issue);
            
            bitBucketIssue.setIssue(issue);
            bitbucketDAO.saveIssue(bitBucketIssue);
            
            bitbucketRepository.getIssues().add(bitBucketIssue);
            bitbucketDAO.save(bitbucketRepository);
        }
    }
}
