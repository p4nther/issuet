package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.BitbucketAPI;
import cz.vse.javaee.issuet.FrontendServlet;
import cz.vse.javaee.issuet.exceptions.NotAuthorizedException;
import cz.vse.javaee.issuet.model.BitBucketRepository;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

/**
 * Servlet for authorizing user in the BitBucket API using Scribe library
 * @author ondrejsatera
 */
public class BitBucketServlet extends FrontendServlet {
    public static final String BITBUCKET_REQUEST_TOKEN = "bitbucketRequestToken";
    public static final String BITBUCKET_ACCESS_TOKEN = "bitbucketAccessToken";
    public static final String BITBUCKET_PAGE_BEAN = "bitbucketPageBean";
    public static final String BITBUCKET_API_SERVICE = "bitbucketAPIService";
    
    public static final String INFO_NOT_AUTHORIZED = "noauth";

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String action)
            throws ServletException, IOException {
        if ("callback".equals(action)) {
            doProcessCallback(request, response);
        } else if ("auth".equals(action)) {
            request.getSession().setAttribute(BITBUCKET_ACCESS_TOKEN, null);
            doAuthorizeUser(request, response);
        } else if ("refresh".equals(action)) {
            doRefreshRepositories(request, response);
        } else {
            Token accessToken = (Token) request.getSession().getAttribute(BITBUCKET_ACCESS_TOKEN);
            if (accessToken == null) {
                doAuthorizeUser(request, response);
            } else {
                doShowDashboard(request, response);
            }
        }
    }
    
    /**
     * Redirects user to BitBucket website to grant permissions to this application
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if redirect fails
     */
    protected void doAuthorizeUser(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, ServletException {
        Token requestToken = getService().getRequestToken();
        
        if (requestToken == null) {
            throw new ServletException("Failed to create request token");
        }
        
        request.getSession().setAttribute(BITBUCKET_REQUEST_TOKEN, requestToken);
        
        String authUrl = getService().getAuthorizationUrl(requestToken);
        
        response.sendRedirect(authUrl);
    }
    
    /**
     * Handles BitBucket API authorization response and save access token to logged user
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching BitBucket page bean fails
     */
    protected void doProcessCallback(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, ServletException {
        String oauthVerifier = request.getParameter("oauth_verifier");
        if (oauthVerifier == null)
            throw new ServletException("oauth_verifier not set");

        Token requestToken = (Token) request.getSession().getAttribute(BITBUCKET_REQUEST_TOKEN);
        
        if (requestToken == null) {
            throw new ServletException("Request token is missing");
        }
        
        Verifier v = new Verifier(oauthVerifier);
        Token accessToken = getService().getAccessToken(requestToken, v);
        
        BitBucketPageBean bitBucketPageBean = new BitBucketPageBean(getService(), bitbucketDAO);
        
        userDAO.saveBitbucketToken(loggedUser, accessToken);
        request.getSession().setAttribute(BITBUCKET_ACCESS_TOKEN, accessToken);
        request.getSession().setAttribute(BITBUCKET_PAGE_BEAN, bitBucketPageBean);
        
        response.sendRedirect("bitbucket");
    }

    /**
     * Displays page with information about authorized user
     * @param request
     * @param response
     * @throws ServletException if fetching BitBucket page bean fails
     * @throws IOException if fetching page template fails
     */
    protected void doShowDashboard(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        try {
            getBitBucketPageBean(request);
        } catch (NotAuthorizedException ex) {
            doAuthorizeUser(request, response);
            return;
        }

        request.getRequestDispatcher("bitbucket.jsp").forward(request, response);
    }
    
    /**
     * Returns access token if any
     * @param request
     * @return access token
     */
    private Token getAccessToken(HttpServletRequest request) {
        return (Token) request.getSession().getAttribute(BITBUCKET_ACCESS_TOKEN);
    }
    
    /**
     * Returns OAuth service created in the FrontendServlet
     * @return OAuthService|null
     */
    private OAuthService getService() {
        return this.bitbucketOAuthService;
    }

    /**
     * Fetches new BitBucket repositories from the API
     * @param request
     * @param response
     * @throws ServletException if fetching BitBucket page bean fails
     * @throws IOException if fetching page template fails
     */
    protected void doRefreshRepositories(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        String queryUrl = BitbucketAPI.formatUrl(new String[]{
            "2.0", 
            "repositories", 
            loggedUser.getBitbucketUsername()
        });
        OAuthRequest oauthRequest = new OAuthRequest(Verb.GET, queryUrl);
        getService().signRequest(getAccessToken(request), oauthRequest);

        Response oauthResponse = oauthRequest.send();
        if (oauthResponse == null)
            throw new ServletException("Server responded with malformed data");
        
        BitBucketPageBean bean;
        try {
            bean = getBitBucketPageBean(request);
        } catch (NotAuthorizedException ex) {
            doAuthorizeUser(request, response);
            return;
        }
        if (bean == null)
            throw new ServletException("Failed to fetch BitBucket page bean");
        
        List<BitBucketRepository> repos = bean.parseRepositoriesFromResponse(oauthResponse);
        for (BitBucketRepository bitBucketRepository : repos) {
            bitBucketRepository.setOwner(loggedUser);
            bitBucketRepository = bitbucketDAO.save(bitBucketRepository);
        }
        
        response.sendRedirect("bitbucket");
    }
}
