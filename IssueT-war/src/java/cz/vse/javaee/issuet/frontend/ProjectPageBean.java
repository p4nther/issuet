package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.model.Project;

/**
 * Bean which provides access to active project in the JSP's
 * @author ondrejsatera
 */
public class ProjectPageBean {
    protected Project activeProject = null;
    
    public Project getActiveProject() {
        return this.activeProject;
    }

    public void setActiveProject(Project activeProject) {
        this.activeProject = activeProject;
    }
}
