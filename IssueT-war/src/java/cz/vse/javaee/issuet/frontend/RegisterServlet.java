package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.NoUserFoundException;
import cz.vse.javaee.issuet.UserDAOBeanLocal;
import cz.vse.javaee.issuet.model.Person;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet used for registering new user accounts
 * @author ondrejsatera
 */
public class RegisterServlet extends HttpServlet {

    @EJB
    protected UserDAOBeanLocal userDAO;
    
    /**
     * Handles the HTTP <code>GET</code> method - displaying user registration form
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("register.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method - handling user registration form
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userEmail = request.getParameter("email");
        String password = request.getParameter("password");
        String bitbucketUsername = request.getParameter("bitbucketUsername");
        
        if (userEmail == null || password == null || bitbucketUsername == null)
            throw new ServletException("Invalid data in the POST");
        
        try {
            Person possibleDuplicit = userDAO.getUserByEmail(userEmail);
            if (possibleDuplicit == null) {
                throw new NoUserFoundException("");
            }
            response.sendRedirect("user?action=create&err=" + UserServlet.ERR_DUPLICIT);
        } catch (NoUserFoundException ex) {
            Person createPerson = userDAO.createPerson(userEmail, password, "developer");
            createPerson.setBitbucketUsername(bitbucketUsername);
            
            userDAO.save(createPerson);

            response.sendRedirect(".?info=" + UserServlet.INF_CREATED);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet for user registration";
    }

}
