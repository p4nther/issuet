package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.FrontendServlet;
import cz.vse.javaee.issuet.GitHubClient;
import cz.vse.javaee.issuet.exceptions.NotAuthorizedException;
import cz.vse.javaee.issuet.model.BitBucketIssue;
import cz.vse.javaee.issuet.model.BitBucketRepository;
import cz.vse.javaee.issuet.model.GithubIssue;
import cz.vse.javaee.issuet.model.GithubRepository;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.Repository;
import cz.vse.javaee.issuet.model.RepositoryCommit;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.egit.github.core.IRepositoryIdProvider;
import org.eclipse.egit.github.core.RepositoryId;
import org.eclipse.egit.github.core.service.CommitService;
import org.eclipse.egit.github.core.service.UserService;
import org.scribe.model.Token;

/**
 * Servlet for managing commits in the repositories
 * @author ondrejsatera
 */
public class CommitsServlet extends FrontendServlet {
    
    private final static String ERR_BITBUCKET_AUTH = "bbnoauth";
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String action)
            throws ServletException, IOException {
        if ("import".equals(action)) {
            doImportCommits(request, response);
        } else if ("assign".equals(action)) {
            doAssignCommitsToIssues(request, response);
        } else {
            doListCommits(request, response);
        }
    }

    /**
     * Displays page with a list of active project commits
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching active project fails
     */
    protected void doListCommits(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Project activeProject;
        try {
            activeProject = getActiveProject(request);
        } catch (IllegalStateException ex) {
            response.sendRedirect("projects");
            return;
        }
        
        Project project = projectDAO.refreshProject(activeProject);
        getProjectPageBean(request).setActiveProject(project);
        
        if (ERR_BITBUCKET_AUTH.equals(request.getParameter("err")))
            request.setAttribute("errorMessage", "BitBucket is not authorized");
        
        request.getRequestDispatcher("commits.jsp").forward(request, response);
    }

    /**
     * Handles importing specific repository commits in the active project
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching active project fails
     */
    protected void doImportCommits(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Project activeProject;
        try {
            activeProject = getActiveProject(request);
        } catch (IllegalStateException ex) {
            response.sendRedirect("projects");
            return;
        }
        
        Repository activeProjectRepository = activeProject.getRepository();
        GithubRepository githubRepository = activeProjectRepository.getGithubRepository();
        BitBucketRepository bitbucketRepository = activeProjectRepository.getBitBucketRepository();
        
        if (githubRepository != null) {
            String TOKEN = (String) request.getSession().getAttribute(GithubServlet.GITHUB_TOKEN);
        
            if (TOKEN == null || TOKEN.isEmpty()) {
                TOKEN = loggedUser.getGithubToken();
                if (TOKEN == null || TOKEN.isEmpty()) {
                    response.sendRedirect("commits?projectId=" + getActiveProjectId(request));
                } else {
                    request.getSession().setAttribute(GithubServlet.GITHUB_TOKEN, TOKEN);
                }
            }
            GitHubClient client = new GitHubClient().setOAuth2Token(TOKEN);
            
            CommitService commitService = new CommitService(client);
            commitService.getClient().setOAuth2Token(TOKEN);
            
            UserService userService = new UserService(client);
            userService.getClient().setOAuth2Token(TOKEN);
            
            importGithubCommits(commitService, userService, activeProjectRepository, githubRepository);
        } else if (bitbucketRepository != null) {
            Token accessToken = (Token) request.getSession().getAttribute(BitBucketServlet.BITBUCKET_ACCESS_TOKEN);
            if (accessToken == null)
                accessToken = loggedUser.getBitbucketToken();
            if (accessToken == null) {
                response.sendRedirect("commits?projectId=" + getActiveProjectId(request) + "&err=" + ERR_BITBUCKET_AUTH);
            }
            try {
                importBitBucketCommits(getBitBucketPageBean(request), accessToken, activeProjectRepository, bitbucketRepository);
            } catch (NotAuthorizedException ex) {
                throw new ServletException(ex);
            }
        }
        response.sendRedirect("commits?projectId="+getActiveProjectId(request));
    }
    
    /**
     * Returns active project based on request parameter 'projectId'
     * @param request
     * @return Project
     * @throws IllegalStateException if project identifier is not available
     */
    private Project getActiveProject(HttpServletRequest request) throws IllegalStateException {
        if (null == request.getParameter("projectId")) {
            throw new IllegalStateException("Project identifier is missing");
        } else {
            Long id = Long.parseLong(request.getParameter("projectId"));
            return projectDAO.getProjectById(id);
        }
    }
    
    /**
     * Return project identifier based on request parameter 'projectId'
     * @param request
     * @return project identifier or zero if not found
     */
    private Long getActiveProjectId(HttpServletRequest request) {
        Long result = Long.parseLong("0");
        if (null != request.getParameter("projectId")) {
            result = Long.parseLong(request.getParameter("projectId"));
        }
        return result;
    }

    /**
     * Handles importing Github API commits in the active project's repository using given eGit commit service
     * @param commitService
     * @param userService
     * @param activeProjectRepository
     * @param githubRepository
     * @throws IOException if fetching page template fails
     */
    private void importGithubCommits(CommitService commitService, UserService userService, Repository activeProjectRepository, GithubRepository githubRepository)
            throws IOException {
        IRepositoryIdProvider idProvider = new RepositoryId(userService.getUser().getLogin(), githubRepository.getName());
        List<org.eclipse.egit.github.core.RepositoryCommit> commits = commitService.getCommits(idProvider);
        for (org.eclipse.egit.github.core.RepositoryCommit repositoryCommit : commits) {
            if (githubDAO.getCommit(repositoryCommit.getSha()) != null)
                continue;
            
            RepositoryCommit newCommit = new RepositoryCommit();
            newCommit.setSha(repositoryCommit.getSha());
            newCommit.setTitle(repositoryCommit.getCommit().getMessage());
            newCommit.setCommiter(repositoryCommit.getAuthor().getEmail());
            newCommit.setRepository(activeProjectRepository);
            
            newCommit = githubDAO.saveCommit(newCommit);
            
            if (newCommit.getTitle().contains("#")) {
                GithubIssue issue = null;
                Matcher matcher = Pattern.compile("#(\\d)\\s").matcher(newCommit.getTitle());
                while(matcher.find()) {
                    Integer number = Integer.parseInt(matcher.group(1));                    
                    issue = githubDAO.getIssue(number);
                    if (issue != null)
                    {
//                        newCommit.getGithubIssue().put(number, issue);
                        issue.getCommits().add(newCommit);
                    }
                }
                if (issue != null) {
//                    githubDAO.saveCommit(newCommit);
                    githubDAO.saveIssue(issue);
                }
            }
            
            activeProjectRepository.getCommits().add(newCommit);
            repositoryDAO.save(activeProjectRepository);
        }
    }

    /**
     * Handles importing BitBucket API commits in the active project's repository using given page bean
     * @param bitBucketPageBean
     * @param accessToken
     * @param activeProjectRepository
     * @param bitbucketRepository
     * @throws ServletException if fetching active project fails
     */
    private void importBitBucketCommits(BitBucketPageBean bitBucketPageBean, Token accessToken, Repository activeProjectRepository, BitBucketRepository bitbucketRepository) 
            throws ServletException {
        List<RepositoryCommit> commits = bitBucketPageBean.getRepositoryCommits(bitbucketRepository, accessToken, loggedUser.getBitbucketUsername());
        for (RepositoryCommit repositoryCommit : commits) {
            if (bitbucketDAO.getCommit(repositoryCommit.getSha()) != null)
                continue;
            
            repositoryCommit.setRepository(activeProjectRepository);
            repositoryCommit = bitbucketDAO.saveCommit(repositoryCommit);
            
            if (repositoryCommit.getTitle().contains("#")) {
                BitBucketIssue issue = null;
                Matcher matcher = Pattern.compile("#(\\d)\\s").matcher(repositoryCommit.getTitle());
                while(matcher.find()) {
                    Long number = Long.parseLong(matcher.group(1));
                    System.out.println("Hledam issue pro commit: " + repositoryCommit.getTitle() + ", " + number);
                    issue = bitbucketDAO.getIssue(number);
                    if (issue != null)
                    {
//                        repositoryCommit.getBitbucketIssue().put(number, issue);
                        issue.getCommits().add(repositoryCommit);
                    }
                }
                if (issue != null) {
//                    bitbucketDAO.saveCommit(repositoryCommit);
                    bitbucketDAO.saveIssue(issue);
                }
            }
            
            activeProjectRepository.getCommits().add(repositoryCommit);
            repositoryDAO.save(activeProjectRepository);
        }
    }

    /**
     * Handles assigning commits to the repository issues based on identifiers in the commit message
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching active project fails
     */
    protected void doAssignCommitsToIssues(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Project activeProject;
        try {
            activeProject = getActiveProject(request);
        } catch (IllegalStateException ex) {
            response.sendRedirect("projects");
            return;
        }
        
        Repository activeProjectRepository = activeProject.getRepository();
        GithubRepository githubRepository = activeProjectRepository.getGithubRepository();
        BitBucketRepository bitbucketRepository = activeProjectRepository.getBitBucketRepository();
        
        if (githubRepository != null) {
            String TOKEN = (String) request.getSession().getAttribute(GithubServlet.GITHUB_TOKEN);
        
            if (TOKEN == null || TOKEN.isEmpty()) {
                TOKEN = loggedUser.getGithubToken();
                if (TOKEN == null || TOKEN.isEmpty()) {
                    response.sendRedirect("commits?projectId=" + getActiveProjectId(request));
                } else {
                    request.getSession().setAttribute(GithubServlet.GITHUB_TOKEN, TOKEN);
                }
            }
            
            assignGithubCommits(activeProjectRepository);
        } else if (bitbucketRepository != null) {
            Token accessToken = (Token) request.getSession().getAttribute(BitBucketServlet.BITBUCKET_ACCESS_TOKEN);
            if (accessToken == null)
                response.sendRedirect("commits?projectId=" + getActiveProjectId(request));
            
            assignBitBucketCommits(activeProjectRepository);
        }
        response.sendRedirect("commits?projectId="+getActiveProjectId(request));
    }

    /**
     * Handles assigning Github commits to the repository issues based on identifiers in the commit message
     * @param activeProjectRepository
     */
    private void assignGithubCommits(Repository activeProjectRepository) {
        GithubIssue issue = null;
        boolean save;
        List<RepositoryCommit> commits = activeProjectRepository.getCommits();
        for (RepositoryCommit repositoryCommit : commits) {
            if (repositoryCommit.getTitle().contains("#")) {
                Matcher matcher = Pattern.compile("#(\\d)\\s").matcher(repositoryCommit.getTitle());
                save = false;
                while(matcher.find()) {
                    Integer number = Integer.parseInt(matcher.group(1));
                    issue = githubDAO.getIssue(number);
                    if (issue != null && !githubDAO.issueHasCommitAssigned(issue, repositoryCommit))
                    {
                        issue.getCommits().add(repositoryCommit);
                        save = true;
                    }
                }
                if (save && issue != null) {
                    githubDAO.saveIssue(issue);
                }
            }
        }
    }

    /**
     * Handles assigning BitBucket commits to the repository issues based on identifiers in the commit message
     * @param activeProjectRepository
     */
    private void assignBitBucketCommits(Repository activeProjectRepository) {
        BitBucketIssue issue = null;
        boolean save;
        List<RepositoryCommit> commits = activeProjectRepository.getCommits();
        for (RepositoryCommit repositoryCommit : commits) {
            if (repositoryCommit.getTitle().contains("#")) {
                Matcher matcher = Pattern.compile("#(\\d)\\s").matcher(repositoryCommit.getTitle());
                save = false;
                while(matcher.find()) {
                    Long number = Long.parseLong(matcher.group(1));
                    issue = bitbucketDAO.getIssue(number);
                    if (issue != null && !bitbucketDAO.issueHasCommitAssigned(issue, repositoryCommit))
                    {
                        issue.getCommits().add(repositoryCommit);
                        save = true;
                    }
                }
                if (save) {
                    bitbucketDAO.saveIssue(issue);
                }
            }
        }
    }

    
}
