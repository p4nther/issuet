package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.FrontendServlet;
import cz.vse.javaee.issuet.exceptions.NotAuthorizedException;
import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Person;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.RepositoryCommit;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.scribe.model.Token;

/**
 * Servlet for managing single internal issue
 * @author ondrejsatera
 */
public class IssueServlet extends FrontendServlet {
    
    public final static String INF_CLOSED = "cls";
    public final static String ERR_BITBUCKET_NOT_AUTHORIZED = "bitbnoauth";
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String action)
            throws ServletException, IOException {
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "delete":
                doDeleteIssue(request, response);
                break;
            case "edit":
                doEditIssue(request, response);
                break;
            case "assign-commit":
                doAssignCommit(request, response);
                break;
            case "close":
                doCloseIssue(request, response);
                break;
            default:
                doPreviewIssue(request, response);
        }
    }

    /**
     * Displays page with active issue preview
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching active issue/project fails
     */
    protected void doPreviewIssue(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (getActiveProject(request) == null) {
            response.sendRedirect("projects");
        } else if (getActiveIssue(request) == null) {
            response.sendRedirect("issues?projectId=" + getActiveProject(request).getId());
        } else {
            this.getIssuePageBean(request).setActiveIssue(getActiveIssue(request));
            if (INF_CLOSED.equals(request.getParameter("info"))) {
                request.setAttribute("message", "Issue has been closed");
            } else if (ERR_BITBUCKET_NOT_AUTHORIZED.equals(request.getParameter("error"))) {
                request.setAttribute("errorMessage", "BitBucket not authorized");
            }
            request.getRequestDispatcher("issue.jsp").forward(request, response);
        }
    }

    /**
     * Handles editing active issue. Showing the form and handling the user input
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching active issue/project fails
     */
    protected void doEditIssue(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Issue issue = getActiveIssue(request);
        if (issue == null) {
            response.sendRedirect("issues?projectId="+getActiveProject(request).getId());
            return;
        }
        if (request.getMethod().equals("POST")) {
            String newIssueName = request.getParameter("name");
            String newIssueBody = request.getParameter("body");
            
            if (newIssueBody == null || newIssueName == null)
                throw new ServletException("Invalid data in POST");
            
            issue.setName(newIssueName);
            issue.setLocalBody(newIssueBody);
            
            issueDAO.editIssue(issue);
            
            String subject = "Issue updated - IssueT - Integrated Issue Tracker";
            String emailBodyTmpl = "Hi %s,\n\nin the project %s has been updated the issue: %s.\n\nBest,\nIssueT";
            for (Person member : issue.getProject().getMembers()) {
                String emailBody = String.format(emailBodyTmpl, member.getEmail(), issue.getProject().getName(), issue.getName());
                try {
                    this.sendEmail(member.getEmail(), subject, emailBody);
                } catch (ServletException ex) {
                    System.err.println("E-mail failed to sent: " + ex.getMessage());
                    break;
                }
                System.out.println("Notification email sent to " + member.getEmail());
            }
            
            response.sendRedirect("issue?id="+issue.getId()+"&projectId="+getActiveProject(request).getId());
        } else {
            getProjectPageBean(request).setActiveProject(getActiveProject(request));
            getIssuePageBean(request).setActiveIssue(issue);
            request.getRequestDispatcher("issue-form.jsp").forward(request, response);
        }
    }

    /**
     * Handles removing active isusue
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     */
    protected void doDeleteIssue(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Issue issueToBeDeleted = getActiveIssue(request);
        if (issueToBeDeleted != null) {
            String subject = "Issue deleted - IssueT - Integrated Issue Tracker";
            String emailBodyTmpl = "Hi %s,\n\nin the project %s has been deleted the issue: %s.\n\nBest,\nIssueT";
            for (Person member : issueToBeDeleted.getProject().getMembers()) {
                String emailBody = String.format(emailBodyTmpl, member.getEmail(), issueToBeDeleted.getProject().getName(), issueToBeDeleted.getName());
                try {
                    this.sendEmail(member.getEmail(), subject, emailBody);
                } catch (ServletException ex) {
                    System.err.println("E-mail failed to sent: " + ex.getMessage());
                    break;
                }
                System.out.println("Notification email sent to " + member.getEmail());
            }
            issueDAO.removeIssue(issueToBeDeleted);
        }
        response.sendRedirect("issues?projectId="+getActiveProject(request).getId());
    }
    
    /**
     * Returns active issue based on request parameter 'id'
     * @param request
     * @return Issue|null
     */
    private Issue getActiveIssue(HttpServletRequest request) {
        Issue result = null;
        if (null != request.getParameter("id")) {
            Long id = Long.parseLong(request.getParameter("id"));
            result = issueDAO.getIssueById(id);
        }
        return result;
    }
    
    /**
     * Returns active project based on request parameter 'projectId'
     * @param request
     * @return Project|null
     */
    private Project getActiveProject(HttpServletRequest request) {
        Issue issue = this.getActiveIssue(request);
        if (issue == null && null != request.getParameter("projectId")) {
            Long id = Long.parseLong(request.getParameter("projectId"));
            return projectDAO.getProjectById(id);
        } else {
            return issue == null ? null : issue.getProject();
        }
    }

    /**
     * Handles manual assigning a commit to the active issue. Showing the form and handling the user input
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching active issue/project fails
     */
    protected void doAssignCommit(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Issue issue = this.getActiveIssue(request);
        if (issue == null) {
            response.sendRedirect("projects");
            return;
        }
        if ("POST".equals(request.getMethod())) {
            String sha = request.getParameter("commit");
            RepositoryCommit commit = commitDAO.getCommit(sha);
            
            if (issue.getBitbucketIssue() != null) {
//                commit.getBitbucketIssue().put(issue.getBitbucketIssue().getLocalId(), issue.getBitbucketIssue());
                issue.getBitbucketIssue().getCommits().add(commit);
                bitbucketDAO.saveIssue(issue.getBitbucketIssue());
            } else if (issue.getGithubIssue() != null) {
//                commit.getGithubIssue().put(issue.getGithubIssue().getNumber(), issue.getGithubIssue());
                issue.getGithubIssue().getCommits().add(commit);
                githubDAO.saveIssue(issue.getGithubIssue());
            } else if (issue.getGoogleCodeIssue() != null) {
//                commit.getGoogleCodeIssue().put(issue.getGoogleCodeIssue().getTitle(), issue.getGoogleCodeIssue());
                issue.getGoogleCodeIssue().getCommits().add(commit);
            }
            commitDAO.editCommit(commit);
            issueDAO.editIssue(issue);
            
            response.sendRedirect("issue?id=" + issue.getId() + "&projectId=" + issue.getProject().getId());
        } else {
            List<RepositoryCommit> commits = commitDAO.getCommits(getActiveProject(request).getRepository());
            
            getIssuePageBean(request).setActiveIssue(issue);
            getProjectPageBean(request).setActiveProject(getActiveProject(request));
            request.setAttribute("commits", commits);
            request.getRequestDispatcher("assign-commit.jsp").forward(request, response);
        }
    }

    /**
     * Handles closing active issue
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching active issue/project fails
     */
    protected void doCloseIssue(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, ServletException {
        Issue issue = this.getActiveIssue(request);
        Project project = this.getActiveProject(request);
        if (issue == null || project == null) {
            response.sendRedirect("projects");
        } else {
            if (issue.getRepositoryIssue() == null) {
                issue.setLocalState("closed");
                issueDAO.editIssue(issue);
            } else {
                if (issue.getGithubIssue() != null) {
                    getGithubPageBean(request).closeIssue(issue, project.getRepository().getGithubRepository());
                    issue.getGithubIssue().setState("closed");
                    githubDAO.editIssue(issue.getGithubIssue());
                } else if (issue.getBitbucketIssue() != null) {
                    try {
                        Token accessToken = getBitBucketToken(request);
                        getBitBucketPageBean(request).closeIssue(issue, accessToken, loggedUser.getBitbucketUsername());
                        issue.getBitbucketIssue().setStatus("resolved");
                        bitbucketDAO.editIssue(issue.getBitbucketIssue());
                    } catch (NotAuthorizedException ex) {
                        response.sendRedirect("issue?id=" + issue.getId() + "&projectId" + issue.getProject().getId() + "&error=" + ERR_BITBUCKET_NOT_AUTHORIZED);
                        return;
                    }
                }
            }

            response.sendRedirect("issue?id=" + issue.getId() + "&projectId" + issue.getProject().getId() + "&info=" + INF_CLOSED);
        }
    }
    
}
