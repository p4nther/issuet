package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.BitBucketDAOBeanLocal;
import cz.vse.javaee.issuet.BitbucketAPI;
import cz.vse.javaee.issuet.model.BitBucketIssue;
import cz.vse.javaee.issuet.model.BitBucketRepository;
import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.RepositoryCommit;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

/**
 * Bean which provides access to BitBucket entities in the JSP's and servlets
 * @author ondrejsatera
 */
public class BitBucketPageBean {
    
    private final OAuthService service;
    private final BitBucketDAOBeanLocal dao;
    
    private List<BitBucketRepository> userRepositories;

    public BitBucketPageBean(OAuthService service, BitBucketDAOBeanLocal dao) {
        this.service = service;
        this.dao = dao;
    }

    /**
     * Fetches repositories from the BitBucket API's response
     * @param oauthResponse
     * @return list of BitBucket repositories
     * @throws ServletException if parsing fails
     */
    public List<BitBucketRepository> parseRepositoriesFromResponse(Response oauthResponse) 
            throws ServletException {
        List<BitBucketRepository> result = new ArrayList<>();
        try {
            JSONParser parser = new JSONParser();
            JSONObject parsedObject = (JSONObject) parser.parse(oauthResponse.getBody());
            
            JSONArray values = (JSONArray) parsedObject.get("values");
            for (Object obj : values) {
                JSONObject repo = (JSONObject) obj;
                BitBucketRepository newRepo = new BitBucketRepository();
                newRepo.setName((String) repo.get("name"));
                newRepo.setHasIssues((Boolean) repo.get("has_issues"));
                
                result.add(newRepo);
            }
        } catch (ParseException ex) {
            throw new ServletException(ex);
        }
        
        return result;
    }

    /**
     * Returns user's BitBucket repositories. If empty fetches them from DAO
     * @return list of repositories
     */
    public List<BitBucketRepository> getUserRepositories() {
        if (userRepositories.isEmpty())
            userRepositories = dao.getAll();
        return userRepositories;
    }

    public void setUserRepositories(List<BitBucketRepository> userRepositories) {
        this.userRepositories = userRepositories;
    }
    
    /**
     * Returns issues from BitBucket repository endpoint
     * @param repository
     * @param accessToken token which will be used for signing the OAuth request
     * @param username
     * @return list of issues
     * @throws ServletException if parsing fails
     */
    public List<BitBucketIssue> getRepositoryIssues(BitBucketRepository repository, Token accessToken, String username) 
            throws ServletException {
        String queryUrl = BitbucketAPI.formatUrl(new String[]{
            "1.0", 
            "repositories", 
            username, 
            repository.getSlug(), 
            "issues"
        });
        OAuthRequest oauthRequest = new OAuthRequest(Verb.GET, queryUrl);
        service.signRequest(accessToken, oauthRequest);

        Response oauthResponse = oauthRequest.send();
        JSONParser parser = new JSONParser();
        List<BitBucketIssue> issues = new ArrayList<>();
        try {
            JSONObject parsedObject = (JSONObject) parser.parse(oauthResponse.getBody());
            JSONArray issuesArr = (JSONArray) parsedObject.get("issues");
            
            if (issuesArr != null) {
                for (Object object : issuesArr) {
                    JSONObject issue = (JSONObject) object;
                    BitBucketIssue bitbucketIssue = new BitBucketIssue();

                    bitbucketIssue.setContent((String) issue.get("title"));
                    bitbucketIssue.setLocalId((Long) issue.get("local_id"));
                    bitbucketIssue.setPriority((String) issue.get("priority"));
                    bitbucketIssue.setStatus((String) issue.get("status"));
                    bitbucketIssue.setTitle((String) issue.get("title"));
                    bitbucketIssue.setRepository(repository);

                    issues.add(bitbucketIssue);
                }
            }
        } catch (ParseException ex) {
            throw new ServletException(ex);
        }
        return issues;
    }

    /**
     * Returns commits from BitBucket repository endpoint
     * @param bitbucketRepository
     * @param accessToken token which will be used for signing the OAuth request
     * @param username
     * @return list of commits
     * @throws ServletException if parsing fails
     */
    public List<RepositoryCommit> getRepositoryCommits(BitBucketRepository bitbucketRepository, Token accessToken, String username) 
            throws ServletException {
        String queryUrl = BitbucketAPI.formatUrl(new String[]{
            "2.0", 
            "repositories", 
            username,
            bitbucketRepository.getSlug(), 
            "commits"
        });
        OAuthRequest oauthRequest = new OAuthRequest(Verb.GET, queryUrl);
        service.signRequest(accessToken, oauthRequest);

        Response oauthResponse = oauthRequest.send();
        JSONParser parser = new JSONParser();
        List<RepositoryCommit> commits = new ArrayList<>();
        try {
            JSONObject parsedObject = (JSONObject) parser.parse(oauthResponse.getBody());
            JSONArray commitsArr = (JSONArray) parsedObject.get("values");
            
            if (commitsArr != null) {
                for (Object object : commitsArr) {
                    JSONObject commit = (JSONObject) object;

                    RepositoryCommit repCommit = new RepositoryCommit();
                    repCommit.setSha((String) commit.get("hash"));
                    repCommit.setTitle((String) commit.get("message"));

                    JSONObject author = (JSONObject) commit.get("author");
                    repCommit.setCommiter((String) author.get("raw"));

                    commits.add(repCommit);
                }
            }
        } catch (ParseException ex) {
            throw new ServletException(ex);
        }
        
        return commits;
    }

    /**
     * Closes issue in the BitBucket issues endpoint
     * @param issue
     * @param accessToken token which will be used for signing the OAuth request
     * @param username
     */
    public void closeIssue(Issue issue, Token accessToken, String username) {
        BitBucketRepository repository = issue.getRepository().getBitBucketRepository();
        String queryUrl = BitbucketAPI.formatUrl(new String[]{
            "1.0", 
            "repositories", 
            username,
            repository.getSlug(), 
            "issues", 
            issue.getBitbucketIssue().getLocalId().toString()
        });
        
        System.out.println("BitBucket request: " + queryUrl);
        
        OAuthRequest oauthRequest = new OAuthRequest(Verb.PUT, queryUrl);
        oauthRequest.addBodyParameter("status", "resolved");
        service.signRequest(accessToken, oauthRequest);
        
        oauthRequest.send();
    }
    
    /**
     * Creates issue in the BitBucket issues endpoint
     * @param newIssue internal issue
     * @param accessToken token which will be used for signing the OAuth request
     * @param bitbucketRepository
     * @param username
     * @return newly created BitBucketIssue with contents from the API
     * @throws ServletException if parsing fails
     */
    public BitBucketIssue createIssue(Issue newIssue, Token accessToken, BitBucketRepository bitbucketRepository, String username) 
            throws ServletException {
        String queryUrl = BitbucketAPI.formatUrl(new String[]{
            "1.0", 
            "repositories", 
            username,
            bitbucketRepository.getSlug(), 
            "issues"
        });
        
        System.out.println("BitBucket request: " + queryUrl);
        
        OAuthRequest oauthRequest = new OAuthRequest(Verb.POST, queryUrl);
        oauthRequest.addBodyParameter("title", newIssue.getName());
        oauthRequest.addBodyParameter("content", newIssue.getLocalBody());
        
        service.signRequest(accessToken, oauthRequest);
        
        Response oauthResponse = oauthRequest.send();
        JSONParser parser = new JSONParser();
        try {
            JSONObject parsedObject = (JSONObject) parser.parse(oauthResponse.getBody());
            
            BitBucketIssue createdIssue = new BitBucketIssue();
            
            createdIssue.setContent((String) parsedObject.get("content"));
            createdIssue.setLocalId((Long) parsedObject.get("local_id"));
            createdIssue.setStatus((String) parsedObject.get("status"));
            createdIssue.setPriority((String) parsedObject.get("priority"));
            createdIssue.setTitle((String) parsedObject.get("title"));
            
            return createdIssue;
        } catch (ParseException ex) {
            System.out.println("Selhalo parsovani odpovedi z Bitbucketu #" + oauthResponse.getCode());
            System.out.println(oauthResponse.getBody());
            throw new ServletException(ex);
        }
    }
    
}
