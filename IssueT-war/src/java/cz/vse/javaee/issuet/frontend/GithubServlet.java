package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.FrontendServlet;
import cz.vse.javaee.issuet.GitHubClient;
import cz.vse.javaee.issuet.GithubAPI;
import cz.vse.javaee.issuet.exceptions.NotAuthorizedException;
import cz.vse.javaee.issuet.model.GithubRepository;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.eclipse.egit.github.core.client.RequestException;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

/**
 * Servlet used for authorizing user in the GitHub API and 
 * fetching OAuth access token used for signing request.
 * @author ondrejsatera
 */
public class GithubServlet extends FrontendServlet {
    
    private OAuthService service;
    private GitHubClient client;
    
    public static final String GITHUB_TOKEN = "githubToken";

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        
        String CLIENT_ID = servletConfig.getInitParameter("CLIENT_ID");
        String CLIENT_SECRET = servletConfig.getInitParameter("CLIENT_SECRET");
        String REDIRECT_URI = servletConfig.getInitParameter("REDIRECT_URI");
        String SCOPE = servletConfig.getInitParameter("SCOPE");
        
        this.client = new GitHubClient();
        
        this.service = new ServiceBuilder()
                .provider(GithubAPI.class)
                .apiKey(CLIENT_ID)
                .apiSecret(CLIENT_SECRET)
                .callback(REDIRECT_URI)
                .scope(SCOPE)
                .build();
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String action)
            throws ServletException, IOException {
        if ("auth".equals(action)) {
            doAuthorizeUser(request, response);
        } else if ("callback".equals(action)) {
            doProcessCallback(request, response);
        } else if ("refresh".equals(action)) {
            doRefreshRepositories(request, response);
        } else {
            doShowDashboard(request, response);
        }
    }

    /**
     * Redirects user to GitHub website to grant permissions to this application
     * @param request
     * @param response
     * @throws IOException if redirecting fails
     */
    protected void doAuthorizeUser(HttpServletRequest request, HttpServletResponse response) 
            throws IOException {
        String authUrl = service.getAuthorizationUrl(null);
        
        response.sendRedirect(authUrl);
    }

    /**
     * Displays page with information about authorized user. If no one is authorized, 
     * redirects user to authorization handler
     * @param request
     * @param response
     * @throws ServletException if fetching Github page bean fails
     * @throws IOException if fetching page template fails
     */
    protected void doShowDashboard(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        if (initializeClient(request.getSession())) {
            getGithubPageBean(request).setClient(client);
            request.getRequestDispatcher("github.jsp").forward(request, response);
        } else {
            doAuthorizeUser(request, response);
        }
    }

    /**
     * Handles Github API authorization response and save access token to logged user
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if authorization fails
     */
    protected void doProcessCallback(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, ServletException {
        String code = request.getParameter("code");
        
        Verifier v = new Verifier(code);
        
        Token accessToken = service.getAccessToken(null, v);
        
        String token = accessToken.getToken();
        
        if (token.isEmpty()) {
            throw new ServletException("Github - authorization failed");
        } else {
            request.getSession().setAttribute(GITHUB_TOKEN, token);
            userDAO.saveGithubToken(loggedUser, token);
            System.out.println("Github - logged user with token: " + token);
        }
        
        response.sendRedirect("github");
    }
    
    /**
     * Initializes GithubClient used for accessing Github API
     * @param session
     * @return true if initialization is succesfull, false otherwise
     * @throws IOException if fetching page template fails
     */
    private boolean initializeClient(HttpSession session) throws IOException {
        String token = (String) session.getAttribute(GITHUB_TOKEN);
        
        if (token == null || token.isEmpty()) {
            token = loggedUser.getGithubToken();
            if (token == null || token.isEmpty()) {
                return false;
            } else {
                session.setAttribute(GITHUB_TOKEN, token);
            }
        } else {
            client = client.setOAuth2Token(token);
        }
        return true;
    }

    /**
     * Fetches new Github repositories from the API
     * @param request
     * @param response
     * @throws IOException if fetching page template fails
     * @throws ServletException if fetching Github page bean fails
     */
    protected void doRefreshRepositories(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, ServletException {
        try {
            List<GithubRepository> repos = getGithubPageBean(request).refreshUserRepositories();
            
            if (repos == null)
                throw new ServletException("Failed to refresh user repositories");

            for (GithubRepository githubRepository : repos) {
                githubRepository.setOwner(loggedUser);
                githubRepository = githubDAO.save(githubRepository);
                loggedUser.getGithubRepositories().add(githubRepository);
            }

            response.sendRedirect("github");
        }
        catch (RequestException | NotAuthorizedException ex) {
            System.out.println("Github - user not authorized");
            System.out.println(ex.getMessage());
            request.setAttribute("action", ""); // jinak se to porad presmerovava
            response.sendRedirect("github");
        }
    }
}
