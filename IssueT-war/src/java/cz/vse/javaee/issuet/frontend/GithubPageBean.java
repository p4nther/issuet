package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.GitHubClient;
import cz.vse.javaee.issuet.GithubDAOBeanLocal;
import cz.vse.javaee.issuet.exceptions.NotAuthorizedException;
import cz.vse.javaee.issuet.model.GithubIssue;
import cz.vse.javaee.issuet.model.GithubRepository;
import cz.vse.javaee.issuet.model.Issue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.ServletException;
import org.eclipse.egit.github.core.IRepositoryIdProvider;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.RepositoryId;
import org.eclipse.egit.github.core.User;
import org.eclipse.egit.github.core.client.RequestException;
import org.eclipse.egit.github.core.service.IssueService;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.eclipse.egit.github.core.service.UserService;

/**
 * Bean which provides access to Github entities in the JSP's and servlets
 * @author ondrejsatera
 */
public class GithubPageBean {
    private GitHubClient client;
    private UserService userService;
    private RepositoryService repositoryService;
    private final GithubDAOBeanLocal dao;
    private List<GithubRepository> userRepositories;
    
    public final static String GITHUB_PAGE_BEAN = "githubPageBean";
    
    public GithubPageBean(GitHubClient client, GithubDAOBeanLocal dao) {
        this.client = client;
        userService = new UserService(client);
        userService.getClient().setOAuth2Token(client.getOAuthToken());
        repositoryService = new RepositoryService(client);
        repositoryService.getClient().setOAuth2Token(client.getOAuthToken());
        this.dao = dao;
        userRepositories = new ArrayList<>();
    }
    
    public void setClient(GitHubClient client) {
        this.client = client;
        userService = new UserService(client);
        userService.getClient().setOAuth2Token(client.getOAuthToken());
        repositoryService = new RepositoryService(client);
        repositoryService.getClient().setOAuth2Token(client.getOAuthToken());
    }
    
    /**
     * Returns authorized user entity
     * @return GithubUser
     * @throws ServletException if fetching fails
     */
    public User getUser() throws ServletException {
        try {
            return userService.getUser();
        }
        catch (IOException ex) {
            throw new ServletException(ex);
        }
    }
    
    /**
     * Returns user's GitHub repositories. If empty fetches them from DAO
     * @return list of repositories
     */
    public List<GithubRepository> getUserRepositories() {
        if (userRepositories.isEmpty())
            userRepositories = dao.getAll();
        return userRepositories;
    }
    
    /**
     * Fetches new repositories from GitHub API
     * @return list of repositories
     * @throws IOException if fetching fails
     * @throws ServletException if fetching fails
     * @throws NotAuthorizedException if user not authorized
     */
    public List<GithubRepository> refreshUserRepositories()
            throws IOException, ServletException, NotAuthorizedException {
        List<Repository> userRepos;
        try {
            userRepos = repositoryService.getRepositories();
        }
        catch(RequestException ex) {
            throw new ServletException(ex);
        }
        if (userRepos == null)
            throw new ServletException("GitHub API responded with malformed data");
        
        List<GithubRepository> list = new ArrayList<>();
        for (Repository repository : userRepos) {
            GithubRepository rep = new GithubRepository();
            rep.setName(repository.getName());
            rep.setDescription(repository.getDescription());
            rep.setFullName(repository.getGitUrl());
            rep.setGithubRepo(repository);
            
            list.add(rep);
        }
        return list;
    }

    /**
     * Closes issue in the GitHub API
     * @param issue
     * @param githubRepository
     * @throws IOException if fetching fails
     * @throws ServletException if fetching fails
     */
    public void closeIssue(Issue issue, GithubRepository githubRepository) 
            throws IOException, ServletException {
        IssueService issueService = new IssueService(client);
        issueService.getClient().setOAuth2Token(client.getOAuthToken());
        
        org.eclipse.egit.github.core.Issue repoIssue = new org.eclipse.egit.github.core.Issue();
        repoIssue.setNumber(issue.getGithubIssue().getNumber());
        repoIssue.setState("closed");
        
        IRepositoryIdProvider repositoryID = new RepositoryId(getUser().getLogin(), githubRepository.getName());
        issueService.editIssue(repositoryID, repoIssue);
    }

    /**
     * Returns issues in all states from the given repository in the GitHub API
     * @param githubRepository
     * @return list of GitHub internal issues
     * @throws IOException if fetching fails
     * @throws ServletException if fetching fails
     */
    public List<org.eclipse.egit.github.core.Issue> getIssues(GithubRepository githubRepository) 
            throws IOException, ServletException {
        IssueService issueService = new IssueService(client);
        issueService.getClient().setOAuth2Token(client.getOAuthToken());
        
        ConcurrentHashMap<String, String> filter = new ConcurrentHashMap<>();
        filter.put("state", "all");
        filter.put("sort", "updated");
        
        IRepositoryIdProvider repositoryID = new RepositoryId(getUser().getLogin(), githubRepository.getName());
        return issueService.getIssues(repositoryID, filter);
    }

    /**
     * Creates issue in the GitHub API
     * @param newIssue internal issue
     * @param githubRepository
     * @return newly created GithubIssue with contents from the API
     * @throws IOException if fetching fails
     * @throws ServletException if fetching fails
     */
    public GithubIssue createIssue(Issue newIssue, GithubRepository githubRepository) throws IOException, ServletException {
        if (null == githubRepository) {
            throw new ServletException("GithubRepository cannot be null");
        }
        
        IssueService issueService = new IssueService(client);
        issueService.getClient().setOAuth2Token(client.getOAuthToken());
        
        org.eclipse.egit.github.core.Issue repoIssue = new org.eclipse.egit.github.core.Issue();
        repoIssue.setTitle(newIssue.getName());
        repoIssue.setBody(newIssue.getLocalBody());
        repoIssue.setUser(getUser());
        
        IRepositoryIdProvider repositoryID = new RepositoryId(getUser().getLogin(), githubRepository.getName());
        org.eclipse.egit.github.core.Issue createdIssue = issueService.createIssue(repositoryID, repoIssue);
        
        GithubIssue result = new GithubIssue();
        
        result.setBody(createdIssue.getBody());
        result.setNumber(createdIssue.getNumber());
        result.setState(createdIssue.getState());
        result.setTitle(createdIssue.getTitle());
        
        return result;
    }
}
