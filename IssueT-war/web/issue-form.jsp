<%-- 
    Document   : project-form
    Created on : 18.5.2014, 11:30:08
    Author     : ondrejsatera
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="issueBean" scope="session" type="cz.vse.javaee.issuet.frontend.IssuePageBean"/>
<jsp:useBean id="projectBean" scope="session" type="cz.vse.javaee.issuet.frontend.ProjectPageBean"/>
<c:set var="issue" value="${issueBean.activeIssue}" />
<c:set var="project" value="${projectBean.activeProject}" />

<c:choose>
    <c:when test="${issue != null}">
        <c:set var="createMode" value="false" />
    </c:when>
    <c:otherwise>
        <c:set var="createMode" value="true" />
    </c:otherwise>
</c:choose>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>
            <c:choose>
                <c:when test="${createMode}">Create new issue</c:when>
                <c:otherwise>Edit issue</c:otherwise>
            </c:choose> - IssueT
        </title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li><a href="projects">My Projects</a></li>
                <li><a href="project?id=${project.id}">${project.name}</a></li>
                <li><a href="issues?projectId=${project.id}">Issues</a></li>
                <c:choose>
                    <c:when test="${createMode}">
                        <li class="active">Create new</li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="issue?id=${issue.id}&projectId=${project.id}">${issue.name}</a></li>
                        <li class="active">Edit</li>
                    </c:otherwise>
                </c:choose>
            </ol>
            
            <div class="jumbotron">
                <c:choose>
                    <c:when test="${createMode}">
                        <h1>Create new issue</h1>
                    </c:when>
                    <c:otherwise>
                        <h1>Edit issue ${issue.name}</h1>
                    </c:otherwise>
                </c:choose>
            </div>
            
            <c:choose>
                <c:when test="${createMode}">
                    <form method="post" action="issues">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input id="name" name="name" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="body">Body:</label>
                            <textarea id="body" name="body" class="form-control"></textarea>
                        </div>
                        
                        <input type="hidden" name="action" value="create">
                        <input type="hidden" name="projectId" value="${project.id}">
                        <input type="submit" name="submit" value="Create" class="btn btn-success">
                    </form>
                </c:when>
                <c:otherwise>
                    <form method="post" action="issue">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input id="name" name="name" type="text" class="form-control" value="${issue.name}">
                        </div>
                        <div class="form-group">
                            <label for="body">Body:</label>
                            <textarea id="body" name="body" class="form-control">${issue.body}</textarea>
                        </div>
                        
                        <input type="hidden" name="action" value="edit">
                        <input type="hidden" name="id" value="${issue.id}">
                        <input type="hidden" name="projectId" value="${project.id}">
                        <input type="submit" name="submit" value="Save changes" class="btn btn-success">
                    </form>
                </c:otherwise>
        </c:choose>
        </div>
    </body>
</html>
