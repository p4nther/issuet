<%-- 
    Document   : issues
    Created on : 18.5.2014, 14:32:17
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="projectBean" scope="session" type="cz.vse.javaee.issuet.frontend.ProjectPageBean"/>
<c:set var="project" value="${projectBean.activeProject}" />

<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>Project repository commits - IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li><a href="projects">My Projects</a></li>
                <li><a href="project?id=${project.id}">${project.name}</a></li>
                <li class="active">Commits</li>
            </ol>
            
            <a href="commits?action=import&projectId=${project.id}" class="btn btn-primary pull-right">Import commits from repository</a>
            <h1>${project.name} - commits</h1>
            <span class="clearfix"></span>
            
            <c:if test="${requestScope.message != null}">
                <p class="alert alert-success"><b>Info:</b> ${requestScope.message}</p>
            </c:if>
            <c:if test="${requestScope.errorMessage != null}">
                <p class="alert alert-danger"><b>Error:</b> ${requestScope.errorMessage}</p>
            </c:if>
            
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Hash</th>
                        <th>Message</th>
                        <th>Author</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${project.repository.commits}" var="commit">
                        <tr>
                            <td>${commit.sha}</td>
                            <td>${commit.title}</td>
                            <td>${commit.commiter}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <c:if test="${project.repository.commits.isEmpty()}">
                <p>There are no commits in this project's repository</p>
            </c:if>
        </div>
    </body>
</html>
