<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>IssueT - your integrated issue tracker</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <div class="jumbotron">
                <h1>Authentication failed</h1>
                <h2>Server responded with code 401 - Unauthorized</h2>
            </div>
        </div>
    </body>
</html>
