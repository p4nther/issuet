<%-- 
    Document   : project
    Created on : 18.5.2014, 13:48:52
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="projectBean" scope="session" type="cz.vse.javaee.issuet.frontend.ProjectPageBean"/>
<c:set var="project" value="${projectBean.activeProject}" />
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>Project Detail - IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li><a href="projects">My Projects</a></li>
                <li><a href="project?id=${project.id}">${project.name}</a></li>
                <li class="active">Members</li>
            </ol>
            
            <c:if test="${project.owner.email.equals(loggedUser.email)}">
                <a href="project?id=${project.id}&action=new-member" class="btn btn-primary pull-right">Add new member</a>
            </c:if>
            <h1>${project.name} - members</h1>
            <span class="clearfix"></span>
            
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>E-mail</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${project.owner.email}</td>
                        <td>owner</td>
                        <td></td>
                    </tr>
                    <c:forEach items="${project.members}" var="person">
                        <tr>
                            <td>${person.email}</td>
                            <td>member</td>
                            <td>
                                <c:if test="${project.owner.email.equals(loggedUser.email)}">
                                    <form action="project" method="POST">
                                        <button type="submit" class="btn btn-danger">Remove from project</button>
                                        <input type="hidden" name="id" value="${project.id}">
                                        <input type="hidden" name="person" value="${person.email}">
                                        <input type="hidden" name="action" value="remove-member">
                                    </form>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
