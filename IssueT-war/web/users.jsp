<%-- 
    Document   : backend-user
    Created on : 14.5.2014, 22:02:10
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>User management | IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li class="active">User management</li>
            </ol>
            
            <div class="jumbotron">
                <h1>User management</h1>
                <p>&nbsp;</p>
                <a href="user?action=create" class="btn btn-primary">Create new</a>
            </div>
            
            <c:if test="${requestScope.message != null}">
                <div class="alert alert-success">${requestScope.message}</p>
            </c:if>
            
            <table class="table">
                <thead>
                    <tr>
                        <th>E-mail</th>
                        <th># of projects</th>
                        <th># of team projects</th>
                        <th>Github active</th>
                        <th>BitBucket active</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${requestScope.users}" var="user">
                        <tr>
                            <td>
                                <c:if test="${user.email.equals(loggedUser.email)}">
                                    <span class="glyphicon glyphicon-star" title="Currently logged user"></span>
                                </c:if>
                                ${user.email}
                            </td>
                            <td>${user.projects.size()}</td>
                            <td>${user.teamProjects.size()}</td>
                            <td><c:if test="${user.githubToken != null}">active</c:if></td>
                            <td><c:if test="${user.bitbucketToken != null}">active</c:if></td>
                            <td>
                                
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
