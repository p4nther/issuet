<%-- 
    Document   : issues
    Created on : 18.5.2014, 14:32:17
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="projectBean" scope="session" type="cz.vse.javaee.issuet.frontend.ProjectPageBean"/>
<c:set var="project" value="${projectBean.activeProject}" />

<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>Project issues - IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li><a href="projects">My Projects</a></li>
                <li><a href="project?id=${project.id}">${project.name}</a></li>
                <li class="active">Issues</li>
            </ol>
            
            <c:if test="${requestScope.message != null}">
                <p class="alert alert-success"><b>Info:</b> ${requestScope.message}</p>
            </c:if>
            <c:if test="${requestScope.errorMessage != null}">
                <p class="alert alert-danger"><b>Error:</b> ${requestScope.errorMessage}</p>
            </c:if>
                
            <div class="pull-right">
                <a href="issues?action=create&projectId=${project.id}" class="btn btn-success">Create new issue</a>
                <a href="issues?action=import&projectId=${project.id}" class="btn btn-primary">Import issues from repository</a>
            </div>
            <h1>${project.name} - issues</h1>
            <span class="clearfix"></span>
            
            <c:set var="buttonColor" value="default"/>
            <c:set var="filter" value="all"/>
            <c:set var="entry" value=""/>
            <c:choose>
                <c:when test="${requestScope.filter == 'all'}">
                    <c:set var="buttonColor" value="default"/>
                    <c:set var="filter" value="${requestScope.filter}"/>
                </c:when>
                <c:when test="${requestScope.filter == 'closed'}">
                    <c:set var="buttonColor" value="danger"/>
                    <c:set var="filter" value="${requestScope.filter}"/>
                </c:when>
                <c:when test="${requestScope.filter == 'open'}">
                    <c:set var="buttonColor" value="success"/>
                    <c:set var="filter" value="${requestScope.filter}"/>
                </c:when>
                <c:otherwise>
                    <c:set var="entry" value="${requestScope.filter}"/>
                </c:otherwise>
            </c:choose>
            
            <div class="row">
                <div class="col-md-2">
                    <div class="btn-group">
                        <button type="button" class="btn btn-${buttonColor}">State: ${filter}</button>
                        <button type="button" class="btn btn-${buttonColor} dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="issues?projectId=${project.id}&action=all">All</a></li>
                            <li><a href="issues?projectId=${project.id}&action=only-opened">Opened</a></li>
                            <li><a href="issues?projectId=${project.id}&action=only-closed">Closed</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <form method="GET" action="issues" role="form">
                        <div class="form-group">
                            <div>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="entry" value="${entry}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Apply filter</button>
                                    </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->
                        </div>
                        <input type="hidden" name="action" value="search">
                        <input type="hidden" name="projectId" value="${project.id}">
                    </form>
                </div>
            </div>
                
            <c:forEach items="${requestScope.issues}" var="issue">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            ${issue.name}
                            <span class="pull-right">
                                #${issue.specificIdentifier}
                            </span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        ${issue.body}
                    </div>
                    <div class="panel-footer clearfix">
                        <c:if test="${issue.isOpen()}">
                            <a href="issue?id=${issue.id}&projectId=${project.id}&action=close" class="btn btn-success pull-left" style="margin-right: 5px;">Mark as resolved</a>
                        </c:if>
                        <a href="issue?action=preview&id=${issue.id}&projectId=${project.id}" class="btn btn-primary pull-left" style="margin-right: 5px;">Preview</a>
                        <a href="issue?action=edit&id=${issue.id}&projectId=${project.id}" class="btn btn-primary pull-left" style="margin-right: 5px;">Edit</a>
                        <form method="post" action="issue">
                            <input type="hidden" name="id" value="${issue.id}">
                            <input type="hidden" name="projectId" value="${project.id}">
                            <input type="hidden" name="action" value="delete">
                            <input type="submit" name="submit" value="Delete issue" class="btn btn-danger pull-left">
                        </form>
                        
                        <span class="pull-right" style="line-height:34px">State: ${issue.specificState}</span>
                    </div>
                </div>
            </c:forEach>
            <c:if test="${issues.isEmpty()}">
                <p>There are no ${requestScope.filter} issues in this project</p>
            </c:if>
        </div>
    </body>
</html>
