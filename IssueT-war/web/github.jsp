<%-- 
    Document   : github
    Created on : 30.5.2014, 10:52:48
    Author     : ondrejsatera
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="githubPageBean" scope="session" type="cz.vse.javaee.issuet.frontend.GithubPageBean"/>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>Github dashboard - IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li>Github</li>
            </ol>
                
            <div class="jumbotron">
                <h1>GitHub - dashboard</h1>
                <p>&nbsp;</p>
                <a href="github?action=auth" class="btn btn-primary">Authorize</a>
                <a href="github?action=refresh" class="btn btn-success">Refresh repos</a>
            </div>
            
            <c:if test="${requestScope.message != null}">
                <p>${requestScope.message}</p>
            </c:if>
            
            <p>Logged User: ${githubPageBean.user.getLogin()}</p>
            
            <c:forEach items="${githubPageBean.userRepositories}" var="repository">
                <p>${repository.name}</p>
            </c:forEach>
        </div>
    </body>
</html>
