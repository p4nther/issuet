<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="bitbucketPageBean" scope="session" type="cz.vse.javaee.issuet.frontend.BitBucketPageBean"/>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>BitBucket dashboard - IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li>BitBucket</li>
            </ol>
                
            <div class="jumbotron">
                <h1>BitBucket - dashboard</h1>
                <p>&nbsp;</p>
                <a href="bitbucket?action=auth" class="btn btn-primary">Authorize</a>
                <a href="bitbucket?action=refresh" class="btn btn-success">Refresh repos</a>
            </div>
            
            <c:forEach items="${bitbucketPageBean.userRepositories}" var="repository">
                <p>${repository.name}</p>
            </c:forEach>
        </div>
    </body>
</html>
