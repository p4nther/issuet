<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="issueBean" scope="session" type="cz.vse.javaee.issuet.frontend.IssuePageBean"/>
<jsp:useBean id="projectBean" scope="session" type="cz.vse.javaee.issuet.frontend.ProjectPageBean"/>
<c:set var="issue" value="${issueBean.activeIssue}" />
<c:set var="project" value="${projectBean.activeProject}" />

<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>Assign a commit to the issue - IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li><a href="projects">My Projects</a></li>
                <li><a href="project?id=${project.id}">${project.name}</a></li>
                <li><a href="issues?projectId=${project.id}">Issues</a></li>
                <li><a href="issue?id=${issue.id}&projectId=${project.id}">${issue.name}</a></li>
                <li class="active">Assign a commit</li>
            </ol>
            
            <h1>Assign a commit to the issue</h1>
            
            <div class="row">
                <div class="col-md-6">
                    <form method="post" action="issue">
                        <div class="form-group">
                            <label for="issue">Issue:</label>
                            <input id="issue" name="issue" type="text" class="form-control" value="${issue.name}" disabled="disabled">
                        </div>
                        <div class="form-group">
                            <label for="commit">Commit:</label>
                            <select id="commit" name="commit" class="form-control">
                                <c:forEach items="${requestScope.commits}" var="commit">
                                    <option value="${commit.sha}">${commit.title}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <input type="hidden" name="action" value="assign-commit">
                        <input type="hidden" name="id" value="${issue.id}">
                        <input type="hidden" name="projectId" value="${project.id}">
                        <input type="submit" name="submit" value="Assign commit" class="btn btn-success">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
