<%-- 
    Document   : project-form
    Created on : 18.5.2014, 11:30:08
    Author     : ondrejsatera
--%>
<%@page import="cz.vse.javaee.issuet.model.Project"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="projectBean" scope="session" type="cz.vse.javaee.issuet.frontend.ProjectPageBean"/>
<c:if test="${sessionScope.githubPageBean != null}">
    <jsp:useBean id="githubPageBean" scope="session" type="cz.vse.javaee.issuet.frontend.GithubPageBean"/>
</c:if>
<c:if test="${sessionScope.bitbucketPageBean != null}">
    <jsp:useBean id="bitbucketPageBean" scope="session" type="cz.vse.javaee.issuet.frontend.BitBucketPageBean"/>
</c:if>
<c:set var="project" value="${projectBean.activeProject}" />

<c:set var="createMode" value="true" />
<c:if test="${project != null}">
    <c:set var="createMode" value="false" />
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>
            <c:choose>
                <c:when test="${createMode}">Create new project</c:when>
                <c:otherwise>Edit project</c:otherwise>
            </c:choose> - IssueT
        </title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li><a href="projects">My Projects</a></li>
                <c:choose>
                    <c:when test="${createMode}">
                        <li class="active">Create new</li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="project?id=${project.id}">${project.name}</a></li>
                        <li class="active">Edit</li>
                    </c:otherwise>
                </c:choose>
            </ol>

            <c:choose>
                <c:when test="${createMode}">
                    <h1>Create new project</h1>
                </c:when>
                <c:otherwise>
                    <h1>Edit project: ${project.name}</h1>
                </c:otherwise>
            </c:choose>
        
            <c:choose>
                <c:when test="${createMode}">
                    <form method="post" action="projects" role="form">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input class="form-control" id="name" name="name" type="text">
                        </div>
                        <div class="form-group">
                            <label for="github">GitHub:</label>
                            <select name="github" id="github" class="form-control">
                                <option value="">-- select --</option>
                                <c:forEach items="${githubPageBean.userRepositories}" var="repo">
                                    <option value="${repo.name}">${repo.fullName}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="bitbucket">BitBucket:</label>
                            <select name="bitbucket" id="bitbucket" class="form-control">
                                <option value="">-- select --</option>
                                <c:forEach items="${bitbucketPageBean.userRepositories}" var="repo">
                                    <option value="${repo.name}">${repo.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        
                        <button type="submit" name="submit" class="btn btn-success">Create project</button>
                        
                        <input type="hidden" name="action" value="create">
                    </form>
                </c:when>
                <c:otherwise>
                    <form method="post" action="project" role="form">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input class="form-control" id="name" name="name" type="text" value="${project.name}">
                        </div>
                        <div class="form-group">
                            <label for="github">GitHub:</label>
                            <select name="github" id="github" class="form-control">
                                <option value="">-- select --</option>
                                <c:forEach items="${githubPageBean.userRepositories}" var="repo">
                                    <c:if test="${project.repository.githubRepository != null}">
                                        <c:choose>
                                            <c:when test="${project.repository.githubRepository.name == repo.name}">
                                                <option value="${repo.name}" selected="selected">
                                                    ${repo.fullName}
                                                </option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${repo.name}">${repo.fullName}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                    <c:if test="${project.repository.githubRepository == null}">
                                        <option value="${repo.name}">${repo.fullName}</option>
                                    </c:if>
                                    
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="bitbucket">BitBucket:</label>
                            <select name="bitbucket" id="bitbucket" class="form-control">
                                <option value="">-- select --</option>
                                <c:forEach items="${bitbucketPageBean.userRepositories}" var="repo">
                                    <c:if test="${project.repository.bitBucketRepository != null}">
                                        <c:choose>
                                            <c:when test="${project.repository.bitBucketRepository.name == repo.name}">
                                                <option value="${repo.name}" selected="selected">
                                                    ${repo.name}
                                                </option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${repo.name}">
                                                    ${repo.name}
                                                </option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                    <c:if test="${project.repository.bitBucketRepository == null}">
                                        <option value="${repo.name}">
                                            ${repo.name}
                                        </option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                        
                        <button type="submit" name="submit" class="btn btn-success">Save changes</button>
                        
                        <input type="hidden" name="action" value="edit">
                        <input type="hidden" name="id" value="${project.id}">
                    </form>
                </c:otherwise>
            </c:choose>
        </div>
    </body>
</html>
