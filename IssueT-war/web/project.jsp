<%-- 
    Document   : project
    Created on : 18.5.2014, 13:48:52
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="projectBean" scope="session" type="cz.vse.javaee.issuet.frontend.ProjectPageBean"/>
<c:set var="project" value="${projectBean.activeProject}" />
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>Project Detail - IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li><a href="projects">My Projects</a></li>
                <li class="active">${project.name}</li>
            </ol>
                
            <div class="jumbotron">
                <h1>${project.name}</h1>
                <p>&nbsp;</p>
                <a href="project?id=${project.id}&action=edit" class="btn btn-primary">Edit</a>
                <a href="issues?projectId=${project.id}" class="btn btn-default">Issues</a>
                <a href="commits?projectId=${project.id}" class="btn btn-default">Commits</a>
                <a href="project?id=${project.id}&action=members" class="btn btn-default">Members</a>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <h2>Issues</h2>
                    <c:if test="${project.issues.isEmpty()}">
                        <p>There are no issues yet</p>
                    </c:if>
                    <div class="list-group">
                        <c:forEach items="${project.issues}" end="4" var="issue">
                            <a href="issue?action=previw&id=${issue.id}&projectId=${project.id}" class="list-group-item">
                                <div class="list-group-item-text">
                                    ${issue.name}
                                </div>
                            </a>
                        </c:forEach>
                        <c:if test="${project.issues.size() > 5}">
                            <a href="issues?projectId=${project.id}" class="list-group-item active">
                                <span class="badge">${project.issues.size()}</span>
                                Show more issues &raquo;
                            </a>
                        </c:if>
                    </div>
                </div>
                <div class="col-md-6">
                    <h2>Commits</h2>
                    <c:if test="${project.repository.commits.isEmpty()}">
                        <p>There are no commits yet</p>
                    </c:if>
                    <div class="list-group">
                        <c:forEach items="${project.repository.commits}" end="4" var="commit">
                            <a href="commit?action=previw&id=${commit.sha}&projectId=${project.id}" class="list-group-item">
                                <div class="list-group-item-text">
                                    ${commit.title}
                                </div>
                            </a>
                        </c:forEach>
                        <c:if test="${project.repository.commits.size() > 5}">
                            <a href="commits?projectId=${project.id}" class="list-group-item active">
                                <span class="badge">${project.repository.commits.size()}</span>
                                Show more commits &raquo;
                            </a>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
