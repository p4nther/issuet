<%-- 
    Document   : dashboard
    Created on : 8.5.2014, 14:48:45
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="UserDAOBean" scope="session" type="cz.vse.javaee.issuet.UserDAOBeanLocal"/>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>Dashboard</title>
    </head>
    <body>
        <h1>Vitej, ${UserDAOBean.user.email}</h1>
    </body>
</html>
