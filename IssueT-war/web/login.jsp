<%-- 
    Document   : login
    Created on : 8.5.2014, 14:40:59
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>Login page</title>
    </head>
    <body>
        <h1>Neprihlaseny uzivatel</h1>
        
        <form method="POST">
            <table border="0">
                <tr>
                    <th><label for="email">E-mail</label></th>
                    <td><input type="email" name="email" id="email"></td>
                </tr>
                <tr>
                    <th><label for="password">Heslo</label></th>
                    <td><input type="password" name="password" id="password"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="hidden" name="action" value="login">
                        <input type="submit" name="submit" value="Přihlásit se">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
