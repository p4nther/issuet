<%-- 
    Document   : index
    Created on : 27.4.2014, 17:19:33
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>IssueT - your integrated issue tracker</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li class="active">Dashboard</li>
            </ol>
            <div class="jumbotron">
                <h1>IssueT</h1>
                <h2>your integrated issue tracker</h2>
                <p>Here you can manage issues from 3 different version control system - GitHub, BitBucket and GoogleCode (comming soon).</p>
            </div>
        </div>
    </body>
</html>
