<%-- 
    Document   : backend-user
    Created on : 14.5.2014, 22:02:10
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>User management | IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li><a href="user">User management</a></li>
                <li class="active">Create new user</li>
            </ol>
            
            <div class="jumbotron">
                <h1>User management</h1>
            </div>
            
            <c:if test="${requestScope.errorMessage != null}">
                <div class="alert alert-danger">${requestScope.errorMessage}</div>
            </c:if>

            <h2>Create new user</h2>

            <form method="POST">
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" name="email" id="email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
                <div class="form-group">
                    <label for="bitbucketUsername">BitBucket Username</label>
                    <input type="text" name="bitbucketUsername" id="bitbucketUsername" class="form-control">
                </div>
                    
                <input type="hidden" name="action" value="create">
                <input type="submit" name="submit" value="Save" class="btn btn-primary">
            </form>
        </div>
    </body>
</html>
