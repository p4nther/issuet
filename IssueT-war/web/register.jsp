<%-- 
    Document   : backend-user
    Created on : 14.5.2014, 22:02:10
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>User management | IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li class="active">Registration</li>
            </ol>
            
            <div class="jumbotron">
                <h1>Register new user account</h1>
            </div>
            
            <c:if test="${requestScope.errorMessage != null}">
                <div class="alert alert-danger">${requestScope.errorMessage}</div>
            </c:if>

            <div class="row">
                <div class="col-md-5">
                    <form method="POST">
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" name="email" id="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="bitbucketUsername">BitBucket Username</label>
                            <input type="text" name="bitbucketUsername" id="bitbucketUsername" class="form-control">
                        </div>

                        <input type="hidden" name="action" value="create">
                        <input type="submit" name="submit" value="Register" class="btn btn-primary">
                    </form>
                </div>
                <div class="col-md-5 col-md-offset-2">
                    <div class="list-group">
                        <a href="#" class="list-group-item active">
                            <h3 class="list-group-item-heading">IssueT - Integrated Issue Tracker</h3>
                        </a>
                        <a href="#" class="list-group-item">
                            <h4 class="list-group-item-heading">Three most used VCS</h4>
                            <p class="list-group-item-text">
                                You can manage issues in three most used VCS's - Github, BitBucket and Google Code (comming soon).
                            </p>
                        </a>
                        <a href="#" class="list-group-item">
                            <h4 class="list-group-item-heading">Infinite number of projects</h4>
                            <p class="list-group-item-text">
                                You can create as many projects as you want to.
                            </p>
                        </a>
                        <a href="#" class="list-group-item">
                            <h4 class="list-group-item-heading">Infinite number of project members</h4>
                            <p class="list-group-item-text">
                                You can assign your team members to your projects and they'll receive notifications when something happens in the project.
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
