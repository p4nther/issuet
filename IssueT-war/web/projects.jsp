<%-- 
    Document   : projects
    Created on : 18.5.2014, 11:16:02
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>${requestScope.pageTitle} - IssueT - integrated issue tracker</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li class="active">${requestScope.pageTitle}</li>
            </ol>
            
            <div class="jumbotron">
                <h1>${requestScope.pageTitle}</h1>
                <p>&nbsp;</p>
                <p>
                    <a href="projects?action=create" class="btn btn-success">Create new project</a>
                </p>
            </div>
                
            <c:if test="${requestScope.message != null}">
                <p class="alert alert-success"><b>Info:</b> ${requestScope.message}</p>
            </c:if>
            <c:if test="${requestScope.errorMessage != null}">
                <p class="alert alert-danger"><b>Error:</b> ${requestScope.errorMessage}</p>
            </c:if>
            
            <c:forEach items="${requestScope.projects}" var="project">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">${project.name}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-8">
                            <a href="issues?projectId=${project.id}" class="btn btn-default">Issues</a>
                            <a href="commits?projectId=${project.id}" class="btn btn-default">Commits</a>
                            <a href="project?id=${project.id}&action=members" class="btn btn-default">Members</a>
                        </div>
                        <div class="col-md-4 clearfix">
                            <c:if test="${project.owner.email.equals(loggedUser.email)}">
                                <form method="post" action="project">
                                    <input type="hidden" name="id" value="${project.id}">
                                    <input type="hidden" name="action" value="delete">
                                    <button type="submit" name="submit" class="btn btn-danger pull-right">Delete</button>
                                </form>
                            </c:if>
                            <c:if test="${project.owner.email.equals(loggedUser.email)}">
                                <a href="project?action=edit&id=${project.id}" class="btn btn-default pull-right" style="margin:0 5px">Edit</a>
                            </c:if>
                            <a href="project?id=${project.id}" class="btn btn-primary pull-right">Preview</a>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div style="text-align: center">
                            <c:if test="${project.repository.bitBucketRepository != null || project.repository.githubRepository != null}">
                                Repository placed on
                                <c:choose>
                                    <c:when test="${project.repository.bitBucketRepository != null}">
                                        <strong>BitBucket</strong>
                                    </c:when>
                                    <c:when test="${project.repository.githubRepository != null}">
                                        <strong>GitHub</strong>
                                    </c:when>
                                </c:choose>
                                in the repository <strong>${project.repository.specificRepository.name}</strong>
                            </c:if>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </body>
</html>
