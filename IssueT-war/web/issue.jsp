<%-- 
    Document   : project
    Created on : 18.5.2014, 13:48:52
    Author     : ondrejsatera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="issueBean" scope="session" type="cz.vse.javaee.issuet.frontend.IssuePageBean"/>
<c:set var="project" value="${issueBean.activeProject}" />
<c:set var="issue" value="${issueBean.activeIssue}" />
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>Issue Detail - IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li><a href="projects">My Projects</a></li>
                <li><a href="project?id=${project.id}">${project.name}</a></li>
                <li><a href="issues?projectId=${project.id}">Issues</a></li>
                <li class="active">${issue.name}</li>
            </ol>
            
            <c:if test="${requestScope.message != null}">
                <p class="alert alert-success"><b>Info:</b> ${requestScope.message}</p>
            </c:if>
            <c:if test="${requestScope.errorMessage != null}">
                <p class="alert alert-danger"><b>Error:</b> ${requestScope.errorMessage}</p>
            </c:if>
            
            <h1 class="pull-left">${issue.name}</h1>
            
            <div class="pull-right">
                <a href="issue?id=${issue.id}&projectId=${project.id}&action=close" class="btn btn-success">Mark as resolved</a>
                <a href="issue?id=${issue.id}&projectId=${project.id}&action=edit" class="btn btn-primary">Edit issue</a>
                <a href="project?id=${project.id}&action=edit" class="btn btn-default">Edit project</a>
            </div>
            <span class="clearfix"></span>
            
            <div class="row">
                <div class="col-md-12">
                    <h2>Description / body</h2>
                    <div class="well">
                        ${issue.body}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2>Repository issue params</h2>
                    <c:choose>
                        <c:when test="${issue.bitbucketIssue != null}">
                            <div class="list-group">
                                <a class="list-group-item active">
                                    <h4 class="list-group-item-heading">Repository local ID</h4>
                                    <p class="list-group-item-text">${issue.bitbucketIssue.localId}</p>
                                </a>
                                <span class="list-group-item">
                                    <h4 class="list-group-item-heading">Status</h4>
                                    <p class="list-group-item-text">${issue.bitbucketIssue.status}</p>
                                </span>
                                <span class="list-group-item">
                                    <h4 class="list-group-item-heading">Priority</h4>
                                    <p class="list-group-item-text">${issue.bitbucketIssue.priority}</p>
                                </span>
                            </div>
                        </c:when>
                        <c:when test="${issue.githubIssue != null}">
                            <div class="list-group">
                                <a class="list-group-item active">
                                    <h4 class="list-group-item-heading">Repository local ID</h4>
                                    <p class="list-group-item-text">${issue.githubIssue.number}</p>
                                </a>
                                <span class="list-group-item">
                                    <h4 class="list-group-item-heading">State</h4>
                                    <p class="list-group-item-text">${issue.githubIssue.state}</p>
                                </span>
                            </div>
                        </c:when>
                    </c:choose>
                </div>
                <div class="col-md-6">
                    <h2>
                        Assigned commits
                        <a href="issue?action=assign-commit&id=${issue.id}&projectId=${project.id}" class="btn btn-success pull-right">Assign</a>
                    </h2>
                    <div class="list-group">
                        <c:forEach items="${issue.repositoryIssue.commits}" var="commit">
                            <a class="list-group-item">
                                <div class="list-group-item-text">${commit.title}</div>
                            </a>
                        </c:forEach>
                    </div>
                    <c:if test="${issue.repositoryIssue.commits == null || issue.repositoryIssue.commits.size() == 0}">
                        <p>There are no assigned commits yet</p>
                    </c:if>
                </div>
            </div>
        </div>
    </body>
</html>
