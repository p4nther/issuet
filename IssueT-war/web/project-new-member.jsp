<%@page import="cz.vse.javaee.issuet.model.Project"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="projectBean" scope="session" type="cz.vse.javaee.issuet.frontend.ProjectPageBean"/>
<c:set var="project" value="${projectBean.activeProject}" />

<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/jspf/head.jspf" %>
        <title>Assign new member to project - IssueT</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/menu.jspf" %>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href=".">Dashboard</a></li>
                <li><a href="projects">My Projects</a></li>
                <li><a href="project?id=${project.id}">${project.name}</a></li>
                <li><a href="project?id=${project.id}&action=members">Members</a></li>
                <li class="active">Assign new</li>
            </ol>
                
            <h1>Assign new member to project ${project.name}</h1>
        
            <form method="post" action="project" role="form">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="user">User:</label>
                        <select name="user" id="user" class="form-control">
                            <option value="">-- select new member --</option>
                            <c:forEach items="${requestScope.users}" var="person">
                                <%--<c:if test="${!project.hasMember(person)}">--%>
                                    <option value="${person.email}">${person.email}</option>
                                <%--</c:if>--%>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                
                <button type="submit" name="submit" class="btn btn-success">Assign</button>

                <input type="hidden" name="action" value="new-member">
                <input type="hidden" name="id" value="${project.id}">
            </form>
        </div>
    </body>
</html>
