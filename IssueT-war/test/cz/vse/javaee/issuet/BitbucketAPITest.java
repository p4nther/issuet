package cz.vse.javaee.issuet;

import org.junit.Test;
import static org.junit.Assert.*;
import org.scribe.model.Token;

/**
 *
 * @author ondrejsatera
 */
public class BitbucketAPITest {
    
    /**
     * Test of getRequestTokenEndpoint method, of class BitbucketAPI.
     */
    @Test
    public void testGetRequestTokenEndpoint() {
        System.out.println("getRequestTokenEndpoint");
        BitbucketAPI instance = new BitbucketAPI();
        String expResult = BitbucketAPI.oauthUrl + "/request_token";
        String result = instance.getRequestTokenEndpoint();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAccessTokenEndpoint method, of class BitbucketAPI.
     */
    @Test
    public void testGetAccessTokenEndpoint() {
        System.out.println("getAccessTokenEndpoint");
        BitbucketAPI instance = new BitbucketAPI();
        String expResult = BitbucketAPI.oauthUrl + "/access_token";
        String result = instance.getAccessTokenEndpoint();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAuthorizationUrl method, of class BitbucketAPI.
     */
    @Test
    public void testGetAuthorizationUrl() {
        System.out.println("getAuthorizationUrl");
        Token token = new Token("aa123", "bb456");
        BitbucketAPI instance = new BitbucketAPI();
        String expResult = BitbucketAPI.oauthUrl + "/authenticate?oauth_token=aa123";
        String result = instance.getAuthorizationUrl(token);
        assertEquals(expResult, result);
    }

    /**
     * Test of formatUrl method, of class BitbucketAPI.
     */
    @Test
    public void testFormatUrl() {
        System.out.println("formatUrl");
        String[] queryParams = new String[]{"aa", "bbb"};
        String expResult = BitbucketAPI.serviceUrl + "/aa/bbb";
        String result = BitbucketAPI.formatUrl(queryParams);
        assertEquals(expResult, result);
    }
    
}
