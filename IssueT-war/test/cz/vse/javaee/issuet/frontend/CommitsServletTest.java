package cz.vse.javaee.issuet.frontend;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import static org.easymock.EasyMock.*;

/**
 *
 * @author ondrejsatera
 */
public class CommitsServletTest {
    
    protected CommitsServlet instance;
    
    @Before
    public void setUp() {
        this.instance = new CommitsServlet();
    }

    /**
     * Test of doListCommits method, of class CommitsServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoListCommits() throws Exception {
        System.out.println("doListCommits");
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("projectId")).andReturn(null);
        expectLastCall().times(1);
        response.sendRedirect("projects");
        replay(response);
        replay(request);
        
        instance.doListCommits(request, response);
        
        verify(response);
        verify(request);
    }

    /**
     * Test of doImportCommits method, of class CommitsServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoImportCommits() throws Exception {
        System.out.println("doImportCommits");
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("projectId")).andReturn(null);
        expectLastCall().times(1);
        response.sendRedirect("projects");
        replay(response);
        replay(request);
        
        instance.doImportCommits(request, response);
        
        verify(response);
        verify(request);
    }

    /**
     * Test of doAssignCommitsToIssues method, of class CommitsServlet.
     */
    @Test
    public void testDoAssignCommitsToIssues() throws Exception {
        System.out.println("doAssignCommitsToIssues");
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("projectId")).andReturn(null);
        expectLastCall().times(1);
        response.sendRedirect("projects");
        replay(response);
        replay(request);
        
        instance.doAssignCommitsToIssues(request, response);
        
        verify(response);
        verify(request);
    }
    
}
