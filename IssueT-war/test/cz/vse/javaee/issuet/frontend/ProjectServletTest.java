package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.ProjectDAOBeanLocal;
import cz.vse.javaee.issuet.model.Project;
import java.lang.reflect.Field;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.easymock.EasyMock.*;

/**
 *
 * @author ondrejsatera
 */
public class ProjectServletTest {
    protected ProjectServlet instance;
    
    @Before
    public void setUp() {
        this.instance = new ProjectServlet();
    }

    /**
     * Test of doDeleteProject method, of class ProjectServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoDeleteProject() throws Exception {
        System.out.println("doDeleteProject");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("id")).andReturn(null);
        expectLastCall().times(1);
        response.sendRedirect("projects?error=" + ProjectServlet.ERR_PROJECT_NOT_DELETED);
        replay(response);
        replay(request);
        
        instance.doDeleteProject(request, response);
        
        verify(response);
        verify(request);
    }

    /**
     * Test of doEditProject method, of class ProjectServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoEditProject() throws Exception {
        System.out.println("doEditProject");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("id")).andReturn(null);
        expectLastCall().times(1);
        response.sendRedirect("projects");
        replay(response);
        replay(request);
        
        instance.doEditProject(request, response);
        
        verify(response);
        verify(request);
    }

    /**
     * Test of doPreviewProject method, of class ProjectServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoPreviewProject() throws Exception {
        System.out.println("doPreviewProject");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("id")).andReturn(null);
        expectLastCall().times(1);
        response.sendRedirect("projects");
        replay(response);
        replay(request);
        
        instance.doPreviewProject(request, response);
        
        verify(response);
        verify(request);
    }
    
    /**
     * Test of doPreviewProject method, of class ProjectServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoPreviewProjectNotNull() throws Exception {
        System.out.println("doPreviewProjectNotNull");
        
        Project testProject = new Project();
        testProject.setId(new Long(1));
        
        ProjectDAOBeanLocal mockDAO = createMock(ProjectDAOBeanLocal.class);
        expect(mockDAO.refreshProject(testProject)).andReturn(testProject);
        expect(mockDAO.getProjectById(new Long(1))).andReturn(testProject);
        
        ProjectPageBean pageBean = createMock(ProjectPageBean.class);
        pageBean.setActiveProject(testProject);
        
        Field declaredField = ProjectServlet.class.getSuperclass().getDeclaredField("projectDAO");
        declaredField.setAccessible(true);
        declaredField.set(instance, mockDAO);
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        HttpSession session = createMock(HttpSession.class);
        RequestDispatcher dispatcher = createMock(RequestDispatcher.class);
        
        dispatcher.forward(request, response);
        expect(session.getAttribute("projectBean")).andReturn(pageBean);
        
        expect(request.getParameter("id")).andReturn("1");
        expectLastCall().times(2);
        expect(request.getSession()).andReturn(session);
        expect(request.getRequestDispatcher("project.jsp")).andReturn(dispatcher);
        
        replay(session, response, request, mockDAO, pageBean, dispatcher);
        
        instance.doPreviewProject(request, response);
        
        verify(response, request, mockDAO, dispatcher, pageBean, session);
    }

    /**
     * Test of doListProjectMembers method, of class ProjectServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoListProjectMembers() throws Exception {
        System.out.println("doListProjectMembers");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("id")).andReturn(null);
        expectLastCall().times(1);
        response.sendRedirect("projects");
        replay(response);
        replay(request);
        
        instance.doListProjectMembers(request, response);
        
        verify(response);
        verify(request);
    }
    
    /**
     * Test of doPreviewProject method, of class ProjectServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoListProjectMembersNotNull() throws Exception {
        System.out.println("DoListProjectMembersNotNull");
        
        Project testProject = new Project();
        testProject.setId(new Long(1));
        
        ProjectDAOBeanLocal mockDAO = createMock(ProjectDAOBeanLocal.class);
        expect(mockDAO.refreshProject(testProject)).andReturn(testProject);
        expect(mockDAO.getProjectById(new Long(1))).andReturn(testProject);
        
        ProjectPageBean pageBean = createMock(ProjectPageBean.class);
        pageBean.setActiveProject(testProject);
        
        Field declaredField = ProjectServlet.class.getSuperclass().getDeclaredField("projectDAO");
        declaredField.setAccessible(true);
        declaredField.set(instance, mockDAO);
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        HttpSession session = createMock(HttpSession.class);
        RequestDispatcher dispatcher = createMock(RequestDispatcher.class);
        
        dispatcher.forward(request, response);
        expect(session.getAttribute("projectBean")).andReturn(pageBean);
        
        expect(request.getParameter("id")).andReturn("1");
        expectLastCall().times(2);
        expect(request.getSession()).andReturn(session);
        expect(request.getRequestDispatcher("project-members.jsp")).andReturn(dispatcher);
        
        replay(session, response, request, mockDAO, pageBean, dispatcher);
        
        instance.doListProjectMembers(request, response);
        
        verify(response, request, mockDAO, dispatcher, pageBean, session);
    }

    /**
     * Test of doAssignNewMember method, of class ProjectServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoAssignNewMember() throws Exception {
        System.out.println("doAssignNewMember");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("id")).andReturn(null);
        expectLastCall().times(1);
        response.sendRedirect("projects");
        replay(response);
        replay(request);
        
        instance.doAssignNewMember(request, response);
        
        verify(response);
        verify(request);
    }

    /**
     * Test of doRemoveMember method, of class ProjectServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoRemoveMember() throws Exception {
        System.out.println("doRemoveMember");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("id")).andReturn(null);
        expectLastCall().times(1);
        response.sendRedirect("projects");
        replay(response);
        replay(request);
        
        instance.doRemoveMember(request, response);
        
        verify(response);
        verify(request);
    }
    
}
