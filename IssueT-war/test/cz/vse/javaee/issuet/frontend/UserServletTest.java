package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.UserDAOBeanLocal;
import cz.vse.javaee.issuet.model.Person;
import java.lang.reflect.Field;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.easymock.EasyMock.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author ondrejsatera
 */
public class UserServletTest {
    
    protected UserServlet instance;
    
    @Before
    public void setUp() {
        this.instance = new UserServlet();
    }
    
    /**
     * Test of doCreateUser method, of class UserServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoCreateUserGet() throws Exception {
        System.out.println("doCreateUser");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getMethod()).andReturn("GET");
        expect(request.getParameter("error")).andReturn(null);
        
        RequestDispatcher dispatcher = createMock(RequestDispatcher.class);
        
        dispatcher.forward(request, response);
        expect(request.getRequestDispatcher("user.jsp")).andReturn(dispatcher);
        
        replay(request, response, dispatcher);
        
        instance.doCreateUser(request, response);
        
        verify(request, dispatcher);
    }

    /**
     * Test of doCreateUser method, of class UserServlet.
     * @throws java.lang.Exception
     */
    @Test(expected = ServletException.class)
    public void testDoCreateUserInvalid() throws Exception {
        System.out.println("doCreateUser");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getMethod()).andReturn("POST");
        expect(request.getParameter("email")).andReturn(null);
        expect(request.getParameter("password")).andReturn(null);
        expect(request.getParameter("bitbucketUsername")).andReturn(null);
        
        replay(request, response);
        
        instance.doCreateUser(request, response);
        
        verify(request);
    }
    
    /**
     * Test of doPost method, of class RegisterServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoCreateUserValidDuplicit() throws Exception {
        System.out.println("DoCreateUserValidDuplicit");
        
        Person testPerson = new Person();
        testPerson.setEmail("test@test.cz");
        testPerson.setPassword("pass");
        testPerson.setBitbucketUsername("test");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getMethod()).andReturn("POST");
        expect(request.getParameter("email")).andReturn(testPerson.getEmail());
        expect(request.getParameter("password")).andReturn(testPerson.getPassword());
        expect(request.getParameter("bitbucketUsername")).andReturn(testPerson.getBitbucketUsername());
        
        UserDAOBeanLocal mockUserDAO = createMock(UserDAOBeanLocal.class);
        expect(mockUserDAO.getUserByEmail(testPerson.getEmail())).andReturn(testPerson);
        
        Field declaredField = UserServlet.class.getSuperclass().getDeclaredField("userDAO");
        declaredField.setAccessible(true);
        declaredField.set(instance, mockUserDAO);
        
        response.sendRedirect("user?action=create&err=" + UserServlet.ERR_DUPLICIT);
        
        replay(request, response, mockUserDAO);
        
        instance.doCreateUser(request, response);
        
        verify(request, response, mockUserDAO);
    }
    
    /**
     * Test of doPost method, of class RegisterServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoCreateUserValidNew() throws Exception {
        System.out.println("DoCreateUserValidNew");
        
        Person testPerson = new Person();
        testPerson.setEmail("test@test.cz");
        testPerson.setPassword("pass");
        testPerson.setBitbucketUsername("test");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getMethod()).andReturn("POST");
        expect(request.getParameter("email")).andReturn(testPerson.getEmail());
        expect(request.getParameter("password")).andReturn(testPerson.getPassword());
        expect(request.getParameter("bitbucketUsername")).andReturn(testPerson.getBitbucketUsername());
        
        UserDAOBeanLocal mockUserDAO = createMock(UserDAOBeanLocal.class);
        expect(mockUserDAO.getUserByEmail(testPerson.getEmail())).andReturn(null);
        expect(mockUserDAO.createPerson(testPerson.getEmail(), testPerson.getPassword(), "developer")).andReturn(testPerson);
        expect(mockUserDAO.save(testPerson)).andReturn(testPerson);
        
        Field declaredField = UserServlet.class.getSuperclass().getDeclaredField("userDAO");
        declaredField.setAccessible(true);
        declaredField.set(instance, mockUserDAO);
        
        response.sendRedirect(".?info=" + UserServlet.INF_CREATED);
        
        replay(request, response, mockUserDAO);
        
        instance.doCreateUser(request, response);
        
        verify(request, response, mockUserDAO);
    }

    /**
     * Test of doListUsers method, of class UserServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoListUsersNotLogged() throws Exception {
        System.out.println("doListUsersNotLogged");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        Field loggedUserField = UserServlet.class.getSuperclass().getDeclaredField("loggedUser");
        loggedUserField.setAccessible(true);
        loggedUserField.set(instance, null);
        
        response.sendRedirect(".");
        
        replay(request, response);
        
        instance.doListUsers(request, response);
        
        verify(request, response);
    }
    
    /**
     * Test of doListUsers method, of class UserServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoListUsers() throws Exception {
        System.out.println("doListUsers");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        RequestDispatcher dispatcher = createMock(RequestDispatcher.class);
        
        Person testLoggedUser = new Person();
        
        Field loggedUserField = UserServlet.class.getSuperclass().getDeclaredField("loggedUser");
        loggedUserField.setAccessible(true);
        loggedUserField.set(instance, testLoggedUser);
        
        UserDAOBeanLocal mockUserDAO = createMock(UserDAOBeanLocal.class);
        expect(mockUserDAO.getAll()).andReturn(new ArrayList<Person>());
        
        Field userDAOField = UserServlet.class.getSuperclass().getDeclaredField("userDAO");
        userDAOField.setAccessible(true);
        userDAOField.set(instance, mockUserDAO);
        
        expect(request.getParameter("info")).andReturn("");
        request.setAttribute(anyString(), anyObject(ArrayList.class));
        
        dispatcher.forward(request, response);
        expect(request.getRequestDispatcher("users.jsp")).andReturn(dispatcher);
        
        replay(request, response, mockUserDAO);
        
        instance.doListUsers(request, response);
        
        verify(request, response, mockUserDAO);
    }
    
}
