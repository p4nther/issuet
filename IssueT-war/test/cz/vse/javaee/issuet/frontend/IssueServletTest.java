package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.GithubDAOBeanLocal;
import cz.vse.javaee.issuet.IssueDAOLocal;
import cz.vse.javaee.issuet.ProjectDAOBeanLocal;
import cz.vse.javaee.issuet.model.GithubIssue;
import cz.vse.javaee.issuet.model.GithubRepository;
import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.Repository;
import java.lang.reflect.Field;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.easymock.EasyMock.*;

/**
 *
 * @author ondrejsatera
 */
public class IssueServletTest {
    
    protected IssueServlet instance;
    
    @Before
    public void setUp() {
        this.instance = new IssueServlet();
    }

    /**
     * Test of doCloseIssue method, of class IssueServl
     * @throws java.lang.Exception
     */
    @Test
    public void testDoCloseInternalIssue() throws Exception {
        System.out.println("DoCloseInternalIssue");
        
        Project testProject = new Project();
        testProject.setId(new Long(1));
        
        Issue testIssue = new Issue();
        testIssue.setId(new Long(1));
        testIssue.setProject(testProject);
        
        IssueDAOLocal mockIssueDAO = createMock(IssueDAOLocal.class);
        expect(mockIssueDAO.getIssueById(new Long(1))).andReturn(testIssue);
        expectLastCall().times(2);
        expect(mockIssueDAO.editIssue(testIssue)).andReturn(testIssue);
        
        Field issueDAOField = ProjectServlet.class.getSuperclass().getDeclaredField("issueDAO");
        issueDAOField.setAccessible(true);
        issueDAOField.set(instance, mockIssueDAO);
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("id")).andReturn("1");
        expectLastCall().times(4);
        
        response.sendRedirect("issue?id=" + testIssue.getId() + "&projectId" + testProject.getId() + "&info=" + IssueServlet.INF_CLOSED);
        
        replay(mockIssueDAO, request, response);
        
        instance.doCloseIssue(request, response);
        
        verify(mockIssueDAO, request, response);
    }
    
    /**
     * Test of doCloseIssue method, of class IssueServl
     * @throws java.lang.Exception
     */
    @Test
    public void testDoCloseGithubIssue() throws Exception {
        System.out.println("DoCloseGithubIssue");
        
        GithubRepository testGitHubRepository = new GithubRepository();
        
        Repository testRepository = new Repository();
        testRepository.setGithubRepository(testGitHubRepository);
        
        Project testProject = new Project();
        testProject.setId(new Long(1));
        testProject.setRepository(testRepository);
        
        GithubIssue testGithubIssue = new GithubIssue();
        
        Issue testIssue = new Issue();
        testIssue.setId(new Long(1));
        testIssue.setProject(testProject);
        testIssue.setGithubIssue(testGithubIssue);
        
        IssueDAOLocal mockIssueDAO = createMock(IssueDAOLocal.class);
        expect(mockIssueDAO.getIssueById(new Long(1))).andReturn(testIssue);
        expectLastCall().times(2);
        
        GithubDAOBeanLocal mockGithubDAO = createMock(GithubDAOBeanLocal.class);
        expect(mockGithubDAO.editIssue(testGithubIssue)).andReturn(testGithubIssue);
        
        Field issueDAOField = IssueServlet.class.getSuperclass().getDeclaredField("issueDAO");
        issueDAOField.setAccessible(true);
        issueDAOField.set(instance, mockIssueDAO);
        
        Field githubDAOField = IssueServlet.class.getSuperclass().getDeclaredField("githubDAO");
        githubDAOField.setAccessible(true);
        githubDAOField.set(instance, mockGithubDAO);
        
        GithubPageBean mockGithubPageBean = createMock(GithubPageBean.class);
        mockGithubPageBean.closeIssue(testIssue, testGitHubRepository);
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        HttpSession session = createMock(HttpSession.class);
        
        expect(session.getAttribute(GithubPageBean.GITHUB_PAGE_BEAN)).andReturn(mockGithubPageBean);
        
        expect(request.getParameter("id")).andReturn("1");
        expectLastCall().times(4);
        expect(request.getSession()).andReturn(session);
        
        response.sendRedirect("issue?id=" + testIssue.getId() + "&projectId" + testProject.getId() + "&info=" + IssueServlet.INF_CLOSED);
        
        replay(mockIssueDAO, request, response, mockGithubDAO, mockGithubPageBean, session);
        
        instance.doCloseIssue(request, response);
        
        verify(mockIssueDAO, request, response, mockGithubDAO, mockGithubPageBean, session);
    }
    
}
