package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.model.BitBucketRepository;
import java.util.ArrayList;
import java.util.List;
import static org.easymock.EasyMock.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.scribe.model.Response;

/**
 *
 * @author ondrejsatera
 */
public class BitBucketPageBeanTest {
    
    /**
     * Test of parseRepositoriesFromResponse method, of class BitBucketPageBean.
     * @throws java.lang.Exception
     */
    @Test
    public void testParseRepositoriesFromResponseEmpty() throws Exception {
        System.out.println("ParseRepositoriesFromResponseEmpty");
        Response oauthResponse = createMock(Response.class);
        expect(oauthResponse.getBody()).andReturn("{\"values\":[]}");
        replay(oauthResponse);
        BitBucketPageBean instance = new BitBucketPageBean(null, null);
        List<BitBucketRepository> expResult = new ArrayList<>();
        List<BitBucketRepository> result = instance.parseRepositoriesFromResponse(oauthResponse);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of parseRepositoriesFromResponse method, of class BitBucketPageBean.
     * @throws java.lang.Exception
     */
    @Test
    public void testParseRepositoriesFromResponseNotEmpty() throws Exception {
        System.out.println("ParseRepositoriesFromResponseNotEmpty");
        Response oauthResponse = createMock(Response.class);
        expect(oauthResponse.getBody()).andReturn("{\"pagelen\": 10,\"values\": [{\"scm\": \"git\",\"has_wiki\": false,\"description\": \"\",\"links\": {\"watchers\": {\"href\": \"https://api.bitbucket.org/2.0/repositories/evzijst/atlassian-connect-fork/watchers\"},\"commits\": {\"href\": \"https://api.bitbucket.org/2.0/repositories/evzijst/atlassian-connect-fork/commits\"},\"self\": {\"href\": \"https://api.bitbucket.org/2.0/repositories/evzijst/atlassian-connect-fork\"},\"html\": {\"href\": \"https://bitbucket.org/evzijst/atlassian-connect-fork\"},\"avatar\": {\"href\": \"https://bitbucket.org/m/3a846b46851a/img/language-avatars/default_16.png\"},\"forks\": {\"href\": \"https://api.bitbucket.org/2.0/repositories/evzijst/atlassian-connect-fork/forks\"},\"clone\": [{\"href\": \"https://bitbucket.org/evzijst/atlassian-connect-fork.git\",\"name\": \"https\"},{\"href\": \"ssh://git@bitbucket.org/evzijst/atlassian-connect-fork.git\",\"name\": \"ssh\"}],\"pullrequests\": {\"href\": \"https://api.bitbucket.org/2.0/repositories/evzijst/atlassian-connect-fork/pullrequests\"}},\"fork_policy\": \"allow_forks\",\"language\": \"\",\"created_on\": \"2013-08-26T18:13:07.339080+00:00\",\"parent\": {\"links\": {\"self\": {\"href\": \"https://api.bitbucket.org/2.0/repositories/evzijst/atlassian-connect\"},\"avatar\": {\"href\": \"https://bitbucket-assetroot.s3.amazonaws.com/m/3a846b46851a/img/language-avatars/default_16.png\"}},\"full_name\": \"evzijst/atlassian-connect\",\"name\": \"atlassian-connect\"},\"full_name\": \"evzijst/atlassian-connect-fork\",\"has_issues\": false,\"owner\": {\"username\": \"evzijst\",\"display_name\": \"Erik van Zijst\",\"links\": {\"self\": {\"href\": \"https://api.bitbucket.org/2.0/users/evzijst\"},\"avatar\": {\"href\": \"https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2013/Oct/28/evzijst-avatar-3454044670-3_avatar.png\"}}},\"updated_on\": \"2013-08-26T18:13:07.514065+00:00\",\"size\": 17657351,\"is_private\": false,\"name\": \"atlassian-connect-fork\"}],\"page\": 1,\"next\": \"https://api.bitbucket.org/2.0/repositories?pagelen=1&after=2013-09-26T23%3A01%3A01.638828%2B00%3A00&page=2\"}");
        replay(oauthResponse);
        BitBucketPageBean instance = new BitBucketPageBean(null, null);
        
        BitBucketRepository repo = new BitBucketRepository();
        repo.setName("atlassian-connect-fork");
        repo.setHasIssues(Boolean.FALSE);
        
        List<BitBucketRepository> result = instance.parseRepositoriesFromResponse(oauthResponse);
        
        for (BitBucketRepository bitBucketRepository : result) {
            assertEquals(repo.getName(), bitBucketRepository.getName());
            assertEquals(repo.getHasIssues(), bitBucketRepository.getHasIssues());
        }
    }

}
