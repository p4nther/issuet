package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.UserDAOBeanLocal;
import cz.vse.javaee.issuet.model.Person;
import java.lang.reflect.Field;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.easymock.EasyMock.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author ondrejsatera
 */
public class RegisterServletTest {
    
    protected RegisterServlet instance;
    
    @Before
    public void setUp() {
        this.instance = new RegisterServlet();
    }

    /**
     * Test of doGet method, of class RegisterServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoGet() throws Exception {
        System.out.println("doGet");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        RequestDispatcher dispatcher = createMock(RequestDispatcher.class);
        
        dispatcher.forward(request, response);
        expect(request.getRequestDispatcher("register.jsp")).andReturn(dispatcher);
        
        replay(request, response, dispatcher);
        
        instance.doGet(request, response);
        
        verify(request, response, dispatcher);
    }

    /**
     * Test of doPost method, of class RegisterServlet.
     * @throws java.lang.Exception
     */
    @Test(expected = ServletException.class)
    public void testDoPostInvalid() throws Exception {
        System.out.println("doPost");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("email")).andReturn(null);
        expect(request.getParameter("password")).andReturn(null);
        expect(request.getParameter("bitbucketUsername")).andReturn(null);
        
        replay(request, response);
        
        instance.doPost(request, response);
        
        verify(request);
    }
    
    /**
     * Test of doPost method, of class RegisterServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoPostValidDuplicit() throws Exception {
        System.out.println("doPost");
        
        Person testPerson = new Person();
        testPerson.setEmail("test@test.cz");
        testPerson.setPassword("pass");
        testPerson.setBitbucketUsername("test");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("email")).andReturn(testPerson.getEmail());
        expect(request.getParameter("password")).andReturn(testPerson.getPassword());
        expect(request.getParameter("bitbucketUsername")).andReturn(testPerson.getBitbucketUsername());
        
        UserDAOBeanLocal mockUserDAO = createMock(UserDAOBeanLocal.class);
        expect(mockUserDAO.getUserByEmail(testPerson.getEmail())).andReturn(testPerson);
        
        Field declaredField = RegisterServlet.class.getDeclaredField("userDAO");
        declaredField.setAccessible(true);
        declaredField.set(instance, mockUserDAO);
        
        response.sendRedirect("user?action=create&err=" + UserServlet.ERR_DUPLICIT);
        
        replay(request, response, mockUserDAO);
        
        instance.doPost(request, response);
        
        verify(request, response, mockUserDAO);
    }
    
    /**
     * Test of doPost method, of class RegisterServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoPostValidNew() throws Exception {
        System.out.println("doPost");
        
        Person testPerson = new Person();
        testPerson.setEmail("test@test.cz");
        testPerson.setPassword("pass");
        testPerson.setBitbucketUsername("test");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("email")).andReturn(testPerson.getEmail());
        expect(request.getParameter("password")).andReturn(testPerson.getPassword());
        expect(request.getParameter("bitbucketUsername")).andReturn(testPerson.getBitbucketUsername());
        
        UserDAOBeanLocal mockUserDAO = createMock(UserDAOBeanLocal.class);
        expect(mockUserDAO.getUserByEmail(testPerson.getEmail())).andReturn(null);
        expect(mockUserDAO.createPerson(testPerson.getEmail(), testPerson.getPassword(), "developer")).andReturn(testPerson);
        expect(mockUserDAO.save(testPerson)).andReturn(testPerson);
        
        Field declaredField = RegisterServlet.class.getDeclaredField("userDAO");
        declaredField.setAccessible(true);
        declaredField.set(instance, mockUserDAO);
        
        response.sendRedirect(".?info=" + UserServlet.INF_CREATED);
        
        replay(request, response, mockUserDAO);
        
        instance.doPost(request, response);
        
        verify(request, response, mockUserDAO);
    }
    
}
