package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.ProjectDAOBeanLocal;
import cz.vse.javaee.issuet.RepositoryDAOBeanLocal;
import cz.vse.javaee.issuet.UserDAOBeanLocal;
import cz.vse.javaee.issuet.model.Person;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.Repository;
import java.lang.reflect.Field;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.easymock.EasyMock.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author ondrejsatera
 */
public class ProjectsServletTest {
    
    protected ProjectsServlet instance;
    
    @Before
    public void setUp() {
        this.instance = new ProjectsServlet();
    }

    /**
     * Test of doCreateProject method, of class ProjectsServlet.
     * @throws java.lang.Exception
     */
    @Test(expected = ServletException.class)
    public void testDoCreateProjectInvalid() throws Exception {
        System.out.println("doCreateProjectInvalid");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getMethod()).andReturn("POST");
        expect(request.getParameter("name")).andReturn(null);
        
        replay(request, response);
        
        instance.doCreateProject(request, response);
        
        verify(request, response);
    }
    
    /**
     * Test of doCreateProject method, of class ProjectsServlet.
     * @throws java.lang.Exception
     */
    @Test(expected = ServletException.class)
    public void testDoCreateProjectInvalidMissingRepo() throws Exception {
        System.out.println("doCreateProjectInvalidMissingRepo");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getMethod()).andReturn("POST");
        expect(request.getParameter("name")).andReturn("project");
        expect(request.getParameter("github")).andReturn(null);
        expect(request.getParameter("bitbucket")).andReturn(null);
        
        replay(request, response);
        
        instance.doCreateProject(request, response);
        
        verify(request, response);
    }
    
    /**
     * Test of doCreateProject method, of class ProjectsServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoCreateProjectValid() throws Exception {
        System.out.println("doCreateProjectValid");
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        Project testProject = new Project();
        testProject.setName("project");
        
        expect(request.getMethod()).andReturn("POST");
        expect(request.getParameter("name")).andReturn(testProject.getName());
        expect(request.getParameter("github")).andReturn("");
        expect(request.getParameter("bitbucket")).andReturn("");
        
        Person testLoggedUser = new Person();
        testLoggedUser.setRepositories(new ArrayList<Repository>());
        testLoggedUser.setProjects(new ArrayList<Project>());
        
        Field loggedUserField = IssuesServlet.class.getSuperclass().getDeclaredField("loggedUser");
        loggedUserField.setAccessible(true);
        loggedUserField.set(instance, testLoggedUser);
        
        Repository testRepository = new Repository();
        testRepository.setOwner(testLoggedUser);
        testLoggedUser.getRepositories().add(testRepository);
        
        RepositoryDAOBeanLocal mockRepositoryDAO = createMock(RepositoryDAOBeanLocal.class);
        expect(mockRepositoryDAO.save(anyObject(Repository.class))).andReturn(testRepository);
        
        Field repositoryDAOField = IssuesServlet.class.getSuperclass().getDeclaredField("repositoryDAO");
        repositoryDAOField.setAccessible(true);
        repositoryDAOField.set(instance, mockRepositoryDAO);
        
        testProject.setRepository(testRepository);
        testProject.setOwner(testLoggedUser);
        
        ProjectDAOBeanLocal mockProjectDAO = createMock(ProjectDAOBeanLocal.class);
        expect(mockProjectDAO.createProject(anyObject(Project.class))).andReturn(testProject);
        
        Field projectDAOField = IssuesServlet.class.getSuperclass().getDeclaredField("projectDAO");
        projectDAOField.setAccessible(true);
        projectDAOField.set(instance, mockProjectDAO);
        
        testLoggedUser.getProjects().add(testProject);
        
        UserDAOBeanLocal mockUserDAO = createMock(UserDAOBeanLocal.class);
        expect(mockUserDAO.save(anyObject(Person.class))).andReturn(testLoggedUser);
        
        Field userDAOField = IssuesServlet.class.getSuperclass().getDeclaredField("userDAO");
        userDAOField.setAccessible(true);
        userDAOField.set(instance, mockUserDAO);
        
        testRepository.setProject(testProject);
        
        expect(mockRepositoryDAO.edit(anyObject(Repository.class))).andReturn(testRepository);
        
        testProject.setId(new Long(1));
        
        response.sendRedirect("project?id=" + testProject.getId());
        
        replay(request, response, mockRepositoryDAO, mockProjectDAO, mockUserDAO);
        
        instance.doCreateProject(request, response);
        
        verify(request, response, mockRepositoryDAO, mockProjectDAO, mockUserDAO);
    }
    
}
