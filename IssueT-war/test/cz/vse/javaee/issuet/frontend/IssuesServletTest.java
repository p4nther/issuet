package cz.vse.javaee.issuet.frontend;

import cz.vse.javaee.issuet.IssueDAOLocal;
import cz.vse.javaee.issuet.ProjectDAOBeanLocal;
import cz.vse.javaee.issuet.UserDAOBeanLocal;
import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Person;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.Repository;
import java.lang.reflect.Field;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import static org.easymock.EasyMock.*;

/**
 *
 * @author ondrejsatera
 */
public class IssuesServletTest {
    protected IssuesServlet instance;
    
    @Before
    public void setUp() {
        this.instance = new IssuesServlet();
    }

    /**
     * Test of doCreateIssue method, of class IssuesServlet.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoCreateIssuePost() throws Exception {
        System.out.println("doCreateIssuePost");
        
        Repository testRepository = new Repository();
        
        Person testLoggedUser = new Person();
        testLoggedUser.setIssues(new ArrayList<Issue>());
        
        Project testProject = new Project();
        testProject.setId(new Long(1));
        testProject.setRepository(testRepository);
        testProject.setOwner(testLoggedUser);
        testProject.setMembers(new ArrayList<Person>()); //vyhnuti se rozesilani emailu clenum tymu
        
        Issue testIssue = new Issue();
        testIssue.setName("Issue name");
        testIssue.setProject(testProject);
        testIssue.setLocalOwner(testLoggedUser);
        testIssue.setLocalBody("issue body");
        testIssue.setLocalState("open");
        testIssue.setRepository(testRepository);
        
        Field loggedUserField = IssuesServlet.class.getSuperclass().getDeclaredField("loggedUser");
        loggedUserField.setAccessible(true);
        loggedUserField.set(instance, testLoggedUser);
        
        IssueDAOLocal mockIssueDAO = createMock(IssueDAOLocal.class);
        expect(mockIssueDAO.createIssue(anyObject(Issue.class))).andReturn(testIssue);
        
        Field issueDAOField = IssuesServlet.class.getSuperclass().getDeclaredField("issueDAO");
        issueDAOField.setAccessible(true);
        issueDAOField.set(instance, mockIssueDAO);
        
        UserDAOBeanLocal mockUserDAO = createMock(UserDAOBeanLocal.class);
        expect(mockUserDAO.save(anyObject(Person.class))).andReturn(testLoggedUser);
        
        Field userDAOField = IssuesServlet.class.getSuperclass().getDeclaredField("userDAO");
        userDAOField.setAccessible(true);
        userDAOField.set(instance, mockUserDAO);
        
        HttpServletRequest request = createMock(HttpServletRequest.class);
        HttpServletResponse response = createMock(HttpServletResponse.class);
        
        expect(request.getParameter("projectId")).andReturn("1");
        expectLastCall().times(4);
        expect(request.getMethod()).andReturn("POST");
        expect(request.getParameter("name")).andReturn(testIssue.getName());
        expect(request.getParameter("body")).andReturn(testIssue.getLocalBody());
        
        ProjectDAOBeanLocal mockProjectDAO = createMock(ProjectDAOBeanLocal.class);
        expect(mockProjectDAO.getProjectById(new Long(1))).andReturn(testProject);
        expectLastCall().times(2);
        
        Field projectDAOField = IssuesServlet.class.getSuperclass().getDeclaredField("projectDAO");
        projectDAOField.setAccessible(true);
        projectDAOField.set(instance, mockProjectDAO);
        
        response.sendRedirect("issue?id=" + testIssue.getId() + "&projectId=" + testProject.getId());
        
        replay(mockIssueDAO, mockUserDAO, mockProjectDAO, request, response);
        
        instance.doCreateIssue(request, response);
        
        verify(mockIssueDAO, mockUserDAO, mockProjectDAO, request, response);
    }
   
}
