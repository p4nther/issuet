package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.Repository;
import javax.ejb.Stateless;

/**
 * Implementation of EJB for managing internal repositories
 * @author ondrejsatera
 */
@Stateless
public class RepositoryDAOBean extends DAO implements RepositoryDAOBeanLocal {

    /**
     * Saves changes in the old repository or persists a new repository
     * @param repository
     * @return refreshed/newly persisted entity
     */
    @Override
    public Repository save(Repository repository) {
        Repository resultRepository;
        if (repository.getId() == null 
                || entityManager.find(Repository.class, repository.getId()) == null) {
            entityManager.persist(repository);
            entityManager.flush();
            resultRepository = entityManager.merge(repository);
        } else {
            resultRepository = entityManager.merge(repository);
        }
        
        return resultRepository;
    }
    
    /**
     * Saves changes in the already saved repository
     * @param repository
     * @return refreshed entity
     */
    @Override
    public Repository edit(Repository repository) {
        Repository resultRepository = entityManager.merge(repository);
        entityManager.flush();
        
        return resultRepository;
    }
}
