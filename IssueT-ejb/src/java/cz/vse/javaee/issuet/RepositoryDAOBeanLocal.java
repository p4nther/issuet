package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.Repository;
import javax.ejb.Local;

/**
 * Interface for DAO EJB which manages internal repositories
 * @author ondrejsatera
 */
@Local
public interface RepositoryDAOBeanLocal {

    Repository save(Repository repository);

    Repository edit(Repository repository);
    
}
