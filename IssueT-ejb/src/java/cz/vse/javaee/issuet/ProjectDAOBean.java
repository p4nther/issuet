package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.Person;
import cz.vse.javaee.issuet.model.Project;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 * Implementation of EJB for managing internal projects
 * @author ondrejsatera
 */
@Stateless
@Local(ProjectDAOBeanLocal.class)
public class ProjectDAOBean extends DAO implements ProjectDAOBeanLocal {

    /**
     * Persists new project
     * @param project
     * @return newly persisted entity
     */
    @Override
    public Project createProject(Project project) {
        entityManager.persist(project);
        entityManager.flush();
        entityManager.refresh(project);
        
        return project;
    }

    /**
     * Return project with specified identifier
     * @param id
     * @return Project if found, otherwise null
     */
    @Override
    public Project getProjectById(Long id) {
        return entityManager.find(Project.class, id);
    }

    /**
     * Returns all projects which owns given user
     * @param owner
     * @return list of owners projects
     */
    @Override
    public List<Project> getAllByOwner(Person owner) {
        Query query = entityManager.createQuery("SELECT p FROM Project p WHERE p.owner=:owner");
        query.setParameter("owner", owner);
        return (List<Project>) query.getResultList();
    }

    /**
     * Merges changes in the given project
     * @param project
     * @return refreshed entity
     */
    @Override
    public Project editProject(Project project) {
        Project merge = entityManager.merge(project);
        entityManager.flush();
        return merge;
    }

    /**
     * Removes given project from the persistence unit
     * @param project
     */
    @Override
    public void removeProject(Project project) {
        entityManager.merge(project);
        entityManager.remove(project);
        entityManager.flush();
    }

    /**
     * Refreshes given project with the state in the persistence unit
     * @param project
     * @return refreshed entity
     */
    @Override
    public Project refreshProject(Project project) {
        Project resultProject = entityManager.merge(project);
        entityManager.refresh(resultProject);
        return resultProject;
    }
}
