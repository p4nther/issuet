package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.GithubIssue;
import cz.vse.javaee.issuet.model.GithubRepository;
import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.RepositoryCommit;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface for DAO EJB which manages entities from GitHub VCS
 * @author ondrejsatera
 */
@Local
public interface GithubDAOBeanLocal {

    GithubRepository getRepository(String name);
    
    List<GithubRepository> getAll();
    
    GithubRepository save(GithubRepository repository);
    
    GithubIssue getIssue(Integer number);
    
    GithubIssue saveIssue(GithubIssue issue);
    
    GithubIssue editIssue(GithubIssue issue);
    
    RepositoryCommit getCommit(String sha);
    
    RepositoryCommit saveCommit(RepositoryCommit commit);
    
    RepositoryCommit editCommit(RepositoryCommit commit);
    
    boolean issueHasCommitAssigned(GithubIssue issue, RepositoryCommit commit);
    
    List<Issue> getIssues(Project project, String filter);
}
