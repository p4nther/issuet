package cz.vse.javaee.issuet;

import com.sun.xml.wss.impl.misc.Base64;
import cz.vse.javaee.issuet.model.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.scribe.model.Token;

/**
 * EJB for working with internal user
 * Stavova bean pro praci s aktualne prihlasenym uzivatelem
 * @author ondrejsatera
 */
@Stateless
public class UserDAOBean extends DAO implements UserDAOBeanLocal {
    
    /**
     * Const string for SQL query to fetch user by email and password
     */
    public static final String USER_BY_EMAIL = "USER_BY_EMAIL";
    
    /**
     * Returns person with given email and password. Used when logging in a user.
     * @param email
     * @return Person if found
     * @throws NoUserFoundException if person is not found
     */
    @Override
    public Person getUserByEmail(String email) throws NoUserFoundException {
        Query query = entityManager.createNamedQuery(USER_BY_EMAIL);
        query.setParameter("email", email);
        try {
            Person foundUser = (Person) query.getSingleResult();
            
            return foundUser;
        } catch (NoResultException e) {
            throw new NoUserFoundException(email);
        }
    }

    /**
     * Persists new person with given email, password and groupname. Password will be digested to be later used with JDBC Realm.
     * @param email
     * @param password
     * @param groupname
     * @return newly persisted person
     */
    @Override
    public Person createPerson(String email, String password, String groupname) {
        Person newPerson = new Person();
        newPerson.setEmail(email);
        newPerson.setPassword(Base64.encode(digest(password)));
        
        entityManager.persist(newPerson);
        entityManager.flush();
        entityManager.refresh(newPerson);
        
        PersonGroup newGroup = new PersonGroup();
        newGroup.setGroupname(groupname);
        newGroup.setUseremail(email);
        entityManager.persist(newGroup);
        
        entityManager.flush();
        
        return newPerson;
    }

    /**
     * Saves changes in the person entity
     * @param person
     * @return refreshed entity
     */
    @Override
    public Person save(Person person) {
        Person resultPerson = entityManager.merge(person);
        entityManager.flush();
        return resultPerson;
    }
    
    /**
     * Saves GitHub token in the person entity
     * @param person
     * @param token
     */
    @Override
    public void saveGithubToken(Person person, String token) {
        person.setGithubToken(token);
        entityManager.merge(person);
        entityManager.flush();
    }
    
    /**
     * Saves BitBucket token in the person entity
     * @param person
     * @param token
     */
    @Override
    public void saveBitbucketToken(Person person, Token token) {
        person.setBitbucketToken(token);
        entityManager.merge(person);
        entityManager.flush();
    }

    /**
     * Returns list of all Persons in the persistence unit
     * @return list of all Persons in the persistence unit
     */
    @Override
    public List<Person> getAll() {
        Query query = entityManager.createQuery("SELECT p FROM Person p");
        return (List<Person>) query.getResultList();
    }
    
    /**
     * Digests given string with crypting algorithm SHA-256
     * @param input
     * @return encrypted string
     */
    private byte[] digest(String input) {
        byte[] output = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            output = md.digest(input.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(UserDAOBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }
}
