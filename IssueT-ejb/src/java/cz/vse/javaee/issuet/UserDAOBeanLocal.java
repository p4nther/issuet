package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.Person;
import java.util.List;
import javax.ejb.Local;
import org.scribe.model.Token;

/**
 * Interface for DAO EJB which manages internal users
 * @author ondrejsatera
 */
@Local
public interface UserDAOBeanLocal {
    
    Person getUserByEmail(String email) throws NoUserFoundException;
    
    Person createPerson(String email, String password, String groupname);

    void saveGithubToken(Person person, String token);

    void saveBitbucketToken(Person person, Token token);

    Person save(Person loggedUser);

    List<Person> getAll();
}
