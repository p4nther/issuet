package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.GithubIssue;
import cz.vse.javaee.issuet.model.GithubRepository;
import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.RepositoryCommit;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 * Implementation of EJB for managing GitHub VCS entities
 * @author ondrejsatera
 */
@Stateless
public class GithubDAOBean extends DAO implements GithubDAOBeanLocal {
    
    private static final String ISSUE_OPEN = "open";
    private static final String ISSUE_CLOSED = "closed";

    /**
     * Returns repository with this name
     * @param name
     * @return GithubRepository if found, otherwise null
     */
    @Override
    public GithubRepository getRepository(String name) {
        return this.entityManager.find(GithubRepository.class, name);
    }

    /**
     * Returns list of all BitBucket repositories
     * @return list of all BitBucket repositories
     */
    @Override
    public List<GithubRepository> getAll() {
        Query query = entityManager.createQuery("SELECT g FROM GithubRepository g");
        return (List<GithubRepository>) query.getResultList();
    }

    /**
     * Saves changes in the old repository or persists a new repository
     * @param repository
     * @return refreshed entity
     */
    @Override
    public GithubRepository save(GithubRepository repository) {
        if (entityManager.find(GithubRepository.class, repository.getName()) == null) {
            entityManager.persist(repository);
            entityManager.flush();
        }
        return entityManager.merge(repository);
    }

    /**
     * Returns GitHub issue with this number (GitHub's internal identifier)
     * @param number
     * @return GithubIssue if found, otherwise null
     */
    @Override
    public GithubIssue getIssue(Integer number) {
        return entityManager.find(GithubIssue.class, number);
    }

    /**
     * Saves changes in the old issue or persists a new issue
     * @param issue
     * @return refreshed entity
     */
    @Override
    public GithubIssue saveIssue(GithubIssue issue) {
        if (entityManager.find(GithubIssue.class, issue.getNumber()) == null) {
            entityManager.persist(issue);
            entityManager.flush();
        }
        return entityManager.merge(issue);
    }

    /**
     * Saves changes in the already created issue
     * @param issue
     * @return refreshed entity
     */
    @Override
    public GithubIssue editIssue(GithubIssue issue) {
        GithubIssue GHissue = entityManager.merge(issue);
        entityManager.flush();
        return GHissue;
    }

    /**
     * Returns commit with sha identifier
     * @param sha
     * @return
     */
    @Override
    public RepositoryCommit getCommit(String sha) {
        return entityManager.find(RepositoryCommit.class, sha);
    }

    /**
     * Saves changes in the old commit or persists a new commit
     * @param commit
     * @return refreshed entity
     */
    @Override
    public RepositoryCommit saveCommit(RepositoryCommit commit) {
        if (getCommit(commit.getSha()) == null) {
            entityManager.persist(commit);
            entityManager.flush();
        }
        return entityManager.merge(commit);
    }

    /**
     * Saves changes in the already created commit
     * @param commit
     * @return refreshed entity
     */
    @Override
    public RepositoryCommit editCommit(RepositoryCommit commit) {
        RepositoryCommit RCcommit = entityManager.merge(commit);
        entityManager.flush();
        return RCcommit;
    }
    
    /**
     * Checks whether this issue has assigned this commit
     * @param issue
     * @param commit
     * @return true if issue has the commit already assigned, false otherwise
     */
    @Override
    public boolean issueHasCommitAssigned(GithubIssue issue, RepositoryCommit commit) {
        for (RepositoryCommit rc : issue.getCommits()) {
            if (rc.getSha() == null ? commit.getSha() == null : rc.getSha().equals(commit.getSha())) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Returns all issues from the project filtered by the specified filter string
     * @param project
     * @param filter
     * @return list of issues
     */
    @Override
    public List<Issue> getIssues(Project project, String filter) {
        Query query;
        if (ISSUE_OPEN.equals(filter) || ISSUE_CLOSED.equals(filter)) {
            query = entityManager.createQuery("SELECT i FROM GithubIssue i WHERE i.repository=:repo AND i.state=:filter");
            query.setParameter("repo", project.getRepository().getGithubRepository());
            query.setParameter("filter", filter);
        } else {
            query = entityManager.createQuery("SELECT i FROM GithubIssue i WHERE i.repository=:repo AND (i.number=:filterInteger OR i.title LIKE :filter OR i.body LIKE :filter)");
            query.setParameter("repo", project.getRepository().getGithubRepository());
            query.setParameter("filter", "%" + filter + "%");
            Integer filterInteger;
            try {
                filterInteger = Integer.parseInt(filter);
            }
            catch (NumberFormatException ex) {
                filterInteger = 0;
            }
            query.setParameter("filterInteger", filterInteger);
        }
        
        List<GithubIssue> issues = query.getResultList();
        List<Issue> result = new ArrayList<>();
        for (GithubIssue githubIssue : issues) {
            result.add(githubIssue.getIssue());
        }
        return result;
    }
    
}
