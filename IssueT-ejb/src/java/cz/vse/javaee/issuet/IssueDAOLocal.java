package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Project;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface for DAO EJB which manages internal issues
 * @author ondrejsatera
 */
@Local
public interface IssueDAOLocal {
    Issue createIssue(Issue issue);
    
    Issue getIssueById(Long id);
    
    List<Issue> getIssues(Project project, String filter);
    
    void removeIssue(Issue issue);
    
    Issue editIssue(Issue issue);
    
    Issue refreshIssue(Issue issue);
}
