package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.Repository;
import cz.vse.javaee.issuet.model.RepositoryCommit;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface for DAO EJB which manages commits
 * @author ondrejsatera
 */
@Local
public interface CommitDAOBeanLocal {
    
    RepositoryCommit getCommit(String sha);

    RepositoryCommit editCommit(RepositoryCommit commit);

    List<RepositoryCommit> getCommits(Repository repository);
    
}
