package cz.vse.javaee.issuet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Specific issue entity used for Google Code
 * @author ondrejsatera
 */
@Entity
public class GoogleCodeIssue implements IRepositoryIssue, Serializable {

    private GoogleCodeRepository repository;
    private List<RepositoryCommit> commits = new ArrayList<>();
    private Issue issue;
    private String title;
    private String description;
    
    @Override
    @Id
    public String getTitle() {
        return this.title;
    }

    /**
     * Provides consistent access to issue's main content
     * @return issue content
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    @ManyToMany(cascade = CascadeType.REMOVE)
    public List<RepositoryCommit> getCommits() {
        return this.commits;
    }

    @Override
    @OneToOne
    public Issue getIssue() {
        return this.issue;
    }

    @Override
    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void setCommits(List<RepositoryCommit> commits) {
        this.commits = commits;
    }

    @ManyToOne
    public GoogleCodeRepository getRepository() {
        return repository;
    }

    public void setRepository(GoogleCodeRepository repository) {
        this.repository = repository;
    }
    
}
