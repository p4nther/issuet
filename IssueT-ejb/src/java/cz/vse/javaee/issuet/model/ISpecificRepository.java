package cz.vse.javaee.issuet.model;

import java.io.Serializable;
import java.util.List;

/**
 * Interface for all specific repositories
 * @author ondrejsatera
 */
public interface ISpecificRepository extends Serializable {

    List<IRepositoryIssue> getIssues();

    String getName();
    
    void setName(String name);
}
