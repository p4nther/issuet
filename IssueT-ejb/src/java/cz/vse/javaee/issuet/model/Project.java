package cz.vse.javaee.issuet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Entity for internal projects
 * @author ondrejsatera
 */
@Entity
public class Project implements Serializable {
    private Long id;
    private String name;
    private String projectDescription;
    private List<Issue> issues;
    private Repository repository;
    private Person owner;
    private List<Person> members = new ArrayList<>();

    @Id @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE)
    public List<Issue> getIssues() {
        return issues;
    }

    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }

    @OneToOne(optional = true, cascade = CascadeType.REMOVE)
    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    @ManyToOne
    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    @ManyToMany(mappedBy = "teamProjects")
    public List<Person> getMembers() {
        return members;
    }

    public void setMembers(List<Person> members) {
        this.members = members;
    }
    
    /**
     * Checks whether given person exists in the project in either owner or member role
     * @param person
     * @return true if person exists in the project, otherwise false
     */
    public boolean hasMember(Person person) {
        if (person == null)
            return false;
        if (owner != null && owner.getEmail().equals(person.getEmail()))
            return true;
        for (Person member : members) {
            if (member.getEmail().equals(person.getEmail()))
                return true;
        }
        return false;
    }

    /**
     * Removes given person from the team members
     * @param person
     */
    public void removeMember(Person person) {
        members.remove(person);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Project))
            return false;
        Project compare = (Project) obj;
        return compare.getId() == this.getId();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
}
