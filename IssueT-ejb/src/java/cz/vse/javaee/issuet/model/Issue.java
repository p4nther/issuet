package cz.vse.javaee.issuet.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 * Entity for internal issue which also holds specific issues form VCSs and provides consistent access to them
 * @author ondrejsatera
 */
@Entity
public class Issue implements Serializable {
    private Long id;
    private String name;
    private String localBody;
    private String localState;
    private Person localOwner;
    private IRepositoryIssue repositoryIssue;
    private Project project;
    private Repository repository;
    private BitBucketIssue bitbucketIssue;
    private GithubIssue githubIssue;
    private GoogleCodeIssue googleCodeIssue;

    @Id @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocalState() {
        return localState;
    }

    public void setLocalState(String localState) {
        this.localState = localState;
    }

    public String getLocalBody() {
        return localBody;
    }

    public void setLocalBody(String localBody) {
        this.localBody = localBody;
    }

    @ManyToOne(optional = true)
    public Person getLocalOwner() {
        return localOwner;
    }

    public void setLocalOwner(Person localOwner) {
        this.localOwner = localOwner;
    }
    
    /**
     * Returns assigned specific issue from VCS if any
     * @return IRepositoryIssue or null if none is assigned
     */
    @Transient
    public IRepositoryIssue getRepositoryIssue() {
        if (repositoryIssue == null) {
            if (githubIssue != null)
                setRepositoryIssue(githubIssue);
            if (bitbucketIssue != null)
                setRepositoryIssue(bitbucketIssue);
            if (googleCodeIssue != null)
                setRepositoryIssue(googleCodeIssue);
        }
        return repositoryIssue;
    }

    private void setRepositoryIssue(IRepositoryIssue repositoryIssue) {
        this.repositoryIssue = repositoryIssue;
    }

    @ManyToOne
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @ManyToOne
    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    @OneToOne(optional = true, cascade = CascadeType.REMOVE)
    public BitBucketIssue getBitbucketIssue() {
        return bitbucketIssue;
    }

    public void setBitbucketIssue(BitBucketIssue bitbucketIssue) {
        this.bitbucketIssue = bitbucketIssue;
        setRepositoryIssue(bitbucketIssue);
    }

    @OneToOne(optional = true, cascade = CascadeType.REMOVE)
    public GithubIssue getGithubIssue() {
        return githubIssue;
    }

    public void setGithubIssue(GithubIssue githubIssue) {
        this.githubIssue = githubIssue;
        setRepositoryIssue(githubIssue);
    }

    @OneToOne(optional = true, cascade = CascadeType.REMOVE)
    public GoogleCodeIssue getGoogleCodeIssue() {
        return googleCodeIssue;
    }

    public void setGoogleCodeIssue(GoogleCodeIssue googleCodeIssue) {
        this.googleCodeIssue = googleCodeIssue;
        setRepositoryIssue(googleCodeIssue);
    }
    
    /**
     * Returns identifier for this issue - internal or specific
     * @return specific identifier if some specific issue is set, otherwise internal
     */
    @Transient
    public String getSpecificIdentifier() {
        String specificIdentifier;
        if (githubIssue != null) {
            specificIdentifier = githubIssue.getNumber().toString();
        } else if (bitbucketIssue != null) {
            specificIdentifier = bitbucketIssue.getLocalId().toString();
        } else if (googleCodeIssue != null) {
            specificIdentifier = googleCodeIssue.getTitle();
        } else {
            specificIdentifier = getId().toString();
        }
        return specificIdentifier;
    }
    
    /**
     * Returns state of this issue - internal or specific
     * @return specific state if some specific issue is set, otherwise internal
     */
    @Transient
    public String getSpecificState() {
        String specificState;
        if (githubIssue != null) {
            specificState = githubIssue.getState();
        } else if (bitbucketIssue != null) {
            specificState = bitbucketIssue.getStatus();
        } else if (googleCodeIssue != null) {
            specificState = googleCodeIssue.getTitle();
        } else {
            specificState = getLocalState();
        }
        return specificState;
    }
    
    /**
     * Returns body of this issue - internal or specific
     * @return specific body if some specific issue is set, otherwise internal
     */
    @Transient
    public String getBody() {
        return getRepositoryIssue() == null || getRepositoryIssue().getDescription().isEmpty() ? getLocalBody() : getRepositoryIssue().getDescription();
    }
    
    /**
     * Checks whether this issue is in the opened state
     * @return true if open, otherwise false
     */
    @Transient
    public boolean isOpen() {
        return getSpecificState().equals("open") || getSpecificState().equals("new");
    }

}
