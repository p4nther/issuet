package cz.vse.javaee.issuet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Specific issue entity used for GitHub
 * @author ondrejsatera
 */
@Entity
public class GithubIssue implements IRepositoryIssue, Serializable {

    private GithubRepository repository;
    private List<RepositoryCommit> commits = new ArrayList<>();
    private Issue issue;
    private String title;
    private String body;
    private String state;
    private Integer number;

    @Id
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
    
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    @Override
    public String getTitle() {
        return this.title;
    }

    /**
     * Provides consistent access to issue's main content
     * @return issue content
     */
    @Override
    public String getDescription() {
        return getBody();
    }

    @Override
    @ManyToMany(cascade = CascadeType.REMOVE)
    public List<RepositoryCommit> getCommits() {
        return this.commits;
    }

    @Override
    @OneToOne
    public Issue getIssue() {
        return this.issue;
    }

    @Override
    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setDescription(String description) {
        setBody(description);
    }

    @Override
    public void setCommits(List<RepositoryCommit> commits) {
        this.commits = commits;
    }

    @ManyToOne
    public GithubRepository getRepository() {
        return repository;
    }

    public void setRepository(GithubRepository repository) {
        this.repository = repository;
    }
}
