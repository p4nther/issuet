package cz.vse.javaee.issuet.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 * Specific repository entity used for Google Code
 * @author ondrejsatera
 */
@Entity
public class GoogleCodeRepository implements ISpecificRepository {

    private String name;
    private List<GoogleCodeIssue> issues;
    private Person owner;

    @Override
    @Id
    public String getName() {
        return this.name;
    }
    
    /**
     * Provides consistent access to repository's issues
     * @return list of repository issues
     */
    @Override
    @Transient
    public List<IRepositoryIssue> getIssues() {
        List<IRepositoryIssue> result = new ArrayList<>();
        for (GoogleCodeIssue googleCodeIssue : getRepositoryIssues()) {
            result.add(googleCodeIssue);
        }
        return result;
    }
    
    @OneToMany(mappedBy = "repository", cascade = CascadeType.REMOVE)
    public List<GoogleCodeIssue> getRepositoryIssues() {
        return this.issues;
    }

    public void setRepositoryIssues(List<GoogleCodeIssue> issues) {
        this.issues = issues;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }
    
}
