package cz.vse.javaee.issuet.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 * Specific repository entity used for GitHub
 * @author ondrejsatera
 */
@Entity
public class GithubRepository implements ISpecificRepository {
    
    private String name;
    private String fullName;
    private String description;
    private List<GithubIssue> issues;
    private org.eclipse.egit.github.core.Repository githubRepo;
    private Person owner;
    
    /**
     * Provides consistent access to repository's issues
     * @return list of repository issues
     */
    @Override
    @Transient
    public List<IRepositoryIssue> getIssues() {
        List<IRepositoryIssue> result = new ArrayList<>();
        for (GithubIssue githubIssue : getRepositoryIssues()) {
            result.add(githubIssue);
        }
        return result;
    }
    
    @OneToMany(mappedBy = "repository", cascade = CascadeType.REMOVE)
    public List<GithubIssue> getRepositoryIssues() {
        return this.issues;
    }

    @Override
    @Id
    public String getName() {
        return this.name;
    }

    public void setRepositoryIssues(List<GithubIssue> issues) {
        this.issues = issues;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
    
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns repository entity received from GitHub API
     * @return repository entity
     */
    @Transient
    public org.eclipse.egit.github.core.Repository getGithubRepo() {
        return githubRepo;
    }

    public void setGithubRepo(org.eclipse.egit.github.core.Repository githubRepo) {
        this.githubRepo = githubRepo;
    }

    @ManyToOne
    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }
    
}
