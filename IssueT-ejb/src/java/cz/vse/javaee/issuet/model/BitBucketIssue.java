package cz.vse.javaee.issuet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 * Specific issue entity used for BitBucket
 * @author ondrejsatera
 */
@Entity
public class BitBucketIssue implements IRepositoryIssue, Serializable {

    private BitBucketRepository repository;
    private String title;
    private String content;
    private String status;
    private String priority;
    private List<RepositoryCommit> commits = new ArrayList<>();
    private Issue issue;
    private Long localId;

    @Id
    public Long getLocalId() {
        return localId;
    }

    public void setLocalId(Long localId) {
        this.localId = localId;
    }
    
    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
    
    /**
     * Provides consistent access to issue's main content
     * @return issue content
     */
    @Override
    @Transient
    public String getDescription() {
        return this.getContent();
    }

    @Override
    @ManyToMany(cascade = CascadeType.REMOVE)
    public List<RepositoryCommit> getCommits() {
        return this.commits;
    }

    @Override
    @OneToOne
    public Issue getIssue() {
        return this.issue;
    }

    @Override
    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    @Override
    public void setDescription(String description) {
        this.setContent(description);
    }

    @Override
    public void setCommits(List<RepositoryCommit> commits) {
        this.commits = commits;
    }

    @ManyToOne
    public BitBucketRepository getRepository() {
        return repository;
    }

    public void setRepository(BitBucketRepository repository) {
        this.repository = repository;
    }
    
}
