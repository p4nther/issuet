package cz.vse.javaee.issuet.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 * Specific repository entity used for BitBucket
 * @author ondrejsatera
 */
@Entity
public class BitBucketRepository implements ISpecificRepository {
    
    private String name;
    private Boolean hasIssues;
    private List<BitBucketIssue> issues;
    private Person owner;

    /**
     * Provides consistent access to repository's issues
     * @return list of repository issues
     */
    @Override
    @Transient
    public List<IRepositoryIssue> getIssues() {
        List<IRepositoryIssue> result = new ArrayList<>();
        for (BitBucketIssue bitBucketIssue : getRepositoryIssues()) {
            result.add(bitBucketIssue);
        }
        return result;
    }
    
    @OneToMany(mappedBy = "repository", cascade = CascadeType.REMOVE)
    public List<BitBucketIssue> getRepositoryIssues() {
        return this.issues;
    }

    @Override
    @Id
    public String getName() {
        return this.name;
    }

    public void setRepositoryIssues(List<BitBucketIssue> issues) {
        this.issues = issues;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
    
    public Boolean getHasIssues() {
        return hasIssues;
    }

    public void setHasIssues(Boolean hasIssues) {
        this.hasIssues = hasIssues;
    }

    @ManyToOne
    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }
    
    public String getSlug() {
        return getName().toLowerCase();
    }
    
}
