package cz.vse.javaee.issuet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 * Entity for internal repository which holds specific repositories (BitBucket, Github etc.)
 * @author ondrejsatera
 */
@Entity
public class Repository implements Serializable {
    
    private Integer id;
    private Project project;
    private Person owner;
    private List<RepositoryCommit> commits;
    private ISpecificRepository specificRepository;
    private BitBucketRepository bitBucketRepository;
    private GithubRepository githubRepository;
    private GoogleCodeRepository googleCodeRepository;

    @Id
    @GeneratedValue
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "repository", cascade = CascadeType.REMOVE)
    public List<RepositoryCommit> getCommits() {
        return this.commits;
    }

    public void setCommits(List<RepositoryCommit> commits) {
        this.commits = commits;
    }

    /**
     * Returns list of issues from specific repository if any
     * @return list of IRepositoryIssue
     */
    @Transient
    public List<IRepositoryIssue> getIssues() {
        if (specificRepository == null)
            return new ArrayList<>();
        return specificRepository.getIssues();
    }

    @OneToOne
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @ManyToOne
    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    /**
     * Returns assigned specific repository
     * @return ISpecificRepository or null if specific repository is not set
     */
    @Transient
    public ISpecificRepository getSpecificRepository() {
        return specificRepository;
    }

    private void setSpecificRepository(ISpecificRepository specificRepository) {
        if (specificRepository == null)
            return;
        this.specificRepository = specificRepository;
    }

    @OneToOne(optional = true)
    public BitBucketRepository getBitBucketRepository() {
        return bitBucketRepository;
    }

    /**
     * Saves the BitBucket repository in the corresponding attribute and sets it as a specific repository for this internal repository
     * @param bitBucketRepository
     */
    public void setBitBucketRepository(BitBucketRepository bitBucketRepository) {
        if (bitBucketRepository == null)
            return;
        this.bitBucketRepository = bitBucketRepository;
        setSpecificRepository(bitBucketRepository);
    }

    @OneToOne(optional = true)
    public GithubRepository getGithubRepository() {
        return githubRepository;
    }

    /**
     * Saves the GitHub repository in the corresponding attribute and sets it as a specific repository for this internal repository
     * @param githubRepository
     */
    public void setGithubRepository(GithubRepository githubRepository) {
        if (githubRepository == null)
            return;
        this.githubRepository = githubRepository;
        setSpecificRepository(githubRepository);
    }

    @OneToOne(optional = true)
    public GoogleCodeRepository getGoogleCodeRepository() {
        return googleCodeRepository;
    }

    /**
     * Saves the Google Code repository in the corresponding attribute and sets it as a specific repository for this internal repository
     * @param googleCodeRepository
     */
    public void setGoogleCodeRepository(GoogleCodeRepository googleCodeRepository) {
        if (googleCodeRepository == null)
            return;
        this.googleCodeRepository = googleCodeRepository;
        setSpecificRepository(googleCodeRepository);
    }
    
}
