package cz.vse.javaee.issuet.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import org.scribe.model.Token;

/**
 * Entity for user - used for logging in, managing team members and project/issue owners
 * @author ondrejsatera
 */
@Entity
public class Person implements Serializable {
    
    private List<Issue> issues;
    private List<Project> teamProjects;
    private String email;
    private String password;
    private String githubToken;
    private Token bitbucketToken;
    private String bitbucketUsername;
    private List<Project> projects;
    private List<Repository> repositories;
    private Long version;
    private List<GoogleCodeRepository> googleCodeRepositories;
    private List<GithubRepository> githubRepositories;
    private List<BitBucketRepository> bitBucketRepositories;

    @Id
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE)
    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE)
    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    @Version
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getGithubToken() {
        return githubToken;
    }

    public void setGithubToken(String githubToken) {
        this.githubToken = githubToken;
    }

    public Token getBitbucketToken() {
        return bitbucketToken;
    }

    public void setBitbucketToken(Token bitbucketToken) {
        this.bitbucketToken = bitbucketToken;
    }

    @OneToMany(mappedBy = "owner")
    public List<BitBucketRepository> getBitBucketRepositories() {
        return bitBucketRepositories;
    }

    public void setBitBucketRepositories(List<BitBucketRepository> bitBucketRepositories) {
        this.bitBucketRepositories = bitBucketRepositories;
    }

    @OneToMany(mappedBy = "owner")
    public List<GithubRepository> getGithubRepositories() {
        return githubRepositories;
    }

    public void setGithubRepositories(List<GithubRepository> githubRepositories) {
        this.githubRepositories = githubRepositories;
    }

    @OneToMany(mappedBy = "owner")
    public List<GoogleCodeRepository> getGoogleCodeRepositories() {
        return googleCodeRepositories;
    }

    public void setGoogleCodeRepositories(List<GoogleCodeRepository> googleCodeRepositories) {
        this.googleCodeRepositories = googleCodeRepositories;
    }

    @ManyToMany
    public List<Project> getTeamProjects() {
        return teamProjects;
    }

    public void setTeamProjects(List<Project> teamProjects) {
        this.teamProjects = teamProjects;
    }

    public void removeTeamProject(Project project) {
        teamProjects.remove(project);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Person) {
            Person compare = (Person) obj;
            return compare.getEmail().equals(this.email);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @OneToMany(mappedBy = "localOwner")
    public List<Issue> getIssues() {
        return issues;
    }

    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }

    public String getBitbucketUsername() {
        return bitbucketUsername;
    }

    public void setBitbucketUsername(String bitbucketUsername) {
        this.bitbucketUsername = bitbucketUsername;
    }
    
}
