package cz.vse.javaee.issuet.model;

import java.util.List;

/**
 * Interface for all specific repository issues
 * @author ondrejsatera
 */
public interface IRepositoryIssue {
    
    String getTitle();
    
    void setTitle(String name);
    
    Issue getIssue();
    
    void setIssue(Issue issue);
    
    String getDescription();
    
    void setDescription(String description);
    
    List<RepositoryCommit> getCommits();
    
    void setCommits(List<RepositoryCommit> commits);
}
