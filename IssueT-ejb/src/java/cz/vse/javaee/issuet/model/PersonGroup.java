package cz.vse.javaee.issuet.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * Entity for user group, etc. developer, administrator
 * @author ondrejsatera
 */
@Entity
public class PersonGroup implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private Long id;
    private String groupname;
    private String useremail;
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    @Version
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id == null ? 0 : id.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof PersonGroup) {
            PersonGroup other = (PersonGroup) object;
            return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "cz.vse.javaee.issuet.model.PersonGroup[ id=" + id + " ]";
    }
    
}
