package cz.vse.javaee.issuet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Parent class for all DAO classes which holds instance of the main entity manager
 * @author ondrejsatera
 */
public class DAO {
    @PersistenceContext(unitName = "IssueT")
    protected EntityManager entityManager;

    protected DAO() {
        // Protected constructor to disable instantiation of this class directly
    }
}
