package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.Person;
import cz.vse.javaee.issuet.model.Project;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface for DAO EJB which manages internal projects
 * @author ondrejsatera
 */
@Local
public interface ProjectDAOBeanLocal {
    
    Project createProject(Project project);
    
    Project getProjectById(Long id);
    
    void removeProject(Project project);
    
    Project editProject(Project project);
    
    Project refreshProject(Project project);

    List<Project> getAllByOwner(Person loggedUser);
}
