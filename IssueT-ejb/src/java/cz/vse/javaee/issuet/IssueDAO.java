package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Project;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Implementation of EJB for managing internal issues
 * @author ondrejsatera
 */
@Stateless
public class IssueDAO extends DAO implements IssueDAOLocal {

    /**
     * Checks whether the issue already exists, if not it'll be persisted, otherwise merged
     * @param issue
     * @return refreshed entity
     */
    @Override
    public Issue createIssue(Issue issue) {
        Issue resultIssue;
        if (issue.getId() == null || getIssueById(issue.getId()) == null) {
            entityManager.persist(issue);
            entityManager.flush();
            resultIssue = entityManager.merge(issue);
        } else {
            resultIssue = editIssue(issue);
        }
        
        return resultIssue;
    }

    /**
     * Returns issue with the specified identifier
     * @param id
     * @return Issue if found, otherwise null
     */
    @Override
    public Issue getIssueById(Long id) {
        return entityManager.find(Issue.class, id);
    }

    /**
     * Removes issue from the persistence unit
     * @param issue
     */
    @Override
    public void removeIssue(Issue issue) {
        entityManager.merge(issue);
        entityManager.remove(issue);
        entityManager.flush();
    }

    /**
     * Saves changes in the already persisted entity
     * @param issue
     * @return refreshed entity
     */
    @Override
    public Issue editIssue(Issue issue) {
        entityManager.merge(issue);
        entityManager.flush();
        
        return issue;
    }

    /**
     * Refreshes entity with the state in the persistence unit
     * @param issue
     * @return refreshed entity
     */
    @Override
    public Issue refreshIssue(Issue issue) {
        Issue resultIssue = entityManager.merge(issue);
        entityManager.refresh(resultIssue);
        return resultIssue;
    }

    /**
     * Returns all issues from the project filtered by the specified filter string
     * @param project
     * @param filter
     * @return list of opened issues if filter equals to 'open', otherwise 'closed'/'resolved'
     */
    @Override
    public List<Issue> getIssues(Project project, String filter) {
        boolean returnOpened = "open".equals(filter);
        boolean returnClosed = "closed".equals(filter);
        
        List<Issue> result = new ArrayList<>();
        for (Issue issue : project.getIssues()) {
            if (issue.isOpen() && returnOpened)
                result.add(issue);
            else if (!issue.isOpen() && returnClosed)
                result.add(issue);
            else {
                if (issue.getBody().contains(filter))
                    result.add(issue);
                else if (issue.getSpecificIdentifier().equals(filter))
                    result.add(issue);
                else if (issue.getSpecificState().equals(filter))
                    result.add(issue);
                else if (issue.getName().contains(filter))
                    result.add(issue);
            }
        }
        return result;
    }
}
