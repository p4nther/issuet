package cz.vse.javaee.issuet;

/**
 * Expection used when queried user does not exist in the persistence unit
 * @author ondrejsatera
 */
public class NoUserFoundException extends Exception {
    private final String invalidEmail;
    
    public NoUserFoundException(String email) {
        super(email);
        this.invalidEmail = email;
    }

    public String getInvalidEmail() {
        return invalidEmail;
    }
    
}
