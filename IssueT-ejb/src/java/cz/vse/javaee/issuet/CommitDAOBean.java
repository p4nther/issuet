package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.Repository;
import cz.vse.javaee.issuet.model.RepositoryCommit;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 * Implementation of EJB for managing commits in the repositories
 * @author ondrejsatera
 */
@Stateless
public class CommitDAOBean extends DAO implements CommitDAOBeanLocal {

    /**
     * Returns commit with specified sha (primary identifier)
     * @param sha
     * @return RepositoryCommit if found, otherwise null
     */
    @Override
    public RepositoryCommit getCommit(String sha) {
        return entityManager.find(RepositoryCommit.class, sha);
    }

    /**
     * Saves changes in the already saved commit
     * @param commit
     * @return refreshed entity
     */
    @Override
    public RepositoryCommit editCommit(RepositoryCommit commit) {
        RepositoryCommit RCcommit = entityManager.merge(commit);
        entityManager.flush();
        
        return RCcommit;
    }

    /**
     * Returns all commits in the repository
     * @param repository
     * @return list of commits
     */
    @Override
    public List<RepositoryCommit> getCommits(Repository repository) {
        Query query = entityManager.createQuery("SELECT rc FROM RepositoryCommit rc WHERE rc.repository=:repo");
        query.setParameter("repo", repository);
        return (List<RepositoryCommit>) query.getResultList();
    }
    
}
