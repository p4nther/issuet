package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.BitBucketIssue;
import cz.vse.javaee.issuet.model.BitBucketRepository;
import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.RepositoryCommit;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface for DAO EJB which manages entities from BitBucket VCS
 * @author ondrejsatera
 */
@Local
public interface BitBucketDAOBeanLocal {

    BitBucketRepository get(String name);
    
    List<BitBucketRepository> getAll();
    
    BitBucketRepository save(BitBucketRepository repository);
    
    BitBucketIssue getIssue(Long localId);
    
    BitBucketIssue saveIssue(BitBucketIssue issue);
    
    BitBucketIssue editIssue(BitBucketIssue issue);
    
    RepositoryCommit getCommit(String sha);
    
    RepositoryCommit saveCommit(RepositoryCommit commit);
    
    RepositoryCommit editCommit(RepositoryCommit commit);
    
    boolean issueHasCommitAssigned(BitBucketIssue issue, RepositoryCommit commit);
    
    List<Issue> getIssues(Project project, String filter);
}
