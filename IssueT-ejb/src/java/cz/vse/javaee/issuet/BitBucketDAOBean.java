package cz.vse.javaee.issuet;

import cz.vse.javaee.issuet.model.BitBucketIssue;
import cz.vse.javaee.issuet.model.BitBucketRepository;
import cz.vse.javaee.issuet.model.Issue;
import cz.vse.javaee.issuet.model.Project;
import cz.vse.javaee.issuet.model.RepositoryCommit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 * Implementation of EJB for managing BitBucket VCS entities
 * @author ondrejsatera
 */
@Stateless
public class BitBucketDAOBean extends DAO implements BitBucketDAOBeanLocal {

    private static final String ISSUE_OPEN = "open";
    private static final String ISSUE_CLOSED = "closed";
    
    /**
     * Returns repository with this name
     * @param name
     * @return BitBucketRepository if found, otherwise null
     */
    @Override
    public BitBucketRepository get(String name) {
        return entityManager.find(BitBucketRepository.class, name);
    }
    
    /**
     * Returns list of all BitBucket repositories
     * @return list of BitBucket repositories
     */
    @Override
    public List<BitBucketRepository> getAll() {
        Query query = entityManager.createQuery("SELECT b FROM BitBucketRepository b");
        return (List<BitBucketRepository>) query.getResultList();
    }
    
    /**
     * Saves changes in the old repository or persists a new repository
     * @param repository
     * @return refreshed entity
     */
    @Override
    public BitBucketRepository save(BitBucketRepository repository) {
        BitBucketRepository BBrepository;
        if (entityManager.find(BitBucketRepository.class, repository.getName()) == null) {
            entityManager.persist(repository);
            entityManager.flush();
            BBrepository = entityManager.merge(repository);
        } else {
            BBrepository = entityManager.merge(repository);
        }
        
        return BBrepository;
    }

    /**
     * Returns BitBucket issue with this localId (BitBucket's internal identifier)
     * @param localId
     * @return BitBucketIssue if found, otherwise null
     */
    @Override
    public BitBucketIssue getIssue(Long localId) {
        return entityManager.find(BitBucketIssue.class, localId);
    }

    /**
     * Saves changes in the old issue or persists a new issue
     * @param issue
     * @return refreshed entity
     */
    @Override
    public BitBucketIssue saveIssue(BitBucketIssue issue) {
        if (entityManager.find(BitBucketIssue.class, issue.getLocalId()) == null) {
            entityManager.persist(issue);
            entityManager.flush();
        }
        return entityManager.merge(issue);
    }

    /**
     * Saves changes in the already created issue
     * @param issue
     * @return refreshed entity
     */
    @Override
    public BitBucketIssue editIssue(BitBucketIssue issue) {
        BitBucketIssue BBissue = entityManager.merge(issue);
        entityManager.flush();
        return BBissue;
    }

    /**
     * Returns commit with sha identifier
     * @param sha
     * @return RepositoryCommit if found, otherwise null
     */
    @Override
    public RepositoryCommit getCommit(String sha) {
        return entityManager.find(RepositoryCommit.class, sha);
    }

    /**
     * Saves changes in the old commit or persists a new commit
     * @param commit
     * @return refreshed entity
     */
    @Override
    public RepositoryCommit saveCommit(RepositoryCommit commit) {
        if (getCommit(commit.getSha()) == null) {
            entityManager.persist(commit);
            entityManager.flush();
        }
        return entityManager.merge(commit);
    }

    /**
     * Saves changes in the already created commit
     * @param commit
     * @return refreshed entity
     */
    @Override
    public RepositoryCommit editCommit(RepositoryCommit commit) {
        RepositoryCommit RCcommit = entityManager.merge(commit);
        entityManager.flush();
        return RCcommit;
    }
    
    /**
     * Checks whether this issue has assigned this commit
     * @param issue
     * @param commit
     * @return true if issue has the commit already assigned, false otherwise
     */
    @Override
    public boolean issueHasCommitAssigned(BitBucketIssue issue, RepositoryCommit commit) {
        for (RepositoryCommit rc : issue.getCommits()) {
            if (rc.getSha() == null ? commit.getSha() == null : rc.getSha().equals(commit.getSha())) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Returns all issues from the project filtered by the specified filter string
     * @param project
     * @param filter
     * @return list of issues
     */
    @Override
    public List<Issue> getIssues(Project project, String filter) {
        Query query;
        if (ISSUE_OPEN.equals(filter) || ISSUE_CLOSED.equals(filter)) {
            query = entityManager.createQuery("SELECT i FROM BitBucketIssue i WHERE i.repository=:repo AND i.status IN :filter");
            query.setParameter("repo", project.getRepository().getBitBucketRepository());
            if ("open".equals(filter)) {
                query.setParameter("filter", Arrays.asList("new","open","on hold"));
            } else {
                query.setParameter("filter", Arrays.asList("resolved","invalid","duplicate","wontfix"));
            }
        } else {
            query = entityManager.createQuery("SELECT i FROM BitBucketIssue i WHERE i.repository=:repo AND (i.localId=:filterLong OR i.title LIKE :filter OR i.content LIKE :filter OR i.priority LIKE :filter)");
            query.setParameter("repo", project.getRepository().getBitBucketRepository());
            query.setParameter("filter", "%" + filter + "%");
            Long filterLong;
            try {
                filterLong = Long.parseLong(filter);
            }
            catch (NumberFormatException ex) {
                filterLong = new Long(0);
            }
            query.setParameter("filterLong", filterLong);
        }
        
        List<BitBucketIssue> issues = query.getResultList();
        List<Issue> result = new ArrayList<>();
        for (BitBucketIssue bitBucketIssue : issues) {
            result.add(bitBucketIssue.getIssue());
        }
        return result;
    }
}
