package cz.vse.javaee.issuet.model;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author ondrejsatera
 */
public class IssueTest {
    
    protected Issue instance;

    @Before
    public void setUp() {
        this.instance = new Issue();
    }
    
    /**
     * Test of getRepositoryIssue, of class Issue
     */
    @Test
    public void testGetRepositoryIssueWithoutAny() {
        System.out.println("GetRepositoryIssueWithoutAny");
        assertNull(instance.getRepositoryIssue());
    }
    
    /**
     * Test of getRepositoryIssue, of class Issue
     */
    @Test
    public void testGetRepositoryIssueWithBitBucketIssue() {
        System.out.println("GetRepositoryIssueWithBitBucketIssue");
        BitBucketIssue bbIssue = this.setBitBucketIssue(Long.MIN_VALUE, null, null);
        assertSame(bbIssue, instance.getRepositoryIssue());
    }
    
    /**
     * Test of getRepositoryIssue, of class Issue
     */
    @Test
    public void testGetRepositoryIssueWithGitHubIssue() {
        System.out.println("GetRepositoryIssueWithGitHubIssue");
        GithubIssue ggIssue = this.setGitHubIssue(Integer.SIZE, null, null);
        assertSame(ggIssue, instance.getRepositoryIssue());
    }

    /**
     * Test of getSpecificIdentifier method, of class Issue.
     */
    @Test
    public void testGetSpecificIdentifier() {
        System.out.println("getSpecificIdentifier");
        Long id = new Long(666);
        instance.setId(id);
        String result = instance.getSpecificIdentifier();
        assertEquals(id.toString(), result);
    }
    
    /**
     * Test of getSpecificIdentifier method, of class Issue.
     */
    @Test
    public void testGetSpecificIdentifierWithBitBucketIssue() {
        System.out.println("GetSpecificIdentifierWithBitBucketIssue");
        Long id = new Long(666);
        this.setBitBucketIssue(id, null, null);
        String result = instance.getSpecificIdentifier();
        assertEquals(id.toString(), result);
    }
    
    /**
     * Test of getSpecificIdentifier method, of class Issue.
     */
    @Test
    public void testGetSpecificIdentifierWithGitHubIssue() {
        System.out.println("GetSpecificIdentifierWithGitHubIssue");
        Integer id = new Integer(666);
        this.setGitHubIssue(id, null, null);
        String result = instance.getSpecificIdentifier();
        assertEquals(id.toString(), result);
    }

    /**
     * Test of getSpecificState method, of class Issue.
     */
    @Test
    public void testGetSpecificState() {
        System.out.println("getSpecificState");
        String state = "open";
        instance.setLocalState(state);
        String result = instance.getSpecificState();
        assertEquals(state, result);
    }
    
    /**
     * Test of getSpecificState method, of class Issue.
     */
    @Test
    public void testGetSpecificStateWithBitBucketIssue() {
        System.out.println("GetSpecificStateWithBitBucketIssue");
        String state = "open";
        this.setBitBucketIssue(Long.MIN_VALUE, state, null);
        String result = instance.getSpecificState();
        assertEquals(state, result);
    }
    
    /**
     * Test of getSpecificState method, of class Issue.
     */
    @Test
    public void testGetSpecificStateWithGitHubIssue() {
        System.out.println("GetSpecificStateWithGitHubIssue");
        String state = "open";
        this.setGitHubIssue(Integer.SIZE, state, null);
        String result = instance.getSpecificState();
        assertEquals(state, result);
    }

    /**
     * Test of getBody method, of class Issue.
     */
    @Test
    public void testGetBody() {
        System.out.println("getBody");
        String body = "Issue body";
        instance.setLocalBody(body);
        String result = instance.getBody();
        assertEquals(body, result);
    }
    
    /**
     * Test of getBody method, of class Issue.
     */
    @Test
    public void testGetBodyWithBitBucketIssue() {
        System.out.println("GetBodyWithBitBucketIssue");
        String body = "Issue body";
        String localBody = "Issue local body";
        instance.setLocalBody(localBody);
        this.setBitBucketIssue(Long.MIN_VALUE, null, body);
        String result = instance.getBody();
        assertEquals(body, result);
    }
    
    /**
     * Test of getBody method, of class Issue.
     */
    @Test
    public void testGetBodyWithGitHubIssue() {
        System.out.println("GetBodyWithGitHubIssue");
        String body = "Issue body";
        String localBody = "Issue local body";
        instance.setLocalBody(localBody);
        this.setGitHubIssue(Integer.SIZE, null, body);
        String result = instance.getBody();
        assertEquals(body, result);
    }

    /**
     * Test of isOpen method, of class Issue.
     */
    @Test
    public void testIsOpen() {
        System.out.println("isOpen");
        boolean expResult = false;
        instance.setLocalState("closed");
        boolean result = instance.isOpen();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isOpen method, of class Issue.
     */
    @Test
    public void testIsOpenWithBitBucketIssue() {
        System.out.println("IsOpenWithBitBucketIssue");
        boolean expResult = false;
        this.setBitBucketIssue(Long.MIN_VALUE, "closed", null);
        instance.setLocalState("open");
        boolean result = instance.isOpen();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isOpen method, of class Issue.
     */
    @Test
    public void testIsOpenWithGitHubIssue() {
        System.out.println("IsOpenWithGitHubIssue");
        boolean expResult = false;
        this.setGitHubIssue(Integer.SIZE, "closed", null);
        instance.setLocalState("open");
        boolean result = instance.isOpen();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isOpen method, of class Issue.
     */
    @Test
    public void testIsClosed() {
        System.out.println("isOpen");
        boolean expResult = true;
        instance.setLocalState("open");
        boolean result = instance.isOpen();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isOpen method, of class Issue.
     */
    @Test
    public void testIsClosedWithBitBucketIssue() {
        System.out.println("IsOpenWithBitBucketIssue");
        boolean expResult = true;
        this.setBitBucketIssue(Long.MIN_VALUE, "open", null);
        instance.setLocalState("closed");
        boolean result = instance.isOpen();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isOpen method, of class Issue.
     */
    @Test
    public void testIsClosedWithGitHubIssue() {
        System.out.println("IsOpenWithGitHubIssue");
        boolean expResult = true;
        this.setGitHubIssue(Integer.SIZE, "open", null);
        instance.setLocalState("closed");
        boolean result = instance.isOpen();
        assertEquals(expResult, result);
    }
    
    private BitBucketIssue setBitBucketIssue(Long localId, String status, String description) {
        BitBucketIssue bbIssue = new BitBucketIssue();
        bbIssue.setLocalId(localId);
        bbIssue.setStatus(status);
        bbIssue.setDescription(description);
        
        this.instance.setBitbucketIssue(bbIssue);
        
        return bbIssue;
    }
    
    private GithubIssue setGitHubIssue(Integer number, String state, String description) {
        GithubIssue ggIssue = new GithubIssue();
        ggIssue.setNumber(number);
        ggIssue.setState(state);
        ggIssue.setDescription(description);
        
        this.instance.setGithubIssue(ggIssue);
        
        return ggIssue;
    }
    
}
