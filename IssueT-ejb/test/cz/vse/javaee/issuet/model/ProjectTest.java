package cz.vse.javaee.issuet.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ondrejsatera
 */
public class ProjectTest {

    /**
     * Test of hasMember method, of class Project.
     */
    @Test
    public void testHasMemberPositive() {
        System.out.println("HasMemberPositive");
        Person person = new Person();
        person.setEmail("test@test.cz");
        Project instance = new Project();
        instance.getMembers().add(person);
        boolean expResult = true;
        boolean result = instance.hasMember(person);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hasMember method, of class Project.
     */
    @Test
    public void testHasMemberNegative() {
        System.out.println("HasMemberNegative");
        Person person = new Person();
        person.setEmail("test@test.cz");
        Project instance = new Project();

        boolean expResult = false;
        boolean result = instance.hasMember(person);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hasMember method, of class Project.
     */
    @Test
    public void testHasMemberOwner() {
        System.out.println("HasMemberOwner");
        Person person = new Person();
        person.setEmail("test@test.cz");
        Project instance = new Project();
        instance.setOwner(person);
        boolean expResult = true;
        boolean result = instance.hasMember(person);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeMember method, of class Project.
     */
    @Test
    public void testRemoveMember() {
        System.out.println("removeMember");
        Person person = new Person();
        person.setEmail("test@test.cz");
        Project instance = new Project();
        instance.removeMember(person);
        
        assertFalse(instance.hasMember(person));
    }
    
    /**
     * Test of equals method, of class Project.
     */
    @Test
    public void testEqualsPositive() {
        System.out.println("equalsPositive");
        Project instance = new Project();
        
        Long sameId = new Long(666);
        instance.setId(sameId);
        
        Project compareInstance = new Project();
        compareInstance.setId(sameId);
        
        assertTrue(instance.getId() == compareInstance.getId());
    }
    
    /**
     * Test of equals method, of class Project.
     */
    @Test
    public void testEqualsNegative() {
        System.out.println("EqualsNegative");
        Project instance = new Project();
        instance.setId(Long.MIN_VALUE);
        
        Project compareInstance = new Project();
        compareInstance.setId(Long.MAX_VALUE);
        
        assertFalse(instance.equals(compareInstance));
    }
}
